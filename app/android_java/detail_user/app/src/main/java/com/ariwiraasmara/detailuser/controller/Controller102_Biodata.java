package com.ariwiraasmara.detailuser.controller;
/*
 * @author Ari Wiraasmara
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.util.AbstractString;
import com.ariwiraasmara.detailuser.util.Connection;
import com.ariwiraasmara.detailuser.util.MyFunction;

import static java.lang.Integer.parseInt;

public class Controller102_Biodata implements Interfacecontroller102Biodata {

    protected Context ctx;
    protected final AbstractString string;
    protected final Connection connection;
    protected SQLiteDatabase db;
    protected Model101UserLogin model;
    protected MyFunction fun;
    protected String token;

    public Controller102_Biodata(Context ctx, String tkn) {
        this.ctx = ctx;
        this.connection = new Connection(ctx);
        this.string = new AbstractString();
        this.model = new Model101UserLogin();
        this.fun = new MyFunction();

        if(tkn.isEmpty()) this.token = "";
        else if(tkn.equals("")) this.token = "";
        else this.token = tkn;
    }

    @Override
    public int checkRow(String where) {
        int res = 0;
        this.db = this.connection.getWritableDatabase();
        try {
            String query = string.read_biodata(where, "");
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) res = 1;
            else res = 0;
        }
        catch(Exception e) {
            res = 0;
        }
        return res;
    }

    @Override
    public String[] getData(String where, String order) {
        this.db = this.connection.getWritableDatabase();
        String data[] = new String[4];
        try {
            String query = string.read_biodata(where, "nama ASC");
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    data[0] = cursor.getString(0);
                    data[1] = cursor.getString(1);
                    data[2] = cursor.getString(2);
                    data[3] = cursor.getString(3);
                    data[4] = cursor.getString(4);
                    data[5] = cursor.getString(5);
                    data[6] = cursor.getString(6);
                } while (cursor.moveToNext());
            }
        }
        catch(Exception e) {
            data = null;
        }
        return data;
    }

    @Override
    public String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, String pemisah) {
        String intaid = null;
        String clause = where + " order by " + order + " DESC limit 1";
        int cek = this.checkRow(where);
        if(cek > 0) {
            String[] data = this.getData(where, order);
            int graid = parseInt(data[0].substring(digitangkaterakhir)) + 1;
            String sgraid = String.valueOf(graid);
            intaid = String.format("%"+banyakpemisah_dalamangka+"s", sgraid).replace("", pemisah).replace(" ", pemisah); // , ,
        }
        else {
            intaid = String.format("%"+banyakpemisah_dalamangka+"s", "1").replace("", pemisah).replace(" ", pemisah);
        }
        return intaid;
    }

    @Override
    public boolean insertData(String[] values) {
        this.db = this.connection.getWritableDatabase();
        ContentValues toPut = new ContentValues();
        try {
            toPut.put("id_102", values[0]);
            toPut.put("nama", values[1]);
            toPut.put("email", values[2]);
            toPut.put("tgl", values[3]);
            toPut.put("alamat", values[4]);
            toPut.put("tlp", values[5]);
            toPut.put("status", values[6]);
            this.db.insert(this.string.getTable(102), null, toPut);
            this.db.close();
            Log.d("Success!!!", "Insert Biodata Success!");
            return true;
        }
        catch(Exception e) {
            Log.d("Fail!!!", "Insert Biodata Failed!");
            return false;
        }
    }

    @Override
    public boolean updateData(String[] values, String id) {
        this.db = this.connection.getWritableDatabase();
        ContentValues toPut = new ContentValues();
        try {
            toPut.put("nama", values[0]);
            toPut.put("tgl", values[1]);
            toPut.put("alamat", values[2]);
            toPut.put("tlp", values[3]);
            toPut.put("status", values[4]);
            toPut.put("password", values[5]);
            this.db.update(this.string.getTable(102), toPut, "id='"+id+"' or email='"+id+"'", null);
            this.db.close();
            Log.d("Success!!!", "Update Biodata Success!");
            return true;
        }
        catch(Exception e) {
            Log.d("Fail!!!", "Update Biodata Failed!");
            return false;
        }
    }

    @Override
    public boolean deleteData(String id) {
        try {
            this.db.delete(this.string.getTable(101), "id='"+id+"' or email='"+id+"'", null);
            Log.d("Success!!!", "Delete Biodata Success!");
            return true;
        }
        catch(Exception e) {
            Log.d("Fail!!!", "Delete Biodata Failed!");
            return false;
        }
    }

}
