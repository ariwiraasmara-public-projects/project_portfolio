package com.ariwiraasmara.detailuser.model;

public interface InterfaceModel102Biodata {

    public String getId();
    public void setId(String id_102);
    public String getNama();
    public void setNama(String nama);
    public String getEmail();
    public void setEmail(String email);
    public String getTgl();
    public void setTgl(String tgl);
    public String getAlamat();
    public void setAlamat(String alamat);
    public String getTlp();
    public void setTlp(String tlp);
    public String getStatus();
    public void setStatus(String status);

}
