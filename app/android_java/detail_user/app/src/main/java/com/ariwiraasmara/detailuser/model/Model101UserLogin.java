package com.ariwiraasmara.detailuser.model;

public class Model101UserLogin implements InterfaceModel101UserLogin {

    protected String id_101;
    protected String username;
    protected String email;
    protected String password;

    public Model101UserLogin() {

    }

    public Model101UserLogin(String id, String user, String email, String pass) {
        this.setId(id);
        this.setUser(user);
        this.setEmail(email);
        this.setPass(pass);
    }

    public final String getId() {
        return this.id_101;
    }

    public void setId(String id_101) {
        this.id_101 = id_101;
    }

    public final String getUser() {
        return this.username;
    }

    public void setUser(String username) {
        this.username = username;
    }

    public final String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public final String getPass() {
        return this.password;
    }

    public void setPass(String password) {
        this.password = password;
    }
}
