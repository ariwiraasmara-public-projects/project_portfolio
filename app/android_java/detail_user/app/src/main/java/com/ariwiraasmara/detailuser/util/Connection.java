package com.ariwiraasmara.detailuser.util;
/*
 * @author Ari Wiraasmara
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
public class Connection extends SQLiteOpenHelper  {

    protected final static String dbName = "db_contoh_myportfolio.db";
    protected final static int dbVersion = 1;

    protected Context context;
    protected SQLiteDatabase db;

    public Connection(Context ctx) {
        super(ctx, this.dbName, null, this.dbVersion);
        this.context = ctx;
        this.db = getWritableDatabase();
    }

    public Connection open() throws SQLException {
        this.db = getWritableDatabase();
        return this;
    }

    @Override
    public void onCreate(SQLiteDatabase dbs) {
        // TODO Auto-generated method stub
        this.db.execSQL(this.createTable(101));
        this.db.execSQL(this.createTable(102));
    }
    @Override
    public void onUpgrade(SQLiteDatabase dbs, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        this.db.execSQL(this.dropTable(101));
        this.db.execSQL(this.dropTable(102));
        this.onCreate(dbs);
    }

    public void close() {
        this.db.close();
    }

    protected final String createTable(int code) {
        if(code == 101) {
            return "create table if not exists my101_userlogin(" +
                        "id_101 varchar(255) not null primary key," +
                        "username varchar(255) not null," +
                        "email varchar(255) not null," +
                        "pass text null" +
                    ");";
        }
        else if(code == 102) {
            return "create table if not exists my102_biodata(" +
                        "id_102 varchar(255) not null primary key," +
                        "nama varchar(255) not null," +
                        "email varchar(255) not null," +
                        "tgl text null," +
                        "alamat text null," +
                        "tlp text null," +
                        "status text null" +
                    ")";
        }
        else return "__null__";
    }

    protected final String dropTable(int code) {
        if(code == 101) return "DROP TABLE IF EXISTS my101_userlogin";
        else if(code == 102) return "DROP TABLE IF EXISTS my102_biodata";
        else return "__null__";
    }

}
