package com.ariwiraasmara.detailuser.util;
/*
 * @author Ari Wiraasmara
 */

public abstract interface InterfaceAbstractString {
    public abstract String read_userlogin(String where);
    public abstract String insert_userlogin(String values);
    public abstract String update_userlogin(String values, String id);
    public abstract String delete_userlogin(String id);
    public abstract String read_biodata(String where, String orderby);
    public abstract String insert_biodata(String kolom, String values);
    public abstract String update_biodata(String values, String id);
    public abstract String delete_biodata(String id);
}
