package com.ariwiraasmara.detailuser.view.my101_userlogin;
/*
 * @author Ari Wiraasmara
 */

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.ariwiraasmara.detailuser.R;
import com.ariwiraasmara.detailuser.controller.Controller101_UserLogin;
import com.ariwiraasmara.detailuser.controller.Controller102_Biodata;
import com.ariwiraasmara.detailuser.controller.Interfacecontroller102Biodata;
import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.model.Model102Biodata;
import com.ariwiraasmara.detailuser.util.Connection;
import com.ariwiraasmara.detailuser.util.MyFunction;

public class VI01Login extends AppCompatActivity {

    protected Model101UserLogin model101;
    protected Model102Biodata model102;
    protected Controller101_UserLogin cont101;
    protected Controller102_Biodata cont102;
    protected Connection conn;
    protected Context appContext;
    protected MyFunction fun;
    protected Intent setInt;
    protected Intent getInt;
    protected String token;

    protected EditText txtUserMail;
    protected EditText txtPass;
    protected Button btnLogin;
    protected Button btnRegister;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vi01_login);
        this.init();
        this.onClick();
    }

    protected final void init() {
        this.fun = new MyFunction();
        this.appContext = this.getApplicationContext();
        if(getInt.getStringExtra("login").isEmpty()) this.token = this.fun.genToken("login");
        else this.token = getInt.getStringExtra("login").toString();
        try {
            this.conn = new Connection(appContext);
            this.cont101 = new Controller101_UserLogin(this.appContext, this.token);
            this.cont102 = new Controller102_Biodata(this.appContext, this.token);
        }
        catch(Exception e) {}
        this.txtUserMail = findViewById(R.id.viu101_txt_usermail);
        this.txtPass = findViewById(R.id.viu101_txt_password);
        this.btnLogin = findViewById(R.id.viu101_btn_login);
        this.btnRegister = findViewById(R.id.viu101_btn_register);
    }

    protected void onClick() {
        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String usermail = " (username='" + fun.escape(txtUserMail.getText().toString()) + "' or email='" + fun.escape(txtUserMail.getText().toString()) + "') ";
                    String pass = " and password='" + fun.escape(txtPass.getText().toString()) + "'";
                    int res = cont101.checkRow(usermail + pass);
                    if(res > 0) {
                        String[] data_login   = cont101.getData(usermail + pass);
                        String[] data_biodata = cont102.getData(" email='" + data_login[2] + "'", "");

                        model101 = new Model101UserLogin(data_login[0], data_login[1], data_login[2], data_login[3]);
                        model102.setNamauser(data_biodata[1]);
                        model102.setEmail("");
                        setInt = new Intent(appContext, VI02Register.class);
                        setInt.putExtra("id_101", data_login[0]);
                        setInt.putExtra("username", data_login[1]);
                        setInt.putExtra("email", data_login[2]);
                        setInt.putExtra("biodata", token);
                        startActivity(setInt);
                    }
                    else Toast.makeText(appContext, "Username / Email atau Password Salah!\nSilahkan Coba Lagi!", Toast.LENGTH_LONG);
                }
                catch(Exception e) {

                }
            }
        });

        this.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInt = new Intent(appContext, VI02Register.class);
                setInt.putExtra("register", token);
                startActivity(setInt);
            }
        });
    }

}