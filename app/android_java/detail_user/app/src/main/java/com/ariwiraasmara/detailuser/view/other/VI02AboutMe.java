package com.ariwiraasmara.detailuser.view.other;
/*
 * @author Ari Wiraasmara
 */

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ariwiraasmara.detailuser.R;
public class VI02AboutMe extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vi02_about_me);
    }
}