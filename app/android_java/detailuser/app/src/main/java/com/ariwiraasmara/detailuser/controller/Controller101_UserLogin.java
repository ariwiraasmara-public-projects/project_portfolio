package com.ariwiraasmara.detailuser.controller;
/*
 * @author Ari Wiraasmara
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.ariwiraasmara.detailuser.model.InterfaceModel101UserLogin;
import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.util.AbstractString;
import com.ariwiraasmara.detailuser.util.Connection;
import com.ariwiraasmara.detailuser.util.MyFunction;

import static java.lang.Integer.parseInt;

public class Controller101_UserLogin implements Interfacecontroller101UserLogin {

    protected Context ctx;
    protected final AbstractString string;
    protected final Connection connection;
    protected SQLiteDatabase db;
    protected Model101UserLogin model;
    protected MyFunction fun;
    protected String token;

    public Controller101_UserLogin(Context ctx, String tkn) {
        this.ctx = ctx;
        this.connection = new Connection(ctx);
        this.string = new AbstractString();
        this.model = new Model101UserLogin();
        this.fun = new MyFunction();

        //if(tkn.isEmpty()) this.token = "";
        //else if(tkn.equals("")) this.token = "";
        //else this.token = tkn;
    }

    @Override
    public int checkRow(String where) {
        int res = 0;
        this.db = this.connection.getWritableDatabase();
        try {
            String query = this.string.read_userlogin(where);
            Cursor cursor = this.db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    res++;
                } while (cursor.moveToNext());
            }
            else res = 0;
        }
        catch(Exception e) {
            res = 0;
        }
        return res;
    }

    @Override
    public String[] getData(String where) {
        this.db = this.connection.getWritableDatabase();
        String data[] = new String[4];
        try {
            String query = string.read_userlogin(where);
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    data[0] = cursor.getString(0);
                    data[1] = cursor.getString(1);
                    data[2] = cursor.getString(2);
                    data[3] = cursor.getString(3);
                } while (cursor.moveToNext());
            }
        }
        catch(Exception e) {
            data = null;
        }
        return data;
    }

    @Override
    public String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, String pemisah) {
        String intaid = null;
        String clause = where + " order by " + order + " DESC limit 1";
        int cek = this.checkRow(where);
        if(cek > 0) {
            String[] data = this.getData(where);
            int graid = parseInt(data[0].substring(digitangkaterakhir)) + 1;
            String sgraid = String.valueOf(graid);
            intaid = String.format("%"+banyakpemisah_dalamangka+"s", sgraid).replace("", pemisah).replace(" ", pemisah); // , ,
        }
        else {
            intaid = String.format("%"+banyakpemisah_dalamangka+"s", "1").replace("", pemisah).replace(" ", pemisah);
        }
        return intaid;
    }

    @Override
    public boolean insertData(String id, String user, String email, String pass) {
        this.db = this.connection.getWritableDatabase();
        ContentValues toPut = new ContentValues();
        try {
            toPut.put("id_101", id);
            toPut.put("username", user);
            toPut.put("email", email);
            toPut.put("password", pass);
            this.db.insert(this.string.getTable(101), null, toPut);
            this.db.close();
            Log.d("Success!!!", "Insert User Login Success!");
            return true;
        }
        catch(Exception e) {
            Log.d("Fail!!!", "Insert User Login Failed!");
            return false;
        }
    }

    @Override
    public boolean updateData(String pass, String id) {
        this.db = this.connection.getWritableDatabase();
        ContentValues toPut = new ContentValues();
        try {
            toPut.put("password", pass);
            this.db.update(this.string.getTable(101), toPut, "id='"+id+"' or email='"+id+"'", null);
            this.db.close();
            Log.d("Success!!!", "Update User Login Success!");
            return true;
        }
        catch(Exception e) {
            Log.d("Fail!!!", "Update User Login Failed!");
            return false;
        }
    }

    @Override
    public boolean deleteData(String id) {
        try {
            db.delete(this.string.getTable(101), "id='"+id+"' or email='"+id+"'", null);
            Log.d("Success!!!", "Delete User Login Success!");
            return true;
        }
        catch(Exception e) {
            Log.d("Fail!!!", "Delete User Login Failed!");
            return false;
        }
    }
}
