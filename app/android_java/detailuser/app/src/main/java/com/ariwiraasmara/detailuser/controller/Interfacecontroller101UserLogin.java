package com.ariwiraasmara.detailuser.controller;

public interface Interfacecontroller101UserLogin {
    public abstract int checkRow(String where);
    public abstract String[] getData(String where);
    public abstract String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, String pemisah);
    public abstract boolean insertData(String id, String user, String email, String pass);
    public abstract boolean updateData(String pass, String id);
    public abstract boolean deleteData(String id);
}
