package com.ariwiraasmara.detailuser.controller;

public interface Interfacecontroller102Biodata {
    public abstract int checkRow(String where);
    public abstract String[] getData(String where, String order);
    public abstract String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, String pemisah);
    public abstract boolean insertData(String id, String nama, String email, String tgl, String alamat, String tlp, String status);
    public abstract boolean updateData(String idemail, String nama, String tgl, String alamat, String tlp, String status);
    public abstract boolean deleteData(String id);
}
