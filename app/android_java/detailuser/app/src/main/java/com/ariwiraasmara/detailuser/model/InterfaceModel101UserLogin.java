package com.ariwiraasmara.detailuser.model;

public interface InterfaceModel101UserLogin {

    public String getId();
    public void setId(String id_101);
    public String getUser();
    public void setUser(String username);
    public String getEmail();
    public void setEmail(String email);
    public String getPass();
    public void setPass(String password);

}
