package com.ariwiraasmara.detailuser.model;

public class Model102Biodata implements InterfaceModel102Biodata {

    protected String namauser;
    protected String id_102;
    protected String nama;
    protected String email;
    protected String tgl;
    protected String alamat;
    protected String tlp;
    protected String status;

    public Model102Biodata() {

    }

    public Model102Biodata(String id, String nama, String email, String tgl, String alamat, String tlp, String status) {
        this.setId(id);
        this.setNama(nama);
        this.setEmail(email);
        this.setTgl(tgl);
        this.setAlamat(alamat);
        this.setTlp(tlp);
        this.setStatus(status);
    }

    public void setNamauser(String namauser) {
        this.namauser = namauser;
    }

    public final String getNamauser() {
        return this.namauser;
    }

    public final String getId() {
        return id_102;
    }

    public void setId(String id_102) {
        this.id_102 = id_102;
    }

    public final String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public final String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public final String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public final String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public final String getTlp() {
        return tlp;
    }

    public void setTlp(String tlp) {
        this.tlp = tlp;
    }

    public final String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
