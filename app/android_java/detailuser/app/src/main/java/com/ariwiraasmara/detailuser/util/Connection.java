package com.ariwiraasmara.detailuser.util;
/*
 * @author Ari Wiraasmara
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.model.Model102Biodata;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class Connection extends SQLiteOpenHelper  {

    protected AbstractString string;
    protected final static String dbName = "myportfolio.db";
    protected final static int dbVersion = 1;

    protected Context context;
    protected SQLiteDatabase sysdb;

    public Connection(Context ctx) {
        super(ctx, dbName, null, dbVersion);
        context = ctx;
        this.sysdb = getWritableDatabase();
    }

    public Connection open() throws SQLException {
        this.string = new AbstractString();
        this.sysdb = getWritableDatabase();
        return this;
    }

    @Override
    public void onCreate(SQLiteDatabase dbs) {
        // TODO Auto-generated method stub
        dbs.execSQL(createTable(101));
        dbs.execSQL(createTable(102));
    }
    @Override
    public void onUpgrade(SQLiteDatabase dbs, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        dbs.execSQL(dropTable(101));
        dbs.execSQL(dropTable(102));
        onCreate(dbs);
    }

    public void close() {
        sysdb.close();
    }

    protected final String createTable(int code) {
        if(code == 101) {
            return "create table my101_userlogin(" +
                        "id_101 varchar(255) not null primary key," +
                        "username varchar(255) not null," +
                        "email varchar(255) not null," +
                        "pass text null" +
                    ");";
        }
        else if(code == 102) {
            return "create table my102_biodata(" +
                        "id_102 varchar(255) not null primary key," +
                        "nama varchar(255) not null," +
                        "email varchar(255) not null," +
                        "tgl text null," +
                        "alamat text null," +
                        "tlp text null," +
                        "status text null" +
                    ")";
        }
        else return "__null__";
    }

    protected final String dropTable(int code) {
        if(code == 101) return "DROP TABLE IF EXISTS my101_userlogin";
        else if(code == 102) return "DROP TABLE IF EXISTS my102_biodata";
        else return "__null__";
    }

    public int checkRow(int code, String where, String order) {
        int res = 0;
        this.sysdb = getWritableDatabase();
        try {
            String query = "";
            if(code == 101) query = this.string.read_userlogin(where);
            else if (code == 10) query = this.string.read_biodata(where, order);
            Cursor cursor = this.sysdb.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    res++;
                } while (cursor.moveToNext());
            }
            else res = 0;
        }
        catch(Exception e) {
            res = 0;
        }
        return res;
    }

    // UserLogin ** {
        public String getID_UserLogin(String where) {
            String id = "";
            this.sysdb = getReadableDatabase();
            try {
                String query = string.read_userlogin(where);
                Cursor cursor = this.sysdb.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    do {
                        id = cursor.getString(0).toString();
                        Log.d("ID User Login : ", id.toString());
                    } while (cursor.moveToNext());
                }

                return id;
            }
            catch(Exception e) {
                return "";
            }
        }

        public List<Model101UserLogin> getDataUserLogin(String where) {
            List<Model101UserLogin> list = new ArrayList<Model101UserLogin>();
            //this.sysdb = getWritableDatabase();
            this.sysdb = getReadableDatabase();
            try {
                String query = string.read_userlogin(where);
                Cursor cursor = this.sysdb.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    do {
                        Model101UserLogin model = new Model101UserLogin();
                        model.setId(cursor.getString(0));//data[0] = cursor.getString(0);
                        model.setUser(cursor.getString(1));//data[1] = cursor.getString(1);
                        model.setEmail(cursor.getString(2));//data[2] = cursor.getString(2);
                        model.setPass(cursor.getString(3));//data[3] = cursor.getString(3);
                        list.add(model);
                    } while (cursor.moveToNext());
                }
            }
            catch(Exception e) {
                list = null;
            }
            return list;
        }

    public List<Model101UserLogin> getAllUserLogin() {
        List<Model101UserLogin> list = new ArrayList<Model101UserLogin>();
        List<String> getList = new ArrayList<String>();
        this.sysdb = getReadableDatabase();
        try {
            String query = string.read_userlogin("");
            Cursor cursor = this.sysdb.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Model101UserLogin model = new Model101UserLogin();
                    model.setId(cursor.getString(0));//data[0] = cursor.getString(0);
                    model.setUser(cursor.getString(1));//data[1] = cursor.getString(1);
                    model.setEmail(cursor.getString(2));//data[2] = cursor.getString(2);
                    model.setPass(cursor.getString(3));//data[3] = cursor.getString(3);
                    list.add(model);
                    //getList.add(Model101UserLogin.getList().toString());
                    Log.d("list barang", getList.toString());
                } while (cursor.moveToNext());
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Log.d("Table Barang", e.toString());
        }

        return list;
    }

        public String getLastIDUserLogin(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, String pemisah) {
            String intaid = null;
            try {
                Model101UserLogin model = new Model101UserLogin();
                String clause = where + " order by " + order + " limit 1";
                int cek = this.checkRow(101, where, order);
                if(cek > 0) {
                    String data = this.getID_UserLogin(where);
                    int graid = parseInt(data.substring(digitangkaterakhir)) + 1;
                    String sgraid = String.valueOf(graid);
                    intaid = String.format("%"+banyakpemisah_dalamangka+"s", sgraid).replace("", pemisah).replace(" ", ""); // , ,
                }
                else {
                    intaid = String.format("%"+banyakpemisah_dalamangka+"s", "1").replace("", pemisah).replace(" ", "");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return "UL_" + intaid;
        }

        public boolean insertDataUserLogin(String id, String user, String email, String pass) {
            this.sysdb = getWritableDatabase();
            ContentValues toPut = new ContentValues();
            try {
                toPut.put("id_101", id);
                toPut.put("username", user);
                toPut.put("email", email);
                toPut.put("password", pass);
                this.sysdb.insert("my101_userlogin", "id_101", toPut);
                //this.sysdb.close();
                System.out.println("ID 101: " + id + "; User: " + user + "; Email: " + email + "; Pass: " + pass);
                Log.d("Success!!!", "Insert User Login Success!");
                return true;
            }
            catch(Exception e) {
                Log.d("Fail!!!", "Insert User Login Failed!");
                return false;
            }
        }

        public boolean updateDataUserLogin(String pass, String id) {
            this.sysdb = getWritableDatabase();
            ContentValues toPut = new ContentValues();
            try {
                toPut.put("password", pass);
                this.sysdb.update("", toPut, "id='"+id+"' or email='"+id+"'", null);
                //this.sysdb.close();
                Log.d("Success!!!", "Update User Login Success!");
                return true;
            }
            catch(Exception e) {
                Log.d("Fail!!!", "Update User Login Failed!");
                return false;
            }
        }

        public boolean deleteDataUserLogin(String id) {
            try {
                this.sysdb.delete(this.string.getTable(101), "id='"+id+"' or email='"+id+"'", null);
                Log.d("Success!!!", "Delete User Login Success!");
                return true;
            }
            catch(Exception e) {
                Log.d("Fail!!!", "Delete User Login Failed!");
                return false;
            }
        }
    // }** UserLogin

    // Biodata **{
        public String[] getDataBiodata(String where) {
            this.sysdb = getWritableDatabase();
            String data[] = new String[4];
            try {
                String query = string.read_biodata(where, "nama ASC");
                Cursor cursor = sysdb.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    do {
                        data[0] = cursor.getString(0);
                        data[1] = cursor.getString(1);
                        data[2] = cursor.getString(2);
                        data[3] = cursor.getString(3);
                        data[4] = cursor.getString(4);
                        data[5] = cursor.getString(5);
                        data[6] = cursor.getString(6);
                    } while (cursor.moveToNext());
                }
            }
            catch(Exception e) {
                data = null;
            }
            return data;
        }

    public List<String> getBiodata(String a, String b) {
        List<Model102Biodata> list = new ArrayList<Model102Biodata>();
        List<String> getList = new ArrayList<String>();

        try {
            String query = "select * from identitas_barang where nama_user ='" + a + "' and email = '" + b + "'";
            Cursor cursor = this.sysdb.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {
                    Model102Biodata model = new Model102Biodata();
                    model.setId(cursor.getString(0));
                    model.setNama(cursor.getString(1));
                    model.setEmail(cursor.getString(2));
                    model.setTgl(cursor.getString(3));
                    model.setAlamat(cursor.getString(4));
                    model.setTlp(cursor.getString(5));
                    model.setStatus(cursor.getString(6));
                    list.add(model);
                    //getList.add(Model102Biodata.getListBarang().toString());
                    Log.d("list barang", getList.toString());
                } while (cursor.moveToNext());
            }
            else getList.add("Barang masih kosong!");
        }
        catch(Exception e) {
            e.printStackTrace();
            Log.d("Table Barang", e.toString());
        }

        return getList;
    }

        public String getLastIDBiodata(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, String pemisah) {
            String intaid = null;
            String clause = where + " order by " + order + " limit 1";
            int cek = this.checkRow(102, where, order);
            if(cek > 0) {
                String[] data = this.getDataBiodata(where);
                int graid = parseInt(data[0].substring(digitangkaterakhir)) + 1;
                String sgraid = String.valueOf(graid);
                intaid = String.format("%"+banyakpemisah_dalamangka+"s", sgraid).replace("", pemisah).replace(" ", ""); // , ,
            }
            else {
                intaid = String.format("%"+banyakpemisah_dalamangka+"s", "1").replace("", pemisah).replace(" ", "");
            }
            return "BI_" + intaid;
        }

        public boolean insertDataBiodata(String id, String nama, String email, String tgl, String alamat, String tlp, String status) {
            //this.sysdb = getWritableDatabase();
            ContentValues toPut = new ContentValues();
            try {
                toPut.put("id_102", id);
                toPut.put("nama", nama);
                toPut.put("email", email);
                toPut.put("tgl", tgl);
                toPut.put("alamat", alamat);
                toPut.put("tlp", tlp);
                toPut.put("status", status);
                this.sysdb.insert("my102_biodata", "id_102", toPut);
                //this.sysdb.close();
                System.out.println("ID 102: " + id + "; Nama: " + nama + "; Email: " + email + "; Tanggal: " + tgl + "; Alamat: " + alamat + "; Tlp: " + tlp + "; Status: " + status);
                Log.d("Success!!!", "Insert Biodata Success!");
                return true;
            }
            catch(Exception e) {
                Log.d("Fail!!!", "Insert Biodata Failed!");
                return false;
            }
        }

        public boolean updateDataBiodata(String idemail, String nama, String tgl, String alamat, String tlp, String status) {
            this.sysdb = getWritableDatabase();
            ContentValues toPut = new ContentValues();
            try {
                toPut.put("nama", nama);
                toPut.put("tgl", tgl);
                toPut.put("alamat", alamat);
                toPut.put("tlp", tlp);
                toPut.put("status", status);
                this.sysdb.update(this.string.getTable(102), toPut, "id='"+idemail+"' or email='"+idemail+"'", null);
                this.sysdb.close();
                Log.d("Success!!!", "Update Biodata Success!");
                return true;
            }
            catch(Exception e) {
                Log.d("Fail!!!", "Update Biodata Failed!");
                return false;
            }
        }

        public boolean deleteDataBiodata(String id) {
            try {
                this.sysdb.delete(this.string.getTable(102), "id='"+id+"' or email='"+id+"'", null);
                Log.d("Success!!!", "Delete Biodata Success!");
                return true;
            }
            catch(Exception e) {
                Log.d("Fail!!!", "Delete Biodata Failed!");
                return false;
            }
        }
    // }** Biodata
}
