package com.ariwiraasmara.detailuser.util;
/*
 * @author Ari Wiraasmara
 */

import android.content.Intent;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
public abstract interface InterfaceMyFunction {
    public abstract void setTitle(String str);
    public abstract String getTitle();
    public abstract String escape(String str);
    public abstract String readable(String str);
    public abstract String insan(String str);
    public abstract String formatNumber(double numb);
    public abstract String rupiah(double rupiah);
    public abstract void toSendMail();
    public abstract void setKey(String myKey);
    public abstract String plain_encrypt(String strToEncrypt);
    public abstract String plain_decrypt(String strToDecrypt);
    public abstract String finalEncrypt(String str);
    public abstract String finalDecrypt(String str);
    public abstract byte[] toMD5(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException;
    public abstract byte[] toSHA(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException;
    public abstract String bin2hex(String strCipherText);
    public abstract String hex2bin(String str_biner);
    public abstract String CharBy_One(char x);
    public abstract String setSession(String str);
    public abstract String getSession(String str);
    public abstract Integer randNumber(int min, int max);
    public abstract String randChar();
    public abstract String randSpecial();
    public abstract String randHexColor();
    public abstract String genPass(int length);
    public abstract String genToken(String namatoken);
    public abstract String splitTime(String tgl, String option);
    public abstract int getTime(String tgl, String option);
    public abstract String getLastnCharacters(String inputString, int subStringLength);
}
