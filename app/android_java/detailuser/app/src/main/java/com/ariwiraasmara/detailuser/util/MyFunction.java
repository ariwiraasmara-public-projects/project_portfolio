package com.ariwiraasmara.detailuser.util;

import android.os.Build;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class MyFunction implements InterfaceMyFunction {
    
    protected NumberFormat nf;
    protected DecimalFormat decimal;
    protected MessageDigest md;
    protected final String secret = "#[CANON-ariwiraasmara_3721195@sofiaria.com]";
    
    public MyFunction() {
        
    }
    
    protected String title;
    public void setTitle(String str) {
        try {
            this.title = str;
        }
        catch(Exception e) {
            //e.printStackTrace();
        }
    }
    
    public String getTitle() {
        try {
            return this.title;
        }
        catch(Exception e) {
            //e.printStackTrace();
            return "No Title";
        }
    }

    public String escape(String str) {
        return str.replace("\\", "\\\\")
          .replace("\t", "\\t")
          .replace("\b", "\\b")
          .replace("\n", "\\n")
          .replace("\r", "\\r")
          .replace("\f", "\\f")
          .replace("\'", "\\'")
          .replace("\"", "\\\"");
    }
    
    public String readable(String str) {
        return str.replace("\\\\", "\\")
          .replace("\\t",  "\t")
          .replace("\\b",  "\b")
          .replace("\\n",  "\n")
          .replace("\\r",  "\r")
          .replace("\\f",  "\f")
          .replace("\\'",  "\'");
          //.replace("\\""", "\"");
    }

    public String insan(String str) {
        if(str.equals("")) return "";
        else if(str.equals("__none__")) return "";
        else return str;
    }   

    public String formatNumber(double numb) {
        decimal = new DecimalFormat("###,###.###");
        String fides = decimal.format(numb);
        return fides;
    }
    
    public String rupiah(double rupiah) {
        return "Rp. " + this.formatNumber(rupiah);
    }
    
    public void toSendMail() {
        
    }
    
    protected SecretKeySpec secretKey;
    protected byte[] key;
 
    public void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); 
            secretKey = new SecretKeySpec(key, "AES");
        } 
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } 
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String plain_encrypt(String strToEncrypt) {
        try {
            this.setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public String plain_decrypt(String strToDecrypt) {
        try {
            this.setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    public String finalEncrypt(String str) {
        return this.plain_encrypt(this.plain_encrypt((str)));
    }

    public String finalDecrypt(String str) {
        return this.plain_decrypt(this.plain_decrypt(str));
    }
    
    public byte[] toMD5(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytesOfMessage = str.getBytes("UTF-8");
        md = MessageDigest.getInstance("MD5");
        byte[] result = md.digest(bytesOfMessage);
        return result;
    }
    
    public byte[] toSHA(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytesOfMessage = str.getBytes("UTF-8");
        md = MessageDigest.getInstance("SHA");
        byte[] result = md.digest(bytesOfMessage);
        return result;
    }
    
    public String bin2hex(String strCipherText) {
        char[] str_char = strCipherText.toCharArray();
        String str_biner = "";
        for(int i = 0; i < str_char.length; i++) {
            str_biner += Integer.toBinaryString(str_char[i]) + " ";
        }
        return str_biner;
    }
    
    public String hex2bin(String str_biner) {
        int charCode = Integer.parseInt(str_biner, 2);
        String str = new Character((char)charCode).toString();
        return str;
    }
    
    public String CharBy_One(char x) {
        try {
            switch(x) {
                case 'A':
                    x = 'B';
                    break;
                case 'B':
                    x = 'C';
                    break;
                case 'C':
                    x = 'D';
                    break;
                case 'D':
                    x = 'E';
                    break;
                case 'E':
                    x = 'F';
                    break;
                case 'F':
                    x = 'G';
                    break;
                case 'G':
                    x = 'H';
                    break;
                case 'H':
                    x = 'I';
                    break;
                case 'I':  
                    x = 'J';
                    break;
                case 'J':
                    x = 'K';
                    break;
                case 'K':
                    x = 'L';
                    break;
                case 'L':
                    x = 'M';
                    break;
                case 'M':
                    x = 'N';
                    break;
                case 'N':
                    x = 'O';
                    break;
                case 'O':
                    x = 'P';
                    break;
                case 'P':
                    x = 'Q';
                    break;
                case 'Q':
                    x = 'R';
                    break;
                case 'R':
                    x = 'S';
                    break;
                case 'S':
                    x = 'T';
                    break;
                case 'T':
                    x = 'U';
                    break;
                case 'U':
                    x = 'V';
                    break;
                case 'V':
                    x = 'W';
                    break;
                case 'W':
                    x = 'X';
                    break;
                case 'X':
                    x = 'Y';
                    break;
                case 'Y':
                    x = 'Z';
                    break;
                case 'Z':
                    x = 'A';
                    break;
                case 'a':
                    x = 'b';
                    break;
                case 'b':
                    x = 'c';
                    break;
                case 'c':
                    x = 'd';
                    break;
                case 'd':
                    x = 'e';
                    break;
                case 'e':
                    x = 'f';
                    break;
                case 'f':
                    x = 'g';
                    break;
                case 'g':
                    x = 'h';
                    break;
                case 'h':
                    x = 'i';
                    break;
                case 'i':  
                    x = 'j';
                    break;
                case 'j':
                    x = 'k';
                    break;
                case 'k':
                    x = 'l';
                    break;
                case 'l':
                    x = 'm';
                    break;
                case 'm':
                    x = 'n';
                    break;
                case 'n':
                    x = 'o';
                    break;
                case 'o':
                    x = 'p';
                    break;
                case 'p':
                    x = 'q';
                    break;
                case 'q':
                    x = 'r';
                    break;
                case 'r':
                    x = 's';
                    break;
                case 's':
                    x = 't';
                    break;
                case 't':
                    x = 'u';
                    break;
                case 'u':
                    x = 'v';
                    break;
                case 'v':
                    x = 'w';
                    break;
                case 'w':
                    x = 'x';
                    break;
                case 'x':
                    x = 'y';
                    break;
                case 'y':
                    x = 'z';
                    break;
                case 'z':
                    x = 'a';
                    break;
                case '0':
                    x = '1';
                    break;
                case '1':
                    x = '2';
                    break;
                case '2':
                    x = '3';
                    break;
                case '3':
                    x = '4';
                    break;
                case '4':
                    x = '5';
                    break;
                case '5':
                    x = '6';
                    break;
                case '6':
                    x = '7';
                    break;
                case '7':
                    x = '8';
                    break;
                case '8':  
                    x = '9';
                    break;
                case '9':
                    x = '0';
                    break;   
                                            
                default:
                    x = x;
            }
            return String.valueOf(x);
        }
        catch(Exception e) {
            return "";
            //return "function CharBy_One() Error: " + e.toString();
        }
    }
    
    public String setSession(String str) {
        return str;
    }
    
    public String getSession(String str) {
        return str;
    }
    
    public Integer randNumber(int min, int max) {
        try {
            int res = (int)(Math.random()*(max-min+1)+min);
            return res;
        }
        catch(Exception e) {
            //e.toString();
            return 0;
        }
    }
    
    public String randChar() {
        try {
            String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random rnd = new Random();
            char c = chars.charAt(rnd.nextInt(chars.length()));
            return String.valueOf(c);
        }
        catch(Exception e) {
            //e.toString();
            return "__NONE__";
        }
    }
    
    public String randSpecial() {
        try {
            String chars = "`~!@#$%^&*()_-+=[{}]|\\/;:,<.>?/'\"";
            Random rnd = new Random();
            char c = chars.charAt(rnd.nextInt(chars.length()));
            return String.valueOf(c);
        }
        catch(Exception e) {
            //e.toString();
            return "__NONE__";
        }
    }
    
    public String randHexColor() {
        try {
            String chars = "0123456789abcdef";
            Random rnd = new Random();
            char c = chars.charAt(rnd.nextInt(chars.length()));
            return String.valueOf(c);
        }
        catch(Exception e) {
            //e.toString();
            return "__NONE__";
        }
    }
    
    public String genPass(int length) {
        String pass = "";
        try {
            String chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@#$%^&*()_-+=[{}]|\\/;:,<.>?/'\"";
            Random rnd = new Random();
            for(int x = 0; x < length; x++) {
                char c = chars.charAt(rnd.nextInt(chars.length()));
                pass += c;
            }
            return pass;
        }
        catch(Exception e) {
            //e.toString();
            return pass;
        }
    }

    public String genToken(String namatoken) {
        try {
            String token = "@!#[" + namatoken + "]:=>";
            token += randNumber(1, 9) + "?" + randChar() + randChar() + randChar() + randSpecial() + randSpecial() + randSpecial() + "#";
            token += randNumber(10, 90) + "?" + randChar() + randChar() + randChar() + randChar() + randChar() + randSpecial() + randSpecial() + randSpecial() + randSpecial() + randSpecial() + "#";
            token += randNumber(100, 900) + "?" + randChar() + randChar() + randChar() + randChar() + randChar() + randChar() + randChar() + randSpecial() + randSpecial() + randSpecial() + randSpecial() + randSpecial() + randSpecial() + randSpecial();
            token += "?}:=>" + genPass(7);
            return token;
        }
        catch(Exception e) {
            //e.toString();
            return "__NONE__";
        }
    }
    
    public String splitTime(String tgl, String option) {
        try {
            String[] split_tgl = tgl.split("-");
            if(option.equals("tahun")) return split_tgl[0];
            else if(option.equals("bulan")) return split_tgl[1];
            else if(option.equals("tgl")) return split_tgl[2];
            else return "";
        }
        catch(Exception e) {
            return "";
        }
    }

    public int getTime(String tgl, String option) {
        String[] split_tgl = tgl.split("/");
        LocalDateTime myDateObj = LocalDateTime.of(Integer.parseInt(split_tgl[2]), Integer.parseInt(split_tgl[1]), Integer.parseInt(split_tgl[0]), 0, 0);
        //System.out.println("Before Formatting: " + myDateObj);
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        if(option.equals("tgl")) return myDateObj.getDayOfMonth();
        else if(option.equals("bulan")) return myDateObj.getMonthValue();
        else if(option.equals("tahun ")) return myDateObj.getYear();
        else return 0;
    }
    
    public String getLastnCharacters(String inputString, 
                                 int subStringLength){
        int length = inputString.length();
        if(length <= subStringLength){
            return inputString;
        }
        int startIndex = length-subStringLength;
        return inputString.substring(startIndex);
    }
    
}
