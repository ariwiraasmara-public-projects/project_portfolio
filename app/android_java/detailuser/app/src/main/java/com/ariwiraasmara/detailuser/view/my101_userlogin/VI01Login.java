package com.ariwiraasmara.detailuser.view.my101_userlogin;
/*
 * @author Ari Wiraasmara
 */

import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.ariwiraasmara.detailuser.R;
import com.ariwiraasmara.detailuser.controller.Controller101_UserLogin;
import com.ariwiraasmara.detailuser.controller.Controller102_Biodata;
import com.ariwiraasmara.detailuser.controller.Interfacecontroller102Biodata;
import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.model.Model102Biodata;
import com.ariwiraasmara.detailuser.util.Connection;
import com.ariwiraasmara.detailuser.util.MyFunction;
import com.ariwiraasmara.detailuser.view.my102_biodata.VI01Biodata;

import java.util.Arrays;
import java.util.List;

public class VI01Login extends AppCompatActivity {

    protected Model101UserLogin model101;
    protected Model102Biodata model102;
    protected Controller101_UserLogin cont101;
    protected Controller102_Biodata cont102;
    protected Connection conn;
    protected Context appContext;
    protected MyFunction fun;
    protected Intent setInt;
    protected Intent getInt;
    protected String token;

    protected EditText txtUserMail;
    protected EditText txtPass;
    protected TextView txtShowall;
    protected Button btnLogin;
    protected Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vi01_login);
        this.init();
        this.onClick();
    }

    public void init() {
        this.fun = new MyFunction();
        this.appContext = this.getApplicationContext();
        //model101 = new Model101UserLogin();
        /*
        try {
            if(getInt.getStringExtra("login").isEmpty()) this.token = this.fun.genToken("login");
            else this.token = getInt.getStringExtra("login").toString();
        }
        catch(Exception e) {
            this.token = this.fun.genToken("login");
        }
        */

        //this.cont101 = new Controller101_UserLogin(this.appContext, this.token);
        //this.cont102 = new Controller102_Biodata(this.appContext, this.token);
        this.txtUserMail = findViewById(R.id.viu101_txt_usermail);
        this.txtPass = findViewById(R.id.viu101_txt_password);
        this.txtShowall = findViewById(R.id.viu101_showall);
        this.btnLogin = findViewById(R.id.viu101_btn_login);
        this.btnRegister = findViewById(R.id.viu101_btn_register);

        try{
            this.conn = new Connection(this.appContext);
        }
        catch(Exception e) {
            System.out.println("Error Koneksi: " + e.toString());
            Toast.makeText(getBaseContext(), "Terjadi kesalahan!\n Koneksi ke Database Terputus!", Toast.LENGTH_LONG).show();
        }

        try {
            List<Model101UserLogin> data = this.conn.getAllUserLogin();

            //String data[] = new String[4];
            //data = this.conn.getDataUserLogin("");
            for(int x = 0; x < data.size(); x++) {
                txtShowall.append(data.get(x) + "\n");
            }
        }
        catch(Exception e) {
            System.out.println("Error Show All Data: " + e.toString());
            Toast.makeText(getBaseContext(), "Tidak Dapat Menampilkan Semua Data!", Toast.LENGTH_LONG).show();
        }
    }

    public void onClick() {
        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("test", "Test btnLogin onClick()");
                Toast.makeText(getBaseContext(), "Test btnLogin onClick()", Toast.LENGTH_LONG).show();

                String where = "(username='" + txtUserMail.getText().toString() + "' or email='" + txtUserMail.getText().toString() + "') and pass='" + txtPass.getText().toString() + "'";
                try{
                    int res = conn.checkRow(101, where, "");
                    if(res > 0) {
                        conn.getDataUserLogin(where);
                        //List<Model101UserLogin> data_login = conn.getDataUserLogin(where);
                        //String[] data_biodata = conn.getDataBiodata(" email='" + data_login[2] + "'");

                        //model101 = new Model101UserLogin(data_login[0], data_login[1], data_login[2], data_login[3]);
                        //model102.setNamauser(model101.getUser());
                        //model102.setEmail("");
                        //setInt = new Intent(appContext, VI01Biodata.class);
                        //setInt.putExtra("id_101", model101.getId());
                        //setInt.putExtra("username", model101.getUser());
                        //setInt.putExtra("email", model101.getEmail());
                        //setInt.putExtra("biodata", token);

                        System.out.println("id_101 : " + model101.getId());
                        System.out.println("username : " + model101.getUser());
                        System.out.println("email : " + model101.getEmail());
                        //startActivity(setInt);
                    }
                    else {
                        Toast.makeText(getBaseContext(), "Username / Email atau Password Salah!\nSilahkan Coba Lagi!", Toast.LENGTH_LONG).show();
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getBaseContext(), "Terjadi Kesalahan!\nGagal Login!", Toast.LENGTH_LONG).show();
                }

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInt = new Intent(appContext, VI02Register.class);
                setInt.putExtra("register", token);
                startActivity(setInt);
            }
        });
    }

}