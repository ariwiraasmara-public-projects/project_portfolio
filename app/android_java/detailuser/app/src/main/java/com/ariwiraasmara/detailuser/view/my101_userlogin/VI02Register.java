package com.ariwiraasmara.detailuser.view.my101_userlogin;
/*
 * @author Ari Wiraasmara
 */

import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.ariwiraasmara.detailuser.R;
import com.ariwiraasmara.detailuser.controller.Controller101_UserLogin;
import com.ariwiraasmara.detailuser.controller.Controller102_Biodata;
import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.model.Model102Biodata;
import com.ariwiraasmara.detailuser.util.Connection;
import com.ariwiraasmara.detailuser.util.MyFunction;

public class VI02Register extends AppCompatActivity {

    protected Model101UserLogin model101;
    protected Model102Biodata model102;
    protected Controller101_UserLogin cont101;
    protected Controller102_Biodata cont102;
    protected Connection conn;
    protected Context appContext;
    protected MyFunction fun;
    protected Intent setInt;
    protected Intent getInt;
    protected String token;

    protected EditText txtUser;
    protected EditText txtEmail;
    protected EditText txtPass;
    protected EditText txtPassver;
    protected Button btnDaftar;
    protected Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vi02_register);
        this.init();
        this.onClick();
    }

    public void init() {
        this.fun = new MyFunction();
        this.appContext = this.getApplicationContext();
        this.token = this.fun.genToken("register");
        /*
        if(this.token.isEmpty()) {
            Toast.makeText(this.appContext, "Tidak Bisa Melakukan Registrasi!", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        else {
            System.out.println("init initialize");
            try {

            }
            catch(Exception e) {}
            System.out.println("init variable view");
            System.out.println("init initialize");

        }
        System.out.println("init end");
         */

        this.conn = new Connection(this.appContext);
        //this.cont101 = new Controller101_UserLogin(this.appContext, this.fun.genToken("login"));
        //this.cont102 = new Controller102_Biodata(this.appContext, this.fun.genToken("register"));
        this.txtUser = findViewById(R.id.viu102_txt_username);
        this.txtEmail = findViewById(R.id.viu102_txt_email);
        this.txtPass = findViewById(R.id.viu102_txt_password);
        this.txtPassver = findViewById(R.id.viu102_txt_passver);
        this.btnDaftar = findViewById(R.id.viu102_btn_register);
        this.btnBack = findViewById(R.id.viu102_btn_back);
    }

    public void onClick() {
        this.btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = txtUser.getText().toString();
                String email = txtEmail.getText().toString();
                String pass = txtPass.getText().toString();
                String passver = txtPassver.getText().toString();
                if(passver.equals(pass)) {
                    String where = "username='" + user + "' or email='" + email + "'";
                    int cek = conn.checkRow(101, where, "");
                    if(cek > 0) {
                        Toast.makeText(getBaseContext(), "Pengguna Username/Email ini sudah ada!\nSilahkan daftar baru dengan Username/Email belum terdaftar!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        try {
                            String id_101 = conn.getLastIDUserLogin(where, "id_101 ASC", 3, 3, "0");
                            //String[] insert_101 = {id_101, user, email, pass};

                            String id_102 = conn.getLastIDBiodata("", "id_102 ASC", 3, 3, "0");
                            //String[] insert_102 = {id_102, "__noname__", email};

                            if( conn.insertDataUserLogin(id_101, user, email, pass) && conn.insertDataBiodata(id_102, "__noname__", email, "", "", "", "") ) {
                                conn.close();
                                Toast.makeText(getBaseContext(), "Pengguna Baru Berhasil Didaftarkan!", Toast.LENGTH_LONG).show();
                                fieldNull();
                            }
                            else {
                                Toast.makeText(getBaseContext(), "Gagal Menambahkan Pengguna Baru!", Toast.LENGTH_LONG).show();
                                System.out.println("ID 101: " + id_101 + "; ID 102: " + id_102);
                            }
                        }
                        catch(Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(), "Gagal Menambahkan Pengguna Baru! EX", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else {
                    Toast.makeText(getBaseContext(), "Password dan Verifikasi Password Tidak Sama!", Toast.LENGTH_LONG).show();
                }
            }
        });

        this.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void fieldNull() {
        txtUser.setText("");
        txtEmail.setText("");
        txtPass.setText("");
        txtPassver.setText("");
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        setInt = new Intent(getBaseContext(), VI01Login.class);
        setInt.putExtra("token", fun.genToken("login"));
        startActivity(setInt);
    }
}