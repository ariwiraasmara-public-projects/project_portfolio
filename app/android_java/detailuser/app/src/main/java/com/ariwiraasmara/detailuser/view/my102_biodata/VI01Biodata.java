package com.ariwiraasmara.detailuser.view.my102_biodata;
/*
 * @author Ari Wiraasmara
 */

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.ariwiraasmara.detailuser.R;
import com.ariwiraasmara.detailuser.controller.Controller101_UserLogin;
import com.ariwiraasmara.detailuser.controller.Controller102_Biodata;
import com.ariwiraasmara.detailuser.model.Model101UserLogin;
import com.ariwiraasmara.detailuser.model.Model102Biodata;
import com.ariwiraasmara.detailuser.util.Connection;
import com.ariwiraasmara.detailuser.util.MyFunction;
import com.ariwiraasmara.detailuser.view.my101_userlogin.VI04UserProfil;
import com.ariwiraasmara.detailuser.view.other.VI01Tutorial;
import com.ariwiraasmara.detailuser.view.other.VI02AboutMe;
public class VI01Biodata extends AppCompatActivity {

    protected Model101UserLogin model101;
    protected Model102Biodata model102;
    protected Controller101_UserLogin cont101;
    protected Controller102_Biodata cont102;
    protected Connection conn;
    protected Context appContext;
    protected MyFunction fun;
    protected Intent setInt;
    protected Intent getInt;
    protected String token;

    protected TextView txtWelcome;
    protected ListView listView;
    protected FloatingActionButton btnAdd;
    int exit = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vi01_biodata);
        this.init();
    }

    protected void init() {
        this.fun = new MyFunction();
        this.appContext = this.getApplicationContext();
        this.token = this.fun.genToken("register");
        if(this.token.isEmpty()) {
            Toast.makeText(this.appContext, "Tidak Bisa Melakukan Registrasi!", Toast.LENGTH_SHORT).show();
            exit = 2;
            onBackPressed();
        }
        else {
            try {
                this.conn = new Connection(appContext);
                this.model101 = new Model101UserLogin();
                this.cont101 = new Controller101_UserLogin(this.appContext, this.fun.genToken("login"));
                this.cont102 = new Controller102_Biodata(this.appContext, this.fun.genToken("register"));
            }
            catch(Exception e) {}
            txtWelcome = findViewById(R.id.vi04_txt_userwelcome);
            txtWelcome.setText(this.model101.getUser().toString());
            listView = findViewById(R.id.vi04_listview102_biodata);
            btnAdd = findViewById(R.id.vi04_btn_addbiodata);
        }
    }


    @Override
    public void onBackPressed() {
        exit++;
        if(exit == 2) {
            System.exit(0);
        }
        else {
            Toast.makeText(this.appContext, "Tekan lagi maka Anda akan keluar dari aplikasi", Toast.LENGTH_SHORT).show();
        }
        //super.onBackPressed();
    }

    protected void getTable() {

    }

    protected void onClick() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.vi04_menu_viewprofil){
            // do something
            setInt = new Intent(getBaseContext(), VI04UserProfil.class);
            setInt.putExtra("token", fun.genToken("profil"));
            startActivity(setInt);
        }
        else if(id == R.id.vi04_menu_refresh) {

        }
        else if(id == R.id.vi04_menu_tutorial) {
            setInt = new Intent(getBaseContext(), VI01Tutorial.class);
            setInt.putExtra("token", fun.genToken("tutorial"));
            startActivity(setInt);

        }
        else if(id == R.id.vi04_menu_aboutme) {
            setInt = new Intent(getBaseContext(), VI02AboutMe.class);
            setInt.putExtra("token", fun.genToken("aboutme"));
            startActivity(setInt);

        }
        else if(id == R.id.vi04_menu_logout) {

        }
        return super.onOptionsItemSelected(item);
    }
}