package com.ariwiraasmara.detail_user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.ariwiraasmara.detail_user.databinding.ActivityMainBinding
import com.ariwiraasmara.detail_user.ui.*
import com.ariwiraasmara.detail_user.util.Connection

class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_home, R.id.navigation_dashboard))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //val db: Connection= Connection(this)
    }

    public fun redirect(context: Context, str: String, extra: String = "") {
        var intento: Intent?
        if(str.equals("login")) intento = Intent(context, VI01Login::class.java)
        else if(str.equals("register")) intento = Intent(context, VI02Register::class.java)
        else if(str.equals("biodata")) intento = Intent(context, VI03Biodata::class.java)
        else if(str.equals("nebiodata")) {
            intento = Intent(context, VI03Biodata::class.java).apply {
                if( extra.equals("add") || extra.equals("edit") ) putExtra("add_edit", extra)
                else putExtra("add_edit", "add")
            }
        }
        else if(str.equals("profil")) intento = Intent(context, VI05Profil::class.java)
        else if(str.equals("editprofil")) intento = Intent(context, VI06EditPassword::class.java)
        else intento = Intent(context, VI01Login::class.java)

        startActivity(intento)
    }

}