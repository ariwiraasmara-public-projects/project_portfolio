package com.ariwiraasmara.detail_user.controller

import android.database.SQLException
interface InterfaceController101UserLogin {

    @Throws(SQLException::class)
    fun checkRow(where: String): Int

    @Throws(SQLException::class)
    fun getData(where: String): Array<String?>

    @Throws(SQLException::class)
    fun getLastID(where: String, order: String, digitangkaterakhir: Int, banyakpemisah_dalamangka: Int, pemisah: Char): String

    @Throws(SQLException::class)
    fun insertData(values: String, pass: String, passver: String): Int

    @Throws(SQLException::class)
    fun updateData(values: String, id: String): Int

    @Throws(SQLException::class)
    fun deleteData(id: String): Int

}