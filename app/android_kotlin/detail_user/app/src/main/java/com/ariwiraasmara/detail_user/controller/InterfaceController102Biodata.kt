package com.ariwiraasmara.detail_user.controller

import android.database.SQLException
interface InterfaceController102Biodata {

    @Throws(SQLException::class)
    fun checkRow(where: String, orderby: String): Int

    @Throws(SQLException::class)
    fun getData(where: String, orderby: String): Array<String>

    @Throws(SQLException::class)
    fun getLastID(where: String?, order: String?, digitangkaterakhir: Int, banyakpemisah_dalamangka: Int, pemisah: Char): String

    @Throws(SQLException::class)
    fun insertData(kolom: String, values: String): Int

    @Throws(SQLException::class)
    fun updateData(values: String, id: String): Int

    @Throws(SQLException::class)
    fun deleteData(id: String): Int

}