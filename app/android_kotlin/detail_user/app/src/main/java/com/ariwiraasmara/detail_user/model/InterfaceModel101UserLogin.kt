package com.ariwiraasmara.detail_user.model

interface InterfaceModel101UserLogin {

    fun logoutSystem()
    fun setID(str: String)
    fun getID(): String
    fun setUser(str: String)
    fun getUser(): String
    fun setEmail(str: String)
    fun getEmail(): String
    fun setPass(str: String)
    fun getPass(): String

}