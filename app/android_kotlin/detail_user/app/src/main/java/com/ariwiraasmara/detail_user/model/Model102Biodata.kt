package com.ariwiraasmara.detail_user.model

open class Model102Biodata(val id:String, val nama:String, val email:String, val tgl:String,
                           val alamat:String, val tlp:String, val status:String) /*: InterfaceModel102Biodata {

    constructor(_id:String, _nama:String,
                _email:String, _tgl:String,
                _alamat:String, _tlp:String, _status:String) {
        this.setID(_id)
        this.setNama(_nama)
        this.setEmail(_email)
        this.setTgl(_tgl)
        this.setAlamat(_alamat)
        this.setTlp(_tlp)
        this.setStatus(_status)
    }

    constructor() {

    }

    override fun logoutSystem() {

    }

    //
    protected var my102id: String = ""
    override fun setID(str: String) {
        this.my102id = str
    }

    override fun getID(): String {
        return this.my102id
    }

    //
    protected var my102nama: String = ""
    override fun setNama(str: String) {
        this.my102nama = str
    }

    override fun getNama(): String {
        return this.my102nama
    }

    //
    protected var my102email: String = ""
    override fun setEmail(str: String) {
        this.my102email = str
    }

    override fun getEmail(): String {
        return this.my102email
    }

    //
    protected var my102tgl: String = ""
    override fun setTgl(str: String) {
        this.my102tgl = str
    }

    override fun getTgl(): String {
        return this.my102tgl
    }

    //
    var my102alamat: String = ""
    override fun setAlamat(str: String) {
        this.my102alamat = str
    }

    override fun getAlamat(): String {
        return this.my102alamat
    }

    //
    var my102tlp: String = ""
    override fun setTlp(str: String) {
        this.my102tlp = str
    }

    override fun getTlp(): String {
        return this.my102tlp
    }

    //
    var my102status: String = ""
    override fun setStatus(str: String) {
        this.my102status = str
    }

    override fun getStatus(): String {
        return this.my102status
    }

}
*/