package com.ariwiraasmara.detail_user.model

open class ModelSG101UserLogin : InterfaceModel101UserLogin {

    private var my101id: String = ""
    private var my101user: String = ""
    private var my101email: String = ""
    private var my101pass: String = ""

    constructor()

    constructor(id:String, user:String,
                email:String, pass:String) {
        this.setID(id)
        this.setUser(user)
        this.setEmail(email)
        this.setPass(pass)
    }

    override fun logoutSystem() {

    }

    //
    override fun setID(str: String) {
        this.my101id = str
    }

    override fun getID(): String {
        return this.my101id
    }

    //
    override fun setUser(str: String) {
        this.my101user = str
    }

    override fun getUser(): String {
        return this.my101user
    }

    //
    override fun setEmail(str: String) {
        this.my101email = str
    }

    override fun getEmail(): String {
        return this.my101email
    }

    //
    override fun setPass(str: String) {
        this.my101pass = str
    }

    override fun getPass(): String {
        return this.my101pass
    }
}