package com.ariwiraasmara.detail_user.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Button
import android.widget.Toast
import com.ariwiraasmara.detail_user.MainActivity
import com.ariwiraasmara.detail_user.R
import com.ariwiraasmara.detail_user.model.Model101UserLogin
import com.ariwiraasmara.detail_user.model.ModelSG101UserLogin
import com.ariwiraasmara.detail_user.util.Connection

class VI01Login : AppCompatActivity() {

    lateinit var main: MainActivity
    lateinit var model101: Model101UserLogin
    lateinit var sg101: ModelSG101UserLogin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vi01_login)
        init()
    }

    private fun init() {
        main = MainActivity()
        val db: Connection = Connection(this)
        var txtUsermail = findViewById<EditText>(R.id.viu101_txt_usermail)
        var txtPassword = findViewById<EditText>(R.id.viu101_txt_password)
        var btnLogin = findViewById<Button>(R.id.viu101_btn_login)
        var btnRegister = findViewById<Button>(R.id.viu101_btn_register)

        btnLogin.setOnClickListener(View.OnClickListener {
            val count = db.checkLogin(txtUsermail.text.toString(), txtPassword.text.toString())
            if(count > 0) {
                val data: List<Model101UserLogin> = db.getUserLogin(txtUsermail.text.toString(), txtPassword.text.toString())
                //val sg101 = ModelSG101UserLogin(data[0].toString(), , , data[3].toString())
                val arrUser = Array<String>(data.size){"null"}
                val arrEmail = Array<String>(data.size){"null"}
                var index = 0
                for(d in data) {
                    arrUser[index] = d.username.toString()
                    arrEmail[index] = d.email.toString()
                    index++
                }

                //main.redirect(this, "biodata")
                var int = Intent(this, VI03Biodata::class.java).apply {
                    putExtra("username", arrUser[1].toString())
                    putExtra("email", arrEmail[2].toString())
                }
                startActivity(int)
            }
            else Toast.makeText(applicationContext,"Gagal Login!", Toast.LENGTH_LONG).show()
            /*
            val res = db.login(txtUsermail.text.toString(), txtPassword.text.toString())
            if(res > 0) {
                var int = Intent(this, MainActivity::class.java)
                startActivity(int)
            }
            else Toast.makeText(applicationContext,"Gagal Login!", Toast.LENGTH_LONG).show()
             */
        })

        btnRegister.setOnClickListener(View.OnClickListener {
            //var int = Intent(this, VI02Register::class.java)
            //startActivity(int)
            main.redirect(this, "register")
        })
    }
}