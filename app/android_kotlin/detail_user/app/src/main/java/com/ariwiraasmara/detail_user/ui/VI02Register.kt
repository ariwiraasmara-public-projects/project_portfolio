package com.ariwiraasmara.detail_user.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.ariwiraasmara.detail_user.R
import com.ariwiraasmara.detail_user.model.Model101UserLogin
import com.ariwiraasmara.detail_user.util.Connection

class VI02Register : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vi02_register)
        init()
    }

    private fun init() {
        var username = findViewById<EditText>(R.id.viu102_txt_username)
        var email = findViewById<EditText>(R.id.viu102_txt_email)
        var pass = findViewById<EditText>(R.id.viu102_txt_password)
        var passver = findViewById<EditText>(R.id.viu102_txt_passver)
        var btnRegister = findViewById<Button>(R.id.viu102_btn_register)
        var btnBack = findViewById<Button>(R.id.viu102_btn_back)

        btnRegister.setOnClickListener(View.OnClickListener {

            val res = this.register(username.text.toString(), email.text.toString(), pass.text.toString(), passver.text.toString())
            if(res > 0) {
                Toast.makeText(applicationContext,"Data Berhasil Disimpan!",Toast.LENGTH_LONG).show()
                username.text.clear()
                email.text.clear()
                pass.text.clear()
                passver.text.clear()
            }
            else Toast.makeText(applicationContext,"Gagal Menyimpan Data!",Toast.LENGTH_LONG).show()
        })

        btnBack.setOnClickListener(View.OnClickListener {
            var int = Intent(this, VI01Login::class.java)
            startActivity(int)
        })
    }

    private fun register(username: String, email: String, pass: String, passver: String): Long {
        val db: Connection = Connection(this)
        if(username.isNotEmpty() && email.isNotEmpty() && pass.isNotEmpty() && passver.isNotEmpty()) {
            if(passver.equals(pass)) {
                val id = "UL#001" //+db.getLastID101(email, pass, 3, 3, '0')
                val status = db.UserLogin("insert", id, username, email, pass)
                if(status > 0) return 1
                else return -1
            }
            else {
                Toast.makeText(this, "Password dan Verifikasi Password Harus Sama!", Toast.LENGTH_LONG)
                return -1
            }
        }
        else {
            Toast.makeText(this, "Data Tidak Boleh Kosong!", Toast.LENGTH_LONG)
            return -1
        }
    }
}