package com.ariwiraasmara.detail_user.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.ariwiraasmara.detail_user.MainActivity
import com.ariwiraasmara.detail_user.R
import com.ariwiraasmara.detail_user.model.Model101UserLogin
import com.ariwiraasmara.detail_user.model.Model102Biodata
import com.ariwiraasmara.detail_user.model.ModelSG101UserLogin
import com.ariwiraasmara.detail_user.util.Connection

class VI03Biodata : AppCompatActivity() {

    lateinit var connection: Connection
    lateinit var model101: Model101UserLogin
    lateinit var sg101: ModelSG101UserLogin
    lateinit var main: MainActivity
    lateinit var txtWelcome: TextView
    lateinit var viewList: ListView
    lateinit var btnAdd: ImageButton
    lateinit var btnProfil: ImageButton
    lateinit var emp: List<Model101UserLogin>
    private var user: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vi03_biodata)
        var bundle :Bundle? = intent.extras
        this.user = bundle?.get("username").toString() // 2
        this.init()
        this.derinit()
    }

    private fun init() {
        sg101 = ModelSG101UserLogin()
        connection = Connection(this)
        txtWelcome = findViewById<TextView>(R.id.vi03_txt_welcome)
        viewList = findViewById<ListView>(R.id.vi03_listview102_biodata)
        btnAdd = findViewById<ImageButton>(R.id.vi03_btn_addbiodata)
        btnProfil = findViewById<ImageButton>(R.id.vi03_btn_profil)
    }

    private fun derinit() {
        txtWelcome.text =  this.user

        btnProfil.setOnClickListener(View.OnClickListener {
            main.redirect(this, "profil")
        })
    }

}