package com.ariwiraasmara.detail_user.ui.home

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ariwiraasmara.detail_user.R
import com.ariwiraasmara.detail_user.databinding.FragmentDashboardBinding
import com.ariwiraasmara.detail_user.databinding.FragmentHomeBinding
import com.ariwiraasmara.detail_user.model.Model102Biodata
import com.ariwiraasmara.detail_user.ui.MyListAdapter
import com.ariwiraasmara.detail_user.ui.dashboard.DashboardViewModel
import com.ariwiraasmara.detail_user.util.Connection

class HomeFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    lateinit var vi04_listview102_biodata: ListView
    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        /*
        val textView: TextView = binding.textHome
        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        this.viewRecord(root)
         */

        val view = inflater.inflate(R.layout.fragment_home, container, false)
        vi04_listview102_biodata = root.findViewById<ListView>(R.id.vi04_listview102_biodata)
        viewRecord(root)

        return root
    }

    //method for read records from database in ListView
    fun viewRecord(view: View){
        //creating the instance of DatabaseHandler class
        val connection: Connection = Connection(view.context)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val emp: List<Model102Biodata> = connection.getBiodata()
        val empArrayID = Array<String>(emp.size){"null"}
        val empArrayNama = Array<String>(emp.size){"null"}
        val empArrayEmail = Array<String>(emp.size){"null"}
        val empArrayTgl = Array<String>(emp.size){"null"}
        val empArrayAlamat = Array<String>(emp.size){"null"}
        val empArrayTlp = Array<String>(emp.size){"null"}
        val empArrayStatus = Array<String>(emp.size){"null"}
        var index = 0
        for(e in emp){
            empArrayID[index] = e.id
            empArrayNama[index] = e.nama
            empArrayEmail[index] = e.email
            empArrayTgl[index] = e.tgl
            empArrayAlamat[index] = e.alamat
            empArrayTlp[index] = e.tlp
            empArrayStatus[index] = e.status
            index++
        }
        //creating custom ArrayAdapter
        val myListAdapter = MyListAdapter(view.context as Activity, empArrayID, empArrayNama, empArrayEmail, empArrayTgl, empArrayAlamat, empArrayTlp, empArrayStatus)
        vi04_listview102_biodata.adapter = myListAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}