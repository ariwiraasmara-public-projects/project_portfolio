package com.ariwiraasmara.detail_user.util

open class AbstractString : InterfaceAbstractString {

    protected val create_table: String = "CREATE TABLE IF NOT EXISTS ";
    protected val drop_table: String = "DROP TABLE IF EXISTS "

    protected val read_userlogin: String = "SELECT * from my101_user_login "
    protected val insert_userlogin: String = "INSERT into my101_user_login"
    protected val update_userlogin: String = "UPDATE my101_user_login set "
    protected val delete_userlogin: String = "DELETE FROM my101_user_login"

    protected val read_biodata: String = "SELECT * from my102_biodata "
    protected val insert_biodata: String = "INSERT into my102_biodata"
    protected val update_biodata: String = "UPDATE my102_biodata set "
    protected val delete_biodata: String = "DELETE FROM my102_biodata "

    protected val tvalues: String = " values "
    protected val twhere: String = " where "
    protected val torder: String = " order by "
    protected val id101: String = " id_101 "
    protected val id102: String = " id_102 "
    protected val nama: String = " nama "
    protected val email: String = " email "

    override fun createTable(): String {
        return "${this.create_table}"
    }

    override fun dropTable(): String {
        return "${this.drop_table}"
    }

    //
    override fun read_userlogin(where: String): String {
        val ftwhere = if(!where.isEmpty()) { this.twhere + where } else { null }
        return this.read_userlogin + ftwhere
    }

    //
    override fun insert_userlogin(values: String): String {
        return this.insert_userlogin + this.tvalues + values
    }

    //
    override fun update_userlogin(values: String, id: String): String {
        return this.update_userlogin + values + this.twhere + this.id101 + "='" + id + "' or " + this.email + " ='" + id + "'"
    }

    //
    override fun delete_userlogin(id: String): String {
        return this.delete_userlogin + this.twhere + this.id101 + "='" + id + "' or " + this.email + " ='"+id+"'";
    }

    //
    override fun read_biodata(where: String, orderby: String): String {
        val ftwhere = if(!where.isEmpty()) { this.twhere + where } else { null }
        val ftorder = if(!orderby.isEmpty()) { this.torder + orderby } else { null }
        return this.read_biodata + ftwhere + ftorder;
    }

    //
    override fun insert_biodata(kolom: String, values: String): String {
        val ftkolom = if(!kolom.isEmpty()) { "(" + kolom + ")" } else { null }
        return this.insert_biodata + ftkolom + this.tvalues + values;
    }

    //
    override fun update_biodata(values: String, id: String): String {
        return this.update_biodata + values + this.twhere + this.id102 + "='" + id + "' or " + this.email + " ='" + id + "'"
    }

    //
    override fun delete_biodata(id: String): String {
        return this.delete_biodata + this.twhere + this.nama + "='" + id + "' or " + this.email + " ='"+id+"'";
    }

}