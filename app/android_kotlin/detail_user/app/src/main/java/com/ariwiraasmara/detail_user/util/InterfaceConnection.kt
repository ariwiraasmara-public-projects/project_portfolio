package com.ariwiraasmara.detail_user.util

import com.ariwiraasmara.detail_user.model.Model101UserLogin
import com.ariwiraasmara.detail_user.model.Model102Biodata

interface Interfaceconnection {

    fun login(email: String, password: String): Int
    fun getUserLogin(email: String, password: String): Model101UserLogin
    fun UserLogin(type: String, model: Model101UserLogin): Long
    fun getBiodata(email: String): Model102Biodata
    fun Biodata(type: String, model: Model102Biodata): Boolean
    fun getLastID(table: Int, where: String?, order: String?, digitangkaterakhir: Int, banyakpemisah_dalamangka: Int, pemisah: Char): String

}