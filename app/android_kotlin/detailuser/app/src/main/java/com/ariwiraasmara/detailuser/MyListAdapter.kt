package com.ariwiraasmara.detail_user.ui

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.ariwiraasmara.detail_user.R

class MyListAdapter(private val context: Activity, private val id: Array<String>, private val nama: Array<String>, private val email: Array<String>,
                    private val tgl: Array<String>, private val alamat: Array<String>, private val tlp: Array<String>, private val status: Array<String>)
    : ArrayAdapter<String>(context, R.layout.listview102_biodata, nama) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.listview102_biodata, null, true)

        val txt_nama = rowView.findViewById(R.id.vi04_txt_nama) as TextView
        val txt_email = rowView.findViewById(R.id.vi04_txt_email) as TextView
        val txt_tgl = rowView.findViewById(R.id.vi04_txt_tgl) as TextView
        val txt_alamat = rowView.findViewById(R.id.vi04_txt_alamat) as TextView
        val txt_tlp = rowView.findViewById(R.id.vi04_txt_tlp) as TextView
        val txt_status = rowView.findViewById(R.id.vi04_txt_status) as TextView

        txt_nama.text = "Id: ${nama[position]}"
        txt_email.text = "Name: ${email[position]}"
        txt_tgl.text = "Email: ${tgl[position]}"
        txt_alamat.text = "Id: ${alamat[position]}"
        txt_tlp.text = "Name: ${tlp[position]}"
        txt_status.text = "Email: ${status[position]}"
        return rowView
    }

}