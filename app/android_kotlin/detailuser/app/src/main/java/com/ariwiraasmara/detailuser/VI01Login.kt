package com.ariwiraasmara.detail_user.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Button
import android.widget.Toast
import com.ariwiraasmara.detail_user.MainActivity
import com.ariwiraasmara.detail_user.R
import com.ariwiraasmara.detail_user.util.Connection

class VI01Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vi01_login)
        init()
    }

    private fun init() {
        val db: Connection = Connection(this)
        var txtUsermail = findViewById<EditText>(R.id.viu101_txt_usermail)
        var txtPassword = findViewById<EditText>(R.id.viu101_txt_password)
        var btnLogin = findViewById<Button>(R.id.viu101_btn_login)
        var btnRegister = findViewById<Button>(R.id.viu101_btn_register)

        btnLogin.setOnClickListener(View.OnClickListener {
            val db: Connection = Connection(this)
            db.getUserLogin(txtUsermail.text.toString(), txtPassword.text.toString())
            if(db.getResLogin() > 0) {
                var int = Intent(this, MainActivity::class.java)
                startActivity(int)
            }
            else Toast.makeText(applicationContext,"Gagal Login!", Toast.LENGTH_LONG).show()
            /*
            val res = db.login(txtUsermail.text.toString(), txtPassword.text.toString())
            if(res > 0) {
                var int = Intent(this, MainActivity::class.java)
                startActivity(int)
            }
            else Toast.makeText(applicationContext,"Gagal Login!", Toast.LENGTH_LONG).show()
             */
        })

        btnRegister.setOnClickListener(View.OnClickListener {
            var int = Intent(this, VI02Register::class.java)
            startActivity(int)
        })
    }
}