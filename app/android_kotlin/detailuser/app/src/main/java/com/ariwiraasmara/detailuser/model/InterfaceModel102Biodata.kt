package com.ariwiraasmara.detail_user.model

interface InterfaceModel102Biodata {

    fun logoutSystem()
    fun setID(str: String)
    fun getID(): String
    fun setNama(str: String)
    fun getNama(): String
    fun setEmail(str: String)
    fun getEmail(): String
    fun setTgl(str: String)
    fun getTgl(): String
    fun setAlamat(str: String)
    fun getAlamat(): String
    fun setTlp(str: String)
    fun getTlp(): String
    fun setStatus(str: String)
    fun getStatus(): String

}