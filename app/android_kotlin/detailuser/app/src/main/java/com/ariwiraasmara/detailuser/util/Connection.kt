package com.ariwiraasmara.detail_user.util


import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.ariwiraasmara.detail_user.model.Model101UserLogin
import com.ariwiraasmara.detail_user.model.Model102Biodata
import java.lang.Integer.parseInt


class Connection(context: Context): SQLiteOpenHelper(context, Connection.dbName, null, Connection.dbVersion) {

    //val abstractString: AbstractString = AbstractString()

    companion object {
        private const val dbName = "db_contoh_myportfolio"
        private const val dbVersion = 1
        private const val createTable = "CREATE TABLE IF NOT EXISTS"
        private const val dropTable = "DROP TABLE IF EXISTS"

        private const val table101 = "my101_userlogin"
        private const val my101id1 = "id_101"
        private const val my101username = "username"
        private const val my101email = "email"
        private const val my101pass = "password"

        private const val table102 = "my102_biodata"
        private const val my102id = "id_102"
        private const val my102nama = "nama"
        private const val my102email = "email"
        private const val my102tgl = "tgl_lahir"
        private const val my102alamat = "alamat"
        private const val my102tlp = "tlp"
        private const val my102status = "status"
    }

    override fun onCreate(db: SQLiteDatabase) {
        val createTable101 = ("$createTable $table101 (" +
                             "$my101id1 varchar(255) not null primary key," +
                             "$my101username varchar(255) not null," +
                             "$my101email varchar(255) not null," +
                             "$my101pass text not null" +
                             ")")

        val createTable102 = ("$createTable $table102 (" +
                              "$my102id varchar(255) not null primary key," +
                              "$my102nama varchar(255) not null," +
                              "$my102email varchar(255) not null," +
                              "$my102tgl text null" +
                              "$my102alamat text null," +
                              "$my102tlp text null," +
                              "$my102status text null" +
                              ")")


        //db?.execSQL(this.table(createTable, this.tfGen(101, 1), this.tfGen(101, 2)))
        //db?.execSQL(this.table(createTable, this.tfGen(102, 1), this.tfGen(102, 2)))
        db.execSQL(createTable101)
        db.execSQL(createTable102)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        //db!!.execSQL(this.table(dropTable, this.tfGen(101, 1)))
        //db!!.execSQL(this.table(dropTable, this.tfGen(102, 1)))
        db.execSQL("$dropTable table101")
        db.execSQL("$dropTable table102")
        onCreate(db)
    }

    private fun table(option: String, table: String, fields: String): String {
        return "$option $table ( $fields );"
    }


    private  fun tfGen(kode: Int, type: Int): String {
        return if(kode == 101 && type == 1) {
            table101
        }
        else if(kode == 101 && type == 2) {
            "$my101id1 varchar(255) not null primary key, " +
            "$my101username varchar(255) not null unique, " +
            "$my101email varchar(255) not null unique, " +
            "$my101pass text not null"
        }
        else if(kode == 102 && type == 1) {
            table102
        }
        else if(kode == 102 && type == 2) {
            "$my102id varchar(255) not null primary key,\n" +
            "$my102nama varchar(255) not null,\n" +
            "$my102email varchar(255) not null unique,\n" +
            "$my102tgl date not null,\n" +
            "$my102alamat text,\n" +
            "$my102tlp text,\n" +
            "$my102status text"
        }
        else {
            ""
        }
    }

    /*
    //
    @SuppressLint("Range")
    fun checkRow(email: String, password: String): Int {
        var xc: Int = 0
        try {
            //val tasks = Model101UserLogin()
            val db = readableDatabase
            //val selectQuery = abstractString.read_userlogin("email='$email' and password='$password'")
            val selectQuery = "SELECT * from my101_userlogin where email='$email' and password='$password'"
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor != null) {
                cursor.moveToFirst()
                while (cursor.moveToNext()) {
                    xc = cursor.getInt(0)
                    //tasks.setID(cursor.getString(cursor.getColumnIndex("id_101")))
                    //tasks.setNama(cursor.getString(cursor.getColumnIndex("username")))
                    //tasks.setEmail(cursor.getString(cursor.getColumnIndex("email")))
                    //tasks.setPass(cursor.getString(cursor.getColumnIndex("password")))
                }
            }
            Log.d("SUCCESS", "Data Cek User!")
            cursor.close()
        }
        catch (e: Exception) {
            e.printStackTrace()

        }
        return xc
    }

    @SuppressLint("Range")
    fun getUserLogin(email: String, password: String, order: String): Model101UserLogin { //List<Model101UserLogin> {
        val tasks = Model101UserLogin()
        try {
            //val model: ArrayList<Model101UserLogin> = ArrayList<Model101UserLogin>()

            val db = readableDatabase
            //val selectQuery = abstractString.read_userlogin("email='$email'")
            val selectQuery = "SELECT * from my101_userlogin where email='$email' and password='$password' $order"
            var rescur: Cursor? = null
            try{
                rescur = db.rawQuery(selectQuery, null)
            }catch (e: SQLiteException) {
                db.execSQL(selectQuery)
                return tasks //ArrayList()
            }

            if (rescur.moveToFirst()) {
                do {
                    val resmod = Model101UserLogin(rescur.getString(rescur.getColumnIndex("id_101")),
                        rescur.getString(rescur.getColumnIndex("username")),
                        rescur.getString(rescur.getColumnIndex("email")),
                        rescur.getString(rescur.getColumnIndex("password"))
                    )
                } while (rescur.moveToNext())
            }
            Log.d("SUCCESS", "Data User Berhasil Didapatkan!")
            rescur.close()
        }
        catch(e:Exception) {
            e.printStackTrace()
        }
        return tasks
    }
     */

    //method to read data
    @SuppressLint("Range")
    fun getUserLogin(usermail: String, pass: String?):Int { //List<Model101UserLogin>{
        var cin = 0
        val empList:ArrayList<Model101UserLogin> = ArrayList<Model101UserLogin>()
        val selectQuery = "SELECT  * FROM $table101 where (username='$usermail' or email='$usermail') and password='$pass'"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return cin //ArrayList()
        }
        if (cursor.moveToFirst()) {
            do {
                val emp = Model101UserLogin(id = cursor.getString(cursor.getColumnIndex("username")),
                                            username = cursor.getString(cursor.getColumnIndex("username")),
                                            email = cursor.getString(cursor.getColumnIndex("email")),
                                            pass = cursor.getString(cursor.getColumnIndex("password")))
                empList.add(emp)
            } while (cursor.moveToNext())
            cin = 1
        }
        return cin //empList
    }

    //
    fun UserLogin(type: String, id: String, username: String, email: String, pass: String): Long {
        try {
            val db = this.writableDatabase
            val values = ContentValues()
            if(type.equals("insert")) {
                values.put("id_101", id)
                values.put("username", username)
                values.put("email", email)
                values.put("password", pass)
                val success = db.insert("my101_userlogin", null, values)
                db.close()
                Log.d("SUCCESS", "Data User Berhasil Disimpan!")
                return success
            }
            else if(type.equals("update")) {
                values.put("password", pass)
                val success = db.update(this.tfGen(101, 1), values, "email="+email, null).toLong()
                db.close()
                Log.d("SUCCESS", "Data User Berhasil Diperbaharui!")
                return success
            }
            else if(type.equals("delete")) {
                val success = db.delete(this.tfGen(101, 1), "email="+email, null).toLong()
                db.close()
                Log.d("SUCCESS", "Data User Berhasil Dihapus!")
                return success
            }
            else return -1
        }
        catch(e: Exception) {
            e.printStackTrace()
            return -1
        }
    }

    //
    /*
    @SuppressLint("Range")
    fun getBiodata(email: String): Model102Biodata {
        val tasks = Model102Biodata()
        val db = writableDatabase
        //val selectQuery = abstractString.read_userlogin("email='$email'")
        val selectQuery = ""
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor != null) {
            cursor.moveToFirst()
            while (cursor.moveToNext()) {
                tasks.setID(cursor.getString(cursor.getColumnIndex("id_101")))
                tasks.setNama(cursor.getString(cursor.getColumnIndex("username")))
                tasks.setEmail(cursor.getString(cursor.getColumnIndex("email")))
                tasks.setTgl(cursor.getString(cursor.getColumnIndex("tgl_lahir")))
                tasks.setAlamat(cursor.getString(cursor.getColumnIndex("alamat")))
                tasks.setTlp(cursor.getString(cursor.getColumnIndex("tlp")))
                tasks.setStatus(cursor.getString(cursor.getColumnIndex("status")))
            }
        }
        cursor.close()
        return tasks
    }

    //
    fun Biodata(type: String, model: Model102Biodata): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        if(type.equals("insert")) {
            values.put("id_101", model.getID())
            values.put("nama", model.getNama())
            values.put("email", model.getEmail())
            values.put("tgl_lahir", model.getTgl())
            values.put("alamat", model.getAlamat())
            values.put("tlp", model.getTlp())
            values.put("status", model.getStatus())
            val _success = db.insert(this.tfGen(101, 1), null, values)
            db.close()
            return (Integer.parseInt("$_success") != -1)
        }
        else if(type.equals("update")) {
            values.put("nama", model.getNama())
            values.put("tgl_lahir", model.getTgl())
            values.put("alamat", model.getAlamat())
            values.put("tlp", model.getTlp())
            values.put("status", model.getStatus())
            val _success = db.update(this.tfGen(101, 1), values, "email=?", arrayOf(model.getEmail())).toLong()
            db.close()
            return Integer.parseInt("$_success") != -1
        }
        else if(type.equals("delete")) {
            val _success = db.delete(this.tfGen(101, 1), "email=?", arrayOf(model.getEmail())).toLong()
            db.close()
            return Integer.parseInt("$_success") != -1
        }
        else return false
    }
     */

    @SuppressLint("Range")
    fun getBiodata():List<Model102Biodata>{
        val empList:ArrayList<Model102Biodata> = ArrayList<Model102Biodata>()
        val selectQuery = "SELECT  * FROM $table102"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        if (cursor.moveToFirst()) {
            do {
                val emp = Model102Biodata(id = cursor.getString(cursor.getColumnIndex("username")),
                    nama = cursor.getString(cursor.getColumnIndex("nama")),
                    email = cursor.getString(cursor.getColumnIndex("email")),
                    tgl = cursor.getString(cursor.getColumnIndex("tgl_lahir")),
                    alamat = cursor.getString(cursor.getColumnIndex("alamat")),
                    tlp = cursor.getString(cursor.getColumnIndex("tlp")),
                    status = cursor.getString(cursor.getColumnIndex("status"))
                )
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }

    //
    fun checkRow(table: String, where: String, order: String): Int {
        var row = 0
        val selectQuery = "SELECT  * FROM $table $where $order"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(selectQuery, null)
            db.execSQL(selectQuery)
        }
        catch(e: SQLiteException) {
            db.execSQL(selectQuery)
        }
        if (cursor != null) {
            if (cursor.moveToFirst()) row = 1
        }
        return row
    }

    //
    /*
    fun getLastID101(email: String, password: String, where: String, order: String, digitangkaterakhir: Int, banyakpemisah_dalamangka: Int, pemisah: Char): String {
        var intaid: String? = null
        try {
            //val clause = "SELECT * from my101_userlogin order by id_101 DESC limit 1"
            val cek: Int = this.checkRow("my101_userlogin", where, order)

            if (cek > 0) {
                val data: List<Model101UserLogin> = this.getUserLogin(email, password, "order by id_101 DESC")
                val graid: Int = parseInt(data.substring(digitangkaterakhir)) + 1
                intaid = graid.toString().padStart(banyakpemisah_dalamangka, pemisah)
            } else {
                intaid = "1".padStart(banyakpemisah_dalamangka, pemisah)
            }
            return "UL#$intaid"
        }
        catch(e: Exception) {
            e.printStackTrace()
            return ""
        }
    }
     */

}
