package com.ariwiraasmara.detail_user.util

interface InterfaceAbstractString {

    fun createTable(): String
    fun dropTable(): String
    fun read_userlogin(where: String): String
    fun insert_userlogin(values: String): String
    fun update_userlogin(values: String, id: String): String
    fun delete_userlogin(id: String): String
    fun read_biodata(where: String, orderby: String): String
    fun insert_biodata(kolom: String, values: String): String
    fun update_biodata(values: String, id: String): String
    fun delete_biodata(id: String): String

}