<!DOCTYPE html>
<html lang="id">
    <head>
        <title>App Contoh - User Detail</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<meta name="keywords" content="Syahri Ramadhan Wiraasmara's Private Website, Personal Use Only" />

		<meta content="IE=edge" http-equiv="x-ua-compatible">
		<meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
		<meta content="yes" name="apple-mobile-web-app-capable">
		<meta content="yes" name="apple-touch-fullscreen">
    
        <style>
            body {
                background: #c0c0ff;
                color: #fff;
                padding: 30px;
                font-family: Georgia, 'Times New Roman', Times, serif;
            }

            a {
                width: auto;
                border: 1px solid #999;
                border-radius: 10px;
                background: #c0c0c0;
                margin-top: 30px;
                font-size: 20px;
                font-weight: bold;
                text-align: center;
                color: #000;
                padding: 20px;
                display: block;
            }
        </style>
    </head>

    <body>
        <a href="php_native/detail_user/app/">User Detail - Native PHP</a>
        <a href="php_laravel/detail_user/public/login">User Detail - PHP Laravel</a>
    </body>
</html>