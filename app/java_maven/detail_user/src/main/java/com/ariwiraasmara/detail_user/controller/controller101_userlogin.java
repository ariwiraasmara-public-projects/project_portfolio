package com.ariwiraasmara.detail_user.controller;
/*
 * @author Ari Wiraasmara
 */


import com.ariwiraasmara.detail_user.util.connection;
import com.ariwiraasmara.detail_user.util.abstractstring;
import com.ariwiraasmara.detail_user.controller.interfacecontroller101_userlogin;
import com.ariwiraasmara.detail_user.model.model101_userlogin;
import com.google.common.base.Strings;
import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
public class controller101_userlogin implements interfacecontroller101_userlogin {
    
    protected connection koneksi;
    protected abstractstring query;
    protected model101_userlogin model;
    
    protected Connection conn = null;
    protected ResultSet rs = null;
    protected PreparedStatement perintahSQL = null;
    
    public controller101_userlogin() throws SQLException {
        this.koneksi        = new connection() {};
        this.query          = new abstractstring();
        this.model          = new model101_userlogin();
        this.conn           = koneksi.getConnection();
        //this.perintahSQL    = conn.createStatement();
    }
    
    public int checkRow(String where) throws SQLException {
        int res = 0;
        this.perintahSQL = this.conn.prepareStatement(this.query.read_userlogin(where));
        this.rs = this.perintahSQL.executeQuery();
        while(this.rs.next()) {
            res = this.rs.getRow();
         }
        return res;
    }
    
    public String[] getData(String where) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.read_userlogin(where));
        this.rs = this.perintahSQL.executeQuery();
        String data[] = new String[4];
        while(this.rs.next()) {
            data[0] = this.rs.getString(1);
            data[1] = this.rs.getString(2);
            data[2] = this.rs.getString(3);
            data[3] = this.rs.getString(4);
        }
        return data;
    }
    
    public String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, char pemisah) throws SQLException {
        String intaid = null;
        String clause = where + " order by " + order + " limit 1";
        int cek = this.checkRow(where);
        if(cek > 0) {
            String[] data = this.getData(where);
            int graid = parseInt(data[0].substring(digitangkaterakhir)) + 1;
            String sgraid = String.valueOf(graid);
            intaid = Strings.padStart(sgraid, banyakpemisah_dalamangka, pemisah);
        }
        else {
            intaid = Strings.padStart("1", banyakpemisah_dalamangka, pemisah);
        }
        return "UL#" + intaid;
    }
    
    public boolean insertData(String values, String pass, String passver) throws SQLException {
        if(pass.equals(passver)) {
            this.perintahSQL = this.conn.prepareStatement(this.query.insert_userlogin(values));
            this.perintahSQL.executeUpdate();
            return true;
        }
        else {
            return false;
        }
    }
    
    public boolean updateData(String values, String id) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.update_userlogin(values, id));
        this.perintahSQL.executeUpdate();
        return true;
    }
    
    public boolean deleteData(String id) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.delete_userlogin(id));
        this.perintahSQL.executeUpdate();
        return false;
    }
}
