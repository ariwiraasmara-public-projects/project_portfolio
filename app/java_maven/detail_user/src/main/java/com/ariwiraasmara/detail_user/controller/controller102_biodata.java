package com.ariwiraasmara.detail_user.controller;
/*
 * @author Ari Wiraasmara
 */

import com.ariwiraasmara.detail_user.util.connection;
import com.ariwiraasmara.detail_user.util.abstractstring;
import com.ariwiraasmara.detail_user.model.model102_biodata;
import com.ariwiraasmara.detail_user.util.myfunction;
import com.google.common.base.Strings;
import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import javax.swing.table.DefaultTableModel;
public class controller102_biodata implements interfacecontroller102_biodata {
    
    protected connection koneksi;
    protected abstractstring query;
    protected model102_biodata model;
    
    protected Connection conn = null;
    protected ResultSet rs = null;
    protected PreparedStatement perintahSQL = null;
    
    public controller102_biodata() throws SQLException {
        this.koneksi        = new connection() {};
        this.query          = new abstractstring() {};
        this.model          = new model102_biodata();
        this.conn           = koneksi.getConnection();
        //this.perintahSQL    = conn.createStatement();
    }

    public int checkRow(String where, String orderby) throws SQLException {
        int res = 0;
        this.perintahSQL = this.conn.prepareStatement(this.query.read_biodata(where, orderby));
        this.rs = this.perintahSQL.executeQuery();
        while(this.rs.next()) {
            res = this.rs.getRow();
         }
        return res;
    }
    
    public String[] getData(String where, String orderby) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.read_biodata(where, orderby));
        this.rs = this.perintahSQL.executeQuery();
        String data[] = new String[7];
        while(this.rs.next()) {
            data[0] = this.rs.getString(1);
            data[1] = this.rs.getString(2);
            data[2] = this.rs.getString(3);
            data[3] = this.rs.getString(4);
            data[4] = this.rs.getString(5);
            data[5] = this.rs.getString(6);
            data[6] = this.rs.getString(7);
        }
        return data;
    }
    
    public DefaultTableModel viewTable(String[] judul, String where, String orderby) throws SQLException {
        DefaultTableModel kolom = new DefaultTableModel(null, judul);
        this.perintahSQL = this.conn.prepareStatement(this.query.read_biodata(where, orderby));
        this.rs = this.perintahSQL.executeQuery();
        String data[] = new String[7];
        int nomor = 1;
        while(this.rs.next()) {
            data[0] = String.valueOf(nomor);
            data[1] = this.rs.getString(2);
            data[2] = this.rs.getString(3);
            data[3] = this.rs.getString(4);
            data[4] = this.rs.getString(5);
            data[5] = this.rs.getString(6);
            data[6] = this.rs.getString(7);
            kolom.addRow(data);
            nomor++;
        }
        return kolom;
    }
    
    public String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, char pemisah) throws SQLException {
        String intaid = null;
        //String clause = " order by " + order + " limit 1";
        int cek = this.checkRow(where, "");
        if(cek > 0) {
            String[] data = this.getData(where, order);
            int graid = parseInt(data[0].substring(digitangkaterakhir)) + 1;
            String sgraid = String.valueOf(graid);
            intaid = Strings.padStart(sgraid, banyakpemisah_dalamangka, pemisah);
        }
        else {
            intaid = Strings.padStart("1", banyakpemisah_dalamangka, pemisah);
        }
        return "BI#" + intaid;
    }
    
    public boolean insertData(String kolom, String values) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.insert_biodata(kolom, values));
        this.perintahSQL.executeUpdate();
        return true;
    }
    
    public boolean updateData(String values, String id) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.update_biodata(values, id));
        this.perintahSQL.executeUpdate();
        return true;
    }
    
    public boolean deleteData(String id) throws SQLException {
        this.perintahSQL = this.conn.prepareStatement(this.query.delete_biodata(id));
        this.perintahSQL.executeUpdate();
        return true;
    }
}
