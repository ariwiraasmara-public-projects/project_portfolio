package com.ariwiraasmara.detail_user.controller;
/*
 * @author Ari Wiraasmara
 */

import java.sql.SQLException;
public abstract interface interfacecontroller101_userlogin {
    public abstract int checkRow(String where) throws SQLException;
    public abstract String[] getData(String where) throws SQLException;
    public abstract String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, char pemisah) throws SQLException;
    public abstract boolean insertData(String values, String pass, String passver) throws SQLException;
    public abstract boolean updateData(String values, String id) throws SQLException;
    public abstract boolean deleteData(String id) throws SQLException;
}
