package com.ariwiraasmara.detail_user.controller;
/*
 * @author Ari Wiraasmara
 */

import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
public abstract interface interfacecontroller102_biodata {
    public abstract int checkRow(String where, String orderby) throws SQLException;
    public abstract String[] getData(String where, String orderby) throws SQLException;
    public abstract DefaultTableModel viewTable(String[] judul, String where, String orderby) throws SQLException;
    public abstract String getLastID(String where, String order, int digitangkaterakhir, int banyakpemisah_dalamangka, char pemisah) throws SQLException;
    public abstract boolean insertData(String kolom, String values) throws SQLException;    
    public abstract boolean updateData(String values, String id) throws SQLException;
    public abstract boolean deleteData(String id) throws SQLException;
}
