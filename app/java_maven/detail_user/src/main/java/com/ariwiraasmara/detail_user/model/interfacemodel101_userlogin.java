package com.ariwiraasmara.detail_user.model;
/*
 * @author Ari Wiraasmara
 */

public abstract interface interfacemodel101_userlogin {
    public abstract void logoutSystem();
    public abstract String getId_101();
    public abstract void setId_101(String id_101);
    public abstract String getUser();
    public abstract void setUser(String user);
    public abstract String getEmail();
    public abstract void setEmail(String email);
    public abstract String getPass();
    public abstract void setPass(String pass);
}
