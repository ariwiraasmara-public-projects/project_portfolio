package com.ariwiraasmara.detail_user.model;
/*
 * @author Ari Wiraasmara
 */

public abstract interface interfacemodel102_biodata {
    public abstract void logoutSystem();
    public abstract String getNamaUser();
    public abstract void setNamaUser(String namauser);
    public abstract String getId();
    public abstract void setId(String id);
    public abstract String getNama();
    public abstract void setNama(String nama);
    public abstract String getEmail();
    public abstract void setEmail(String email);
    public abstract String getTgl();
    public abstract void setTgl(String tgl);
    public abstract String getAlamat();
    public abstract void setAlamat(String alamat);
    public abstract String getTlp();
    public abstract void setTlp(String tlp);
    public abstract String getStatus();
    public abstract void setStatus(String status);
    public abstract String getnebiodataLabel();
    public abstract void setnebiodataLabel(String nebiodatalabel);
}
