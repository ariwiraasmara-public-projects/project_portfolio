package com.ariwiraasmara.detail_user.model;
/*
 * @author Ari Wiraasmara
 */

public abstract interface interfacemodel103_token {
    public String getLogin();
    public String getRegister();
    public String getProfil();
    public String getUpdatePassword();
    public String getBiodata();
    public String getNEBiodata();
}
