package com.ariwiraasmara.detail_user.model;
/*
 * @author Ari Wiraasmara
 */

import com.ariwiraasmara.detail_user.util.myfunction;
public class model101_userlogin implements interfacemodel101_userlogin {
    
    protected static String id_101;
    protected static String user;
    protected static String email;
    protected static String pass;
    protected myfunction fun;
    
    public model101_userlogin() {
        fun = new myfunction();
    }
    
    public model101_userlogin(String id_101, String user, String email, String pass) {
        fun = new myfunction();
        this.setId_101(id_101);
        this.setUser(user);
        this.setEmail(email);
        this.setPass(pass);
    }
    
    public void logoutSystem() {
        model101_userlogin.id_101 = null;
        model101_userlogin.user = null;
        model101_userlogin.email = null;
        model101_userlogin.pass = null;
    }

    public final String getId_101() {
        return model101_userlogin.id_101;
    }

    public void setId_101(String id_101) {
        model101_userlogin.id_101 = fun.plain_encrypt(id_101);
    }

    public final String getUser() {
        return model101_userlogin.user;
    }

    public void setUser(String user) {
        model101_userlogin.user = fun.plain_encrypt(user);
    }

    public final String getEmail() {
        return model101_userlogin.email;
    }

    public void setEmail(String email) {
        model101_userlogin.email = fun.plain_encrypt(email);
    }

    public final String getPass() {
        return model101_userlogin.pass;
    }

    public void setPass(String pass) {
        model101_userlogin.pass = fun.plain_encrypt(pass);
    }
    
}
