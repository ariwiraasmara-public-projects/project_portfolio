package com.ariwiraasmara.detail_user.model;
/*
 * @author Ari Wiraasmara
 */

import com.ariwiraasmara.detail_user.util.myfunction;
public class model102_biodata implements interfacemodel102_biodata {
    
    protected static String namauser;
    protected static String id_102;
    protected static String nama;
    protected static String email;
    protected static String tgl;
    protected static String alamat;
    protected static String tlp;
    protected static String status;
    protected static String nebiodatalabel;
    protected myfunction fun;

    public model102_biodata() {
        fun = new myfunction();
    }
    
    public model102_biodata(String id, String nama, String email,
                         String tgl, String alamat, String tlp, String status) {
        fun = new myfunction();
        this.setId(id);
        this.setNama(nama);
        this.setEmail(email);
        this.setTgl(tgl);
        this.setAlamat(alamat);
        this.setTlp(tlp);
        this.setStatus(status);
    }

    final public void logoutSystem() {
        model102_biodata.namauser = null;
        model102_biodata.id_102 = null;
        model102_biodata.nama = null;
        model102_biodata.email = null;
        model102_biodata.tgl = null;
        model102_biodata.alamat = null;
        model102_biodata.tlp = null;
        model102_biodata.status = null;
        model102_biodata.nebiodatalabel = null;
    }
    
    public final String getNamaUser() {
        return model102_biodata.namauser;
    }

    public void setNamaUser(String namauser) {
        model102_biodata.namauser = fun.plain_encrypt(namauser);
    }
    
    
    
    public final String getId() {
        return model102_biodata.id_102;
    }

    public void setId(String id) {
        model102_biodata.id_102 = fun.plain_encrypt(id);
    }

    public final String getNama() {
        return model102_biodata.nama;
    }

    public void setNama(String nama) {
        model102_biodata.nama = fun.plain_encrypt(nama);
    }

    public final String getEmail() {
        return model102_biodata.email;
    }

    public void setEmail(String email) {
        model102_biodata.email = fun.plain_encrypt(email);
    }

    public final String getTgl() {
        return model102_biodata.tgl;
    }

    public void setTgl(String tgl) {
        model102_biodata.tgl = fun.plain_encrypt(tgl);
    }

    public final String getAlamat() {
        return model102_biodata.alamat;
    }

    public void setAlamat(String alamat) {
        model102_biodata.alamat = fun.plain_encrypt(alamat);
    }

    public final String getTlp() {
        return model102_biodata.tlp;
    }

    public void setTlp(String tlp) {
        model102_biodata.tlp = fun.plain_encrypt(tlp);
    }

    public final String getStatus() {
        return model102_biodata.status;
    }

    public void setStatus(String status) {
        model102_biodata.status = fun.plain_encrypt(status);
    }
    
    public final String getnebiodataLabel() {
        return model102_biodata.nebiodatalabel;
    }
    
    public void setnebiodataLabel(String nebiodatalabel) {
        model102_biodata.nebiodatalabel = fun.plain_encrypt(nebiodatalabel);
    }
}
