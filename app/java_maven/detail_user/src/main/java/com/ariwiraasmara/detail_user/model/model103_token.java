package com.ariwiraasmara.detail_user.model;
/*
 * @author Ari Wiraasmara
 */

import com.ariwiraasmara.detail_user.util.myfunction;
public class model103_token implements interfacemodel103_token {
 
    protected myfunction fun;

    protected String token_login;
    protected String token_register;
    protected String token_profil;
    protected String token_updatepassword;
    protected String token_biodata;
    protected String token_nebiodata;

    public model103_token() {
        this.fun = new myfunction();
        this.setLogin();
        this.setRegister();
        this.setProfil();
        this.setUpdatePassword();
        this.setBiodata();
        this.setNEBiodata();
    }

    protected void setLogin() {
        this.token_login = this.fun.finalEncrypt(this.fun.genToken("login"));
    }

    public final String getLogin() {
        return this.token_login;
    }

    protected void setRegister() {
        this.token_register = this.fun.finalEncrypt(this.fun.genToken("register"));
    }

    public final String getRegister() {
        return this.token_register;
    }

    protected void setProfil() {
        this.token_profil = this.fun.finalEncrypt(this.fun.genToken("profil"));
    }

    public final String getProfil() {
        return this.token_profil;
    }

    protected void setUpdatePassword() {
        this.token_updatepassword = this.fun.finalEncrypt(this.fun.genToken("updatepassword"));
    }

    public final String getUpdatePassword() {
        return this.token_updatepassword;
    }

    protected void setBiodata() {
        this.token_biodata = this.fun.finalEncrypt(this.fun.genToken("biodata"));
    }

    public final String getBiodata() {
        return this.token_biodata;
    }

    protected void setNEBiodata() {
        this.token_nebiodata = this.fun.finalEncrypt(this.fun.genToken("nebiodata"));
    }

    public final String getNEBiodata() {
        return this.token_nebiodata;
    }
   
}
