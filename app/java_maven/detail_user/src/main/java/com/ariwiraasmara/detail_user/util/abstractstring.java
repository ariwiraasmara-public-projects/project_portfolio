package com.ariwiraasmara.detail_user.util;
/*
 * @author Ari Wiraasmara
 */

import com.ariwiraasmara.detail_user.util.interface_abstractstring;
public class abstractstring implements interface_abstractstring {
    protected final String read_userlogin   = "SELECT * from my101_user_login ";
    protected final String insert_userlogin = "INSERT into my101_user_login";
    protected final String update_userlogin = "UPDATE my101_user_login set ";
    protected final String delete_userlogin = "DELETE FROM my101_user_login";
    
    protected final String read_biodata   = "SELECT * from my102_biodata ";
    protected final String insert_biodata = "INSERT into my102_biodata";
    protected final String update_biodata = "UPDATE my102_biodata set ";
    protected final String delete_biodata = "DELETE FROM my102_biodata ";
    
    protected final String tvalues = " values ";
    protected final String twhere  = " where ";
    protected final String torder  = " order by ";
    protected final String id101   = " id_101 ";
    protected final String id102   = " id_102 ";
    protected final String nama    = " nama ";
    protected final String email   = " email ";
    
    public abstractstring() {
        
    }
    
    public final String read_userlogin(String where) {
        if(!where.isEmpty()) where = this.twhere + where;
        return this.read_userlogin + where;
    }
    
    public final String insert_userlogin(String values) {
        return this.insert_userlogin + this.tvalues + values;
    }
    
    public final String update_userlogin(String values, String id) {
        return this.update_userlogin + values + this.twhere + this.id101 + "='" + id + "' or " + this.email + " ='" + id + "'";
    }
    
    public final String delete_userlogin(String id) {
        return this.delete_userlogin + this.twhere + this.id101 + "='" + id + "' or " + this.email + " ='"+id+"'";
    }
    
    public final String read_biodata(String where, String orderby) {
        if(!where.isEmpty()) where = this.twhere + where;
        if(!orderby.isEmpty()) orderby = this.torder + orderby;
        return this.read_biodata + where + orderby;
    }
    
    public final String insert_biodata(String kolom, String values) {
        if(!kolom.isEmpty()) kolom =  "(" + kolom + ")";
        return this.insert_biodata + kolom + this.tvalues + values;
    }
    
    public final String update_biodata(String values, String id) {
        return this.update_biodata + values + this.twhere + this.id102 + "='" + id + "' or " + this.email + " ='" + id + "'";
    }
   
    public final String delete_biodata(String id) {
        return this.delete_biodata + this.twhere + this.nama + "='" + id + "' or " + this.email + " ='"+id+"'";
    }
}
