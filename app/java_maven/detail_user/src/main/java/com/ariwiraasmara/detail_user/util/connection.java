package com.ariwiraasmara.detail_user.util;
/*
 * @author Ari Wiraasmara
 */

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Signature;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.crypto.Cipher;
import javax.swing.JOptionPane;

public abstract class connection implements interfaceconnection {
    protected final String sv = "localhost";
    protected final String db = "db_contoh_myportfolio";
    protected final String us = "root";
    protected final String pw = "";
    
    protected String url;  //= "jdbc:mysql://localhost/db_contoh_myportfolio";
    protected String user; //= "root";
    protected String pass; //= "";

    public connection() {
        this.url  = "jdbc:mysql://" + this.sv + "/" + this.db;
    }
    
    public connection(String host, String user, String pass, String db) {
        this.url  = "jdbc:mysql://" + host + "/" + db;
        this.user = user;
        this.pass = pass;
    }
    
    public Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(this.url,this.us,this.pw); //DriverManager.getConnection("jdbc:sqlite:db_contoh_myportfolio.db");
        }
        catch (ClassNotFoundException e) {
            // System.out.println("Error +1" + e.getMessage());
            JOptionPane.showMessageDialog(null, "Tidak dapat terkoneksi ke server", "Server Information", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
        catch (SQLException e) {
            //System.out.println("Error +2 : " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Error: " + e + "\n\nPeriksa Sambungan Database", "Koneksi.java - Koneksi ke Database Gagal", JOptionPane.ERROR_MESSAGE);
        }
        return con;
    }
    
}
