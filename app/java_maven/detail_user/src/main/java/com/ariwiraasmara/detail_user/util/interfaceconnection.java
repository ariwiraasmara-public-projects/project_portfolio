package com.ariwiraasmara.detail_user.util;
/*
 * @author Ari Wiraasmara
 */

import java.sql.Connection;
public abstract interface interfaceconnection {
    public Connection getConnection();
}
