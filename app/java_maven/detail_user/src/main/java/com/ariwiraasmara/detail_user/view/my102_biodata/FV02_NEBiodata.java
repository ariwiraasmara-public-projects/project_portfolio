package com.ariwiraasmara.detail_user.view.my102_biodata;
/*
 * @author Ari Wiraasmara
 */

import java.time.LocalDate;
import com.ariwiraasmara.detail_user.model.model102_biodata;
import com.ariwiraasmara.detail_user.controller.controller102_biodata;
import com.ariwiraasmara.detail_user.model.model101_userlogin;
import com.ariwiraasmara.detail_user.util.myfunction;
import java.sql.SQLException;
import java.util.Arrays;
import javax.swing.JOptionPane;
public class FV02_NEBiodata extends javax.swing.JFrame {

    protected LocalDate date = LocalDate.now();
    protected model101_userlogin muserlogin;
    protected model102_biodata mbiodata;
    protected controller102_biodata cbiodata;
    protected myfunction fun = new myfunction();
    protected String id102;
    
    public FV02_NEBiodata() {
        super("Biodata | Detail User");
        initComponents();
        setLocationRelativeTo(null);
        classInitialize();
    }
    
    protected void classInitialize() {
        this.muserlogin = new model101_userlogin();
        this.loginSession();
        this.tgl_bulan_tahun("tgl");
        this.tgl_bulan_tahun("bulan");
        this.tgl_bulan_tahun("tahun");
        this.fun        = new myfunction();
        this.mbiodata   = new model102_biodata();
        //lbl_title.setText(this.mbiodata.getnebiodataLabel().toString());
        try {
            this.cbiodata  = new controller102_biodata();
            this.setField();
        } catch (SQLException ex) {
            //Logger.getLogger(FV01_Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void loginSession() {
        if( this.fun.plain_decrypt(this.muserlogin.getId_101()).isEmpty() || this.fun.plain_decrypt(this.muserlogin.getUser()).isEmpty() || this.fun.plain_decrypt(this.muserlogin.getEmail()).isEmpty() ) {
            JOptionPane.showMessageDialog(this, "System Has Been Hacked!", "ERROR CRITICAL SYSTEM!", JOptionPane.WARNING_MESSAGE);
            this.muserlogin.logoutSystem();
            this.fun.redirect(this, "dispose", "login");
        }
        else if( this.fun.plain_decrypt(this.muserlogin.getId_101()).equals("") || this.fun.plain_decrypt(this.muserlogin.getUser()).equals("") || this.fun.plain_decrypt(this.muserlogin.getEmail()).equals("") ) {
            JOptionPane.showMessageDialog(this, "System Has Been Hacked!", "ERROR CRITICAL SYSTEM!", JOptionPane.WARNING_MESSAGE);
            this.muserlogin.logoutSystem();
            this.fun.redirect(this, "dispose", "login");
        }
        else if( this.fun.plain_decrypt(this.muserlogin.getId_101()).equals(" ") || this.fun.plain_decrypt(this.muserlogin.getUser()).equals(" ") || this.fun.plain_decrypt(this.muserlogin.getEmail()).equals(" ") ) {
            JOptionPane.showMessageDialog(this, "System Has Been Hacked!", "ERROR CRITICAL SYSTEM!", JOptionPane.WARNING_MESSAGE);
            this.muserlogin.logoutSystem();
            this.fun.redirect(this, "dispose", "login");
        }
        else if( this.fun.plain_decrypt(this.muserlogin.getId_101()).equals(null) || this.fun.plain_decrypt(this.muserlogin.getUser()).equals(null) || this.fun.plain_decrypt(this.muserlogin.getEmail()).equals(null) ) {
            JOptionPane.showMessageDialog(this, "System Has Been Hacked!", "ERROR CRITICAL SYSTEM!", JOptionPane.WARNING_MESSAGE);
            this.muserlogin.logoutSystem();
            this.fun.redirect(this, "dispose", "login");
        }
    }
    
    protected void tgl_bulan_tahun(String type) {
        if(type.equals("tgl")) {
            for(int x=1; x<32; x++) {
                if(x < 10) cbo_tgl.addItem(String.valueOf("0" + x));
                else cbo_tgl.addItem(String.valueOf(x));
            }
        }
        else if(type.equals("bulan")) {
            for(int x=1; x<13; x++) {
                cbo_bulan.addItem(String.valueOf(x));
            }
        }
        else if(type.equals("tahun")) {
            int year = this.date.getYear();
            cbo_tahun.addItem(String.valueOf(year));
            for(int x=1; x<101; x++) {
                cbo_tahun.addItem(String.valueOf(year-x));
            }
        }
        else {
            cbo_tgl.addItem("");
            cbo_bulan.addItem("");
            cbo_tahun.addItem("");
        }
    }
    
    protected int[] selectTgl_Bulan_Tahun(String type) {
        int[] arres = null;
        if(type.equals("tgl")) {
            for(int x=0; x<31; x++) {
                arres[x] = x+1;
            }
        }
        else if(type.equals("bulan")) {
            for(int x=0; x<12; x++) {
                arres[x] = x+1;
            }
        }
        else if(type.equals("tahun")) {
            int year = date.getYear();
            cbo_tahun.addItem(String.valueOf(year));
            for(int x=0; x<100; x++) {
                arres[x] = year-(x+1);
            }
        }
        else arres = null;
        return arres;
    }
    
    protected void fieldNull() {
        txt_nama.setText("");
        txt_email.setText("");
        txt_alamat.setText("");
        txt_tlp.setText("");
        txt_status.setText("");
        cbo_tahun.setSelectedItem(null);
        cbo_bulan.setSelectedItem(null);
        cbo_tgl.setSelectedItem(null);
    }
    
    protected void setField()  {
        try {
            System.out.println("Email : " + this.fun.plain_decrypt(this.mbiodata.getEmail()));
            if( this.fun.plain_decrypt(this.mbiodata.getEmail()).isEmpty() ) {
                this.fieldNull();
                lbl_title.setText("Biodata Baru");
                btn_save.setText("Simpan");
            }
            else {
                lbl_title.setText("Edit Biodata");
                btn_save.setText("Update");
                String[] data = this.cbiodata.getData(" email='" + this.fun.plain_decrypt(this.mbiodata.getEmail().toString()) +"' ", "");
                this.id102 = data[0];
                txt_nama.setText(data[1]);
                txt_email.setText(data[2]);
                txt_email.disable();
                txt_alamat.setText(data[4]);
                txt_tlp.setText(data[5]);
                txt_status.setText(data[6]);
                cbo_tahun.setSelectedItem(this.fun.splitTime(data[3], "tahun"));
                cbo_bulan.setSelectedItem(this.fun.splitTime(data[3], "bulan"));
                cbo_tgl.setSelectedItem(this.fun.splitTime(data[3], "tgl"));
            }
        }
        catch (SQLException ex) {
            //lbl_title.setText("Tambah Biodata");
            //this.fieldNull();
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_nama = new javax.swing.JTextField();
        txt_email = new javax.swing.JTextField();
        txt_tlp = new javax.swing.JTextField();
        txt_status = new javax.swing.JTextField();
        btn_save = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        cbo_tgl = new javax.swing.JComboBox<>();
        cbo_bulan = new javax.swing.JComboBox<>();
        cbo_tahun = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_alamat = new javax.swing.JTextArea();
        lbl_title = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menu_back = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel1.setText("Nama :");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel2.setText("Email : ");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel4.setText("Tanggal Lahir : ");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel5.setText("Alamat : ");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel6.setText("No. Telpon : ");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        jLabel7.setText("Status : ");

        btn_save.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        btn_save.setText("Simpan");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tanggal / Bulan / Tahun", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 11))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbo_tgl, 0, 80, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbo_bulan, 0, 80, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbo_tahun, 0, 80, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbo_tgl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbo_bulan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbo_tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        txt_alamat.setColumns(20);
        txt_alamat.setLineWrap(true);
        txt_alamat.setRows(5);
        jScrollPane1.setViewportView(txt_alamat);

        lbl_title.setFont(new java.awt.Font("Georgia", 1, 15)); // NOI18N
        lbl_title.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_title.setText("Tambah / Edit Biodata");

        jMenu1.setText("File");

        menu_back.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        menu_back.setText("Kembali");
        menu_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_backActionPerformed(evt);
            }
        });
        jMenu1.add(menu_back);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_save, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_email)
                            .addComponent(txt_nama, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txt_status, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_tlp, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lbl_title, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lbl_title, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_tlp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menu_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_backActionPerformed
        // TODO add your handling code here:
        this.fun.redirect(this, "dispose", "biodata");
    }//GEN-LAST:event_menu_backActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        // TODO add your handling code here:
        String tgl;
        //int res;
        try {
            String nama = this.fun.escape(txt_nama.getText().toString());
            String email = this.fun.escape(txt_email.getText().toString());
            tgl = this.fun.escape(cbo_tahun.getSelectedItem().toString()) + "/" + this.fun.escape(cbo_bulan.getSelectedItem().toString()) + "/" + this.fun.escape(cbo_tgl.getSelectedItem().toString());
            String alamat = this.fun.escape(txt_alamat.getText().toString());
            String tlp = this.fun.escape(txt_tlp.getText().toString());
            String status = this.fun.escape(txt_status.getText().toString());
            if(this.fun.plain_decrypt(this.mbiodata.getEmail()).isEmpty()) {
                String id  = this.cbiodata.getLastID("", "id_102 ASC", 3, 3, '0');
                //System.out.println(id);
                //res = this.cbiodata.insertData("", "('" + id + "','" + nama + "','" + email + "','" + tgl + "','" + alamat + "','" + tlp + "','" + status + "')");
                if(this.cbiodata.insertData("", "('" + id + "','" + nama + "','" + email + "','" + tgl + "','" + alamat + "','" + tlp + "','" + status + "')")) {
                    JOptionPane.showMessageDialog(this, "Biodata Berhasil Disimpan!", "Success!", JOptionPane.INFORMATION_MESSAGE);
                    this.fieldNull();
                    this.fun.redirect(this, "dispose", "biodata");
                }
                else JOptionPane.showMessageDialog(this, "Terjadi Kesalahan! Data Gagal Disimpan", "Error!", JOptionPane.WARNING_MESSAGE);
                //this.fun.redirect(this, "dispose", "biodata");
            }
            else {
                //res = this.cbiodata.updateData("nama='" + nama + "', " + "tgl_lahir='" + tgl + "', " + "alamat='" + alamat + "', " + "tlp='" + tlp + "', " + "status='" + status + "' ", this.fun.plain_decrypt(this.mbiodata.getEmail()).toString());
                if( this.cbiodata.updateData("nama='" + nama + "', " + "tgl_lahir='" + tgl + "', " + "alamat='" + alamat + "', " + "tlp='" + tlp + "', " + "status='" + status + "' ", this.fun.plain_decrypt(this.mbiodata.getEmail()).toString()) ) {
                    JOptionPane.showMessageDialog(this, "Biodata Berhasil Diperbaharui!", "Success!", JOptionPane.INFORMATION_MESSAGE);
                    this.fieldNull();
                    this.fun.redirect(this, "dispose", "biodata");
                }
                else JOptionPane.showMessageDialog(this, "Terjadi Kesalahan! Data Gagal Diperbaharui", "Error!", JOptionPane.WARNING_MESSAGE);
            }
            
        } catch (SQLException ex) {
            tgl = "";
            //res = 0;
        }
    }//GEN-LAST:event_btn_saveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FV02_NEBiodata.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FV02_NEBiodata.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FV02_NEBiodata.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FV02_NEBiodata.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FV02_NEBiodata().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_save;
    private javax.swing.JComboBox<String> cbo_bulan;
    private javax.swing.JComboBox<String> cbo_tahun;
    private javax.swing.JComboBox<String> cbo_tgl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_title;
    private javax.swing.JMenuItem menu_back;
    private javax.swing.JTextArea txt_alamat;
    private javax.swing.JTextField txt_email;
    private javax.swing.JTextField txt_nama;
    private javax.swing.JTextField txt_status;
    private javax.swing.JTextField txt_tlp;
    // End of variables declaration//GEN-END:variables
}
