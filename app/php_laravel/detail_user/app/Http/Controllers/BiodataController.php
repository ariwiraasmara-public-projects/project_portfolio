<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\MyLibs\myfunction;
use App\Models\my101_user_login;
use App\Models\my102_biodata;
use Redirect;
use Cookie;

class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $userlogin  = my101_user_login::where('email', '=', Cookie::get('email'))->get();
        $profil     = my102_biodata::where('email', '=', Cookie::get('email'))->get();
        $biodata    = my102_biodata::where('email', '!=', Cookie::get('email'))->orderBy('nama', 'asc')->get();

        $arrbio = my102_biodata::where('email', '!=', Cookie::get('email'))->get()->toArray();

        //$res = in_array($userlogin[1]->email, $arrbio);

        //return 'asdsd '.myfunction::getCookie('email', 1);
        //return $res;

        return view('after_login.my102.biodata',
                    ['title'    => 'Dashboard ',
                    'userlogin' => $userlogin,
                    'biodata'   => $biodata,
                    'nama'      => $userlogin[0]->username
                    ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('after_login.my102.nebiodata',
                    ['title'     => 'Tambah Biodata',
                    'ne'         => 'add'
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $id_102 = myfunction::getlastID(102, 'id_102', null, null, 'BI#', 5, 3, 'id_102', 3, 5);
        $nama = myfunction::escape(match($request->nama) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->nama
                });

        $email = myfunction::escape(match($request->email) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->email
                });

        $tgl_lahir = myfunction::escape(match($request->tgl_lahir) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->tgl_lahir
                });

        $alamat = myfunction::escape(match($request->alamat) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->alamat
                });

        $tlp = myfunction::escape(match($request->tlp) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->tlp
                });

        $status = myfunction::escape(match($request->status) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->status
                });

        my102_biodata::insert([
            'id_102'    => $id_102,
            'nama'      => $nama,
            'email'     => $email,
            'tgl_lahir' => $tgl_lahir,
            'alamat'    => $alamat,
            'tlp'       => $tlp,
            'status'    => $status
        ]);

        return Redirect::to('bio')->with('success','Biodata Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $denid = myfunction::escape(myfunction::denval($id));
        $where = array('id_102'=>$denid);
        $biodata = my102_biodata::where($where)->get();

        return view('after_login.my102.nebiodata',
                    ['title'     => 'Edit Biodata',
                    'ne'         => 'edit',
                    'biodata'    => $biodata
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $denid  = myfunction::escape(myfunction::denval($id));
        $where  = array('id_102'=>$denid);

        $nama = myfunction::escape(match($request->nama) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->nama
                });

        $email = myfunction::escape(match($request->email) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->email
                });

        $tgl_lahir = myfunction::escape(match($request->tgl_lahir) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->tgl_lahir
                });

        $alamat = myfunction::escape(match($request->alamat) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->alamat
                });

        $tlp = myfunction::escape(match($request->tlp) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->tlp
                });

        $status = myfunction::escape(match($request->status) {
                    null => '__null__',
                    '' => '__null__',
                    default => $request->status
                });

        $update = ['nama'      => $nama,
                   'tgl_lahir' => $tgl_lahir,
                   'alamat'    => $alamat,
                   'tlp'       => $tlp,
                   'status'    => $status
                  ];
        my102_biodata::where($where)->update($update);
        return Redirect::to('bio')->with('success','Biodata Berhasil Diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $demail = myfunction::escape(myfunction::denval($id));
        $where  = array('id_102' => $demail);

        //return 'Delete ID '.$demail;

        my102_biodata::where($where)->delete();
        return Redirect::to('bio')->with('success','Biodata Berhasil Dihapus!');
    }
}
