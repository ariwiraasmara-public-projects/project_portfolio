<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\MyLibs\myfunction;
use App\Models\my101_user_login;
use App\Models\my102_biodata;
use Redirect;
use Cookie;

class UserLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $email = Cookie::get('email');
        $biodata = my101_user_login::join('my102_biodata', 'my101_user_login.email', '=', 'my102_biodata.email')
                       ->where("my101_user_login.email", "=", $email)
                       ->get();

        //return $biodata;

        return view('after_login.my101.userprofil',
                    ['title'=>'Profil',
                     'biodata'=>$biodata,
                    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return Redirect::to('ulo', Cookie::get('email'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $detype = myfunction::denval($request->type);
        if($detype == 'register') {
            if($request->password == $request->passver) {
                $id_101 = myfunction::getlastID(101, 'id_101', null, null, 'UL#', 5, 3, 'id_101', 3, 5);
                $id_102 = myfunction::getlastID(102, 'id_102', null, null, 'BI#', 5, 3, 'id_102', 3, 5);

                $username = myfunction::escape(match($request->username) {
                                null => '__null__',
                                '' => '__null__',
                                default => $request->username
                            });

                $email = myfunction::escape(match($request->email) {
                            null => '__null__',
                            '' => '__null__',
                            default => $request->email
                        });

                $password = myfunction::escape(match($request->password) {
                            null => '__null__',
                            '' => '__null__',
                            default => $request->password
                        });


                my101_user_login::insert([
                    'id_101'    => $id_101,
                    'username'  => $username,
                    'email'     => $email,
                    'password'  => $password,
                ]);

                my102_biodata::insert([
                    'id_102'    => $id_102,
                    'nama'      => '',
                    'email'     => $email,
                    'tgl_lahir' => null,
                    'alamat'    => null,
                    'tlp'       => null,
                    'status'    => null
                ]);

                return view('before_login.login',
                            ['title'=>'Register',
                             'message'=>'Akun Baru Berhasil Dibuat!'
                            ]);
            }
            else {
                return view('before_login.register',
                            ['title'=>'Register',
                             'message'=>'Password dan Password Konfirmasi Tidak Sama!'
                            ]);
            }
        }
        else if($detype == 'login') {
            $usermail = myfunction::escape($request->usermail);
            $password = myfunction::escape($request->password);
            //echo $usermail.' :: '.$password;

            $where1 = array("username" => $usermail, "password" => $password);
            $where2 = array("email" => $usermail, "password" => $password);
            //$where = array(['username', '==', $usermail], ['password', '==', $password]);
            $cek1 = my101_user_login::where($where1)->count();
            $cek2 = my101_user_login::where($where2)->count();

            //echo $cek1.' == '.$cek2;

            if( ($cek1 == 0) && ($cek2 == 0) ) {
                return view('before_login.login', ['title'=>'Login', 'message'=>"Username / Email / Password Salah! Silahkan Coba Lagi!"]);
            }
            else {
                $userlogin  = my101_user_login::where($where1)->orWhere($where2)->get();

                Cookie::queue(Cookie::make('login', csrf_token(), 1*24*60*60));
                Cookie::queue(Cookie::make('username', $userlogin[0]->username, 1*24*60*60));
                Cookie::queue(Cookie::make('email',   $userlogin[0]->email, 1*24*60*60));

                //return $response;
                return Redirect::to('bio');
            }
        }
        else {
            return Redirect::to('login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $denid = myfunction::escape(myfunction::denval($id));
        $where = array('id_101'=>$denid);
        $userlogin = my101_user_login::where($where)->get();

        return view('after_login.my101.updateuser',
                    ['title'=>'Edit Profil',
                     'userlogin'=>$userlogin,
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $oldpassword = myfunction::escape(match($request->oldpassword) {
                        null => '__null__',
                        '' => '__null__',
                        default => $request->oldpassword
                    });

        $newpassword = myfunction::escape(match($request->newpassword) {
                        null => '__null__',
                        '' => '__null__',
                        default => $request->newpassword
                    });

        if($request->newpassword == $request->oldpassword) {
            $denid = myfunction::escape(myfunction::denval($id));
            $where = array('id_101'=>$denid);

            $update = ["password" => $newpassword];
            my101_user_login::where($where)->update($update);

            return Redirect::to('ulo')->with('success','Password Profil Berhasil Diperbaharui!');
        }
        else {
            return Redirect::to('ulo')->with('success','Password dan Password Verifikasi Tidak Sama! Profil Gagal Diperbaharui!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $detype = myfunction::escape(myfunction::denval($id));

        if($detype == 'delete') {
            $where = array('email' => myfunction::escape($request->email));
            my101_user_login::where($where)->delete();
            my102_biodata::where($where)->delete();
        }
        else {
            Cookie::queue(Cookie::forget('login'));
            Cookie::queue(Cookie::forget('username'));
            Cookie::queue(Cookie::forget('email'));
        }

        return Redirect::to('login');
    }

}
