<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class my101_user_login extends Model
{
    use HasFactory;

    protected $table = 'my101_user_login';
    //public $table = 'my101_user_login';

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
