<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class my103_tabungan extends Model
{
    use HasFactory;

    protected $table = 'my103_tabungan';
    //public $table = 'my101_user_login';

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
