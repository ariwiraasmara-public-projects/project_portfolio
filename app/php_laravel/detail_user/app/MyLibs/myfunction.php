<?php
namespace App\Mylibs;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Models\my101_user_login;
use App\Models\my102_biodata;

class myfunction {

    // GET AND POST PARAMETER
    public function POST(String|float $str=null, int $state=0, int $position=null) {
        $str = self::escape($str);
        try {
            return match(true) {
                ($position != null) || ($position != '') || !empty($position) || !is_null($position) => match($state) {
                    1 => @$_POST[self::enlink($str)][$position],
                    2 => self::denval(@$_POST[self::enlink($str)][$position]),
                    default => @$_POST[$str][$position],
                },
                default => match($state) {
                    1 => @$_POST[self::enlink($str)],
                    2 => self::denval(@$_POST[self::enlink($str)]),
                    default => @$_POST[$str],
                },
            };
        }
        catch(Exception $e) {
            return "function POST() Error: ".$e;
        }
    }

    public function GET(String|float $str=null, int $state=0) {
        $str = self::escape($str);
        try {
            return match($state) {
                1 => @$_GET[self::enlink($str)],
                2 => self::denval(@$_GET[self::enlink($str)]),
                default => @$_GET[$str],
            };
        }
        catch(Exception $e) {
            return "function GET() Error: ".$e;
        }
    }

    public function FILE(String|float $str=null, String $tipe=null, int $state=0, int $position=null) {
        try {
            return match(true) {
                $position == null || $position == '' || empty($position) || is_null($position) => match($state) {
                    1 => @$_FILES[self::enlink($str)][$tipe],
                    default => @$_FILES[$str][$tipe],
                },
                default => match($state) {
                    1 => @$_FILES[self::enlink($str)][$tipe][$position],
                    default => @$_FILES[$str][$tipe][$position],
                }
            };
        }
        catch(Exception $e) {
            return "function FILE() Error: ".$e;
        }
    }


    // SET TITLE
    public static $title, $project_title, $separator_title;
    public static $title_company;
    public static function setTitle(String|float $title = null) {
        try {
            self::$title = $title;
        }
        catch(Exception $e) {
            echo "function setTitle() Error: ".$e;
        }
    }

    public static function setTitle_Company(String $title) {
        try {
            self::$title_company = $title;
        }
        catch(Exception $e) {
            echo "function setTitle_Company() Error: ".$e;
        }
    }

    public function setProjectTitle(String|float $title = null) {
        try {
            self::$project_title = $title;
        }
        catch(Exception $e) {
            echo "function setProjectTitle() Error: ".$e;
        }
    }

    public function setSepatatorTitle(String $title = null) {
        try {
            self::$separator_title = $title;
        }
        catch(Exception $e) {
            echo "function setSepatatorTitle() Error: ".$e;
        }
    }

    public function getTitle() {
        try {
            return self::$title;
        }
        catch(Exception $e) {
            return "function getTitle() Error: ".$e;
        }
    }

    public function getProjectTitle() {
        try {
            return self::$project_title;
        }
        catch(Exception $e) {
            return "function getProjectTitle() Error: ".$e;
        }
    }

    public function getSeparatorTitle() {
        try {
            return self::$separator_title;
        }
        catch(Exception $e) {
            return "function getSeparatorTitle() Error: ".$e;
        }
    }

    public function getDocTitle(String|float $title = "__-__") { ?>
        <script type="text/javascript">
            document.title = "<?php echo self::getTitle().self::getSeparatorTitle().self::getProjectTitle(); ?>";
            $("<?php echo '#'.$title; ?>").html("<?php echo self::getTitle(); ?>");
        </script>
        <?php
    }

    public function redirect(String $str) {
        return header("location: ".$str);
    }


    // ENCRYPT & DECRYPT
    public function encrypt_plain(String $plaintext=null, String $key=null){
        try {
            $k = self::cryptkey($key);

            $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
            $hmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
            $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

            return $ciphertext;
        }
        catch(Exception $e) {
            return "function encrypt_plain() Error: ".$e;
        }
    }

    public function decrypt_plain(String $code=null, String $key=null){
        try {
            $k = self::cryptkey($key);

            $c = base64_decode($code);
            $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
            if (hash_equals($hmac, $calcmac)) {
                return $original_plaintext;
            }
        }
        catch(Exception $e) {
            return "function decrypt_plain() Error: ".$e;
        }
    }

    public function encrypt(String|float $txt=null) {
        try {
            $str = self::encrypt_plain($txt, self::getK());
            return $str;
        }
        catch(Exception $e) {
            return "function encrypt() Error: ".$e;
        }
    }

    public function decrypt(String|float $txt=null) {
        try {
            $str = self::decrypt_plain($txt, self::getK());
            return $str;
        }
        catch(Exception $e) {
            return "function decrypt() Error: ".$e;
        }
    }

    protected function cryptkey(String $code=null){
        try {
            $c = base64_decode($code);
            $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            //$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, 'c8f6dae75c5e2010d46924649e0c0357', $options=OPENSSL_RAW_DATA, $iv);
            //$calcmac = hash_hmac('sha256', $ciphertext_raw, 'c8f6dae75c5e2010d46924649e0c0357', $as_binary=true);
            //$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, '{[/=<<@-19O6I1A14A20A19I1DES1I14T53O17U9L125-@>>=\]}', $options=OPENSSL_RAW_DATA, $iv);
            //$calcmac = hash_hmac('sha256', $ciphertext_raw, '{[/=<<@-19O6I1A14A20A19I1DES1I14T53O17U9L125-@>>=\]}', $as_binary=true);
            $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, crypt(sha1(md5('rL+J1dguWCXBiKOt32uUAqTc0/CVZpJwPsA9ahOP6UbQSBAkcXGrpLUi96W15fmmAKBTPbjnc83h83qLJ+rfeRyn0NLt4q/CCB/7I6RVsmk=', TRUE),TRUE),'$6$rounds=999999999'), $options=OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, crypt(sha1(md5('rL+J1dguWCXBiKOt32uUAqTc0/CVZpJwPsA9ahOP6UbQSBAkcXGrpLUi96W15fmmAKBTPbjnc83h83qLJ+rfeRyn0NLt4q/CCB/7I6RVsmk=', TRUE),TRUE),'$6$rounds=999999999'), $as_binary=true);
            if (hash_equals($hmac, $calcmac)) {
                return $original_plaintext;
            }
        }
        catch(Exception $e) {
            return "function ks() Error: ".$e;
        }
    }

    // SAFETY CONNECTION STRING FROM INJECTION
    public static function escape(String|float $str=null) {
        try {
            return htmlspecialchars(htmlentities(addslashes($str)));
        }
        catch(Exception $e) {
            return "Error Function escape() : ".$e;
        }
    }

    public function safe(String|float $str=null) {
        try {
            //return mysqli_real_escape_string(self::getConnection(), self::escape($str));
            return self::getConnection()->real_escape_string($str);
        }
        catch(Exception $e) {
            return "Error Function safe() : ".$e;
        }
    }

    // READABLE FORMAT TEXT
    public function readable(String|float $str=null) {
        try {
            return html_entity_decode(htmlspecialchars_decode($str));
        }
        catch(Exception $e) {
            return "Error Function READABLE() : ".$e;
        }
    }

    public function formatnumber(int|float $str=null, $koma=0) {
        try {
            return number_format($str, $koma, ',', '.');
        }
        catch(Exception $e) {
            return "Error Function formatnumber() : ".$e;
        }
    }

    public function rupiah(int|float $str=null) {
        try {
            return 'Rp. '.self::formatnumber($str, 2);
        }
        catch(Exception $e) {
            return "Error Function Rupiah() : ".$e;
        }
    }


    // SEND EMAIL
    public function toSendMail(String $to=null, String $subject=null, String $txt=null) {
        try {
            $headers = "From: dev.ariwiraasmara.emailgateway@gmail.com";
            mail($to,$subject,$txt,$headers);
        }
        catch(Exception $e) {
            echo "function toSendMail() Error: ".$e;
        }
    }

    public function toSendMailBy(String $to=null, String $subject=null, String $txt=null, String $headers=null) {
        try {
            //$headers = "From: dev.ariwiraasmara.emailgateway@gmail.com";
            mail($to,$subject,$txt,$headers);
        }
        catch(Exception $e) {
            echo "function toSendMail() Error: ".$e;
        }
    }

    // ENCRYPT & DECRYPT TEXT
    public static function toCrypt(String|float $str) {
        //return crypt($str, '$6$rounds='.self::randNumber(7, 1));
        return crypt($str, self::random('numb', 7));
    }

    public static function toMD5(String|float $str) {
        return md5($str, TRUE);
    }

    public static function toSHA1(String|float $str) {
        return sha1($str, TRUE);
    }

    public static function toSHA512(String|float $str) {
        return hash("sha512", $str);
    }

    public static function toHaval256(String|float $str) {
        return hash("haval256,5", $str);
    }

    public static function tokenName(String $str) {
        return self::toCrypt(self::toSHA512(self::toMD5($str)));
    }

    public static function tokenValue() {
        return self::toCrypt(self::toSHA512(self::toMD5(self::toHaval256(self::encrypt(openssl_random_pseudo_bytes(self::randNumber(3)))))));
    }

    public static function enlink(String|float $str) {
        try {
            return self::toCrypt(self::toSHA512(self::toMD5($str)));
        }
        catch(Exception $e) {
            return 'function enlink() Error: '.$e;
        }
    }

    public function enparam(String $a, String $b, String $c, String $d) {
        try {
            return self::setIDParam($a, $b).'&'.self::setIDParam($c, $d);
        }
        catch(Exception $e) {
            return 'function enparam Error: '.$e;
        }
    }

    public function setIDParam(String|float $id, String|float $val) {
        try {
            return self::enlink($id).'='.self::enval($val);
        }
        catch(Exception $e) {
            return "Error Function setIDParam() : ".$e;
        }
    }

    public function getSafeParam(String|float $id) {
        try {
            return self::safe(self::denval(@$_GET[self::enlink($id)]));
        }
        catch(Exception $e) {
            return "Error Function getIDParam() : ".$e;
        }
    }


    // Encrypt Value with Decrypt
    public static function enval(String|float $str) {
        try {
            return bin2hex(base64_encode( $str ));
        }
        catch(Exception $e) {
            return 'Error function di myfunction->enval: '.$e;
        }
    }

    public static function denval($str) {
        try {
            return base64_decode(hex2bin( $str ));
        }
        catch(Exception $e) {
            return 'function denval() Error: '.$e;
        }
    }

    // SESSION AND COOKIE
    public function setOneSession(String $str, String $val, int $type=0) {
        try {
            if( $type > 1 ) {
                $_SESSION[self::enlink($str)] = self::enval($val);
            }
            else if( $type == 1 ) {
                $_SESSION[self::enlink($str)] = $val;
            }
            else {
                $_SESSION[$str] = $val;
            }
        }
        catch(Exception $e) {
            return "Error Function setOneSession() : ".$e;
        }
    }

    public function setSession(Array $array, int $type=0) {
        try {
            if( $type > 1 ) {
                foreach($array as $arr => $val) {
                    $_SESSION[self::enlink($arr)] = self::enval($val);
                }
            }
            else if( $type == 1 ) {
                foreach($array as $arr => $val) {
                    $_SESSION[self::enlink($arr)] = $val;
                }
            }
            else {
                foreach($array as $arr => $val) {
                    $_SESSION[$arr] = $val;
                }
            }
        }
        catch(Exception $e) {
            return "Error Function setSession() : ".$e;
        }
    }

    public function getSession(String|float $str=null, int $type=0) {
        try {
            return match($type) {
                1 => $_SESSION[self::enlink($str)],
                2 => self::denval($_SESSION[self::enlink($str)]),
                default => $_SESSION[$str],
            };
        }
        catch(Exception $e) {
            return "Error Function getSession() : ".$e;
        }
    }

    public function setOneCookie(String $name, String|float $val=1, int $type=0, int $hari=1, int $jam=24, int $menit=60, int $detik=60) {
        try {
            if($type > 1) {
                setcookie(self::enlink($name), self::enval($val), time() + ($hari * $jam * $menit * $detik), "/");
            }
            else if($type == 1) {
                setcookie(self::enlink($name), $val, time() + ($hari * $jam * 60 * 60), "/");
            }
            else {
                setcookie($name, $val, time() + ($hari * $jam * 60 * 60), "/");
            }
        }
        catch(Exception $e) {
            return "Error Function setOneCookie() : ".$e;
        }
    }

    public static function setCookie($array, $hari=1, $jam=24, $menit=60, $detik=60) {
        try {
            foreach($array as $arr => $val) {
                setcookie(self::enlink($arr), self::enval($val), time() + ($hari * $jam * $menit * $detik), "/"); // 86400 = 1 day
            }
        }
        catch(Exception $e) {
            return "Error Function setCookie() : ".$e;
        }
    }

    public static function getCookie(String|float $str, $type = 0) {
        try {
            return match($type) {
                1 => @$_COOKIE[self::enlink($str)],
                2 => self::denval(@$_COOKIE[self::enlink($str)]),
                default => @$_COOKIE[$str],
            };
        }
        catch(Exception $e) {
            return "Error Function getCookie() : ".$e;
        }
    }

    public function setCookieOff(String $str, String|float $val = null, $type = 1) {
        try {
            if($type > 1) {
                setcookie(self::enlink($str), $val, time() - (365 * 24 * 60 * 60), "/");
            }
            else {
                setcookie($str, $val, time() - (365 * 24 * 60 * 60), "/");
            }
        }
        catch(Exception $e) {
            return "Error Function getValookie() : ".$e;
        }
    }


    // SYSTEM LOGOUT
    public function logoutSystem($array) {
        try {
            foreach($array as $arr) {
                setcookie(self::enlink($arr), null, time() - (365 * 24 * 60 * 60), "/");
            }
            session_destroy();
        }
        catch(Exception $e) {
            return "Error Function logoutSystem() : ".$e;
        }
    }


    public static function getLastID($model, $field_id, $field_iduser, $field_iduser_val, $kodedepan_id, $jumlahnol_id, $substr_field_iduser_val_depan, $orderbyfield, $substr_data_id_depan, $substr_data_id_belakang) {
        try {
            $cekdata = match($model) {
                101 => my101_user_login::count(),
                102 => my102_biodata::count(),
                default => null
            };


            if($cekdata > 0) {
                $data = match($model) {
                    101 => my101_user_login::orderBy($orderbyfield,'desc')->limit(1)->get(),
                    102 => my102_biodata::orderBy($orderbyfield,'desc')->limit(1)->get(),
                    default => null
                };

                $substr = (int)substr($data[0][$field_id], $substr_data_id_depan, $substr_data_id_belakang) + 1;
                $id = $kodedepan_id.str_pad($substr, $jumlahnol_id, '0', STR_PAD_LEFT).substr($field_iduser_val, $substr_field_iduser_val_depan);
            }
            else {
                $id = $kodedepan_id.str_pad(1, $jumlahnol_id, '0', STR_PAD_LEFT);//.substr($field_iduser_val, $substr_field_iduser_val_depan);
            }
            //$enid = myfunction::Enval($id);
            return $id;
        }
        catch(Exception $e) {
            return "function getLastID() Error: ".$e;
        }
    }


    // GET SYSTEM TIME
    public function getHari($str) {
        try {
            return date('l', strtotime($str));
        }
        catch(Exception $e) {
            return "function Hari() Error: ".$e;
        }
    }

    public function getTanggal($str, $pemisah, $format, $jam) {
        try {
            if($format == 'dmy') {
                if($jam == 'Yes') {
                    return date('d'.$pemisah.'m'.$pemisah.'Y H:i:s', strtotime($str));
                }
                else {
                    return date('d'.$pemisah.'m'.$pemisah.'Y', strtotime($str));
                }
            }
            else if($format == 'ymd') {
                if($jam == 'Yes') {
                    return date('Y'.$pemisah.'m'.$pemisah.'d H:i:s', strtotime($str));
                }
                else {
                    return date('Y'.$pemisah.'m'.$pemisah.'d', strtotime($str));
                }
            }
            else {
                return date('d'.$pemisah.'m'.$pemisah.'Y');
            }
        }
        catch(Exception $e) {
            return "function getBulan() Error: ".$e;
        }
    }

    public function getTanggalAkhir($str) {
        try {
            return date('t', strtotime($str));
        }
        catch(Exception $e) {
            return "function getTanggalAkhir() Error: ".$e;
        }
    }

    public function getBulan($str, $bulan, $tahun) {
        try {
            if($bulan == 'Angka') {
                if($tahun == 'Yes') {
                    return date('m Y', strtotime($str));
                }
                else {
                    return date('m', strtotime($str));
                }
            }
            else if($bulan == 'Nama') {
                if($tahun == 'Yes') {
                    return date('F Y', strtotime($str));
                }
                else {
                    return date('F', strtotime($str));
                }
            }
            else {
                return date('F Y');
            }
        }
        catch(Exception $e) {
            return "function getBulan() Error: ".$e;
        }
    }


    // FORM FILL
    public static function inputField($type, $id, $name, $text, $field_class, $text_class, $val=null, $required=null, $readonly=null) {
        try { ?>
            <label for="<?php echo self::Enlink($id); ?>" class="<?php echo $text_class; ?>"><?php echo $text; ?></label>
            <input type="<?php echo $type; ?>" name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" class="<?php echo $field_class; ?>" value="<?php echo $val; ?>" <?php echo $required.' '.$readonly; ?> >
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function inputField() Error: ".$e;
        }
    }

    public static function textField($name, $id, $text, $field_class, $text_class=null, $val=null, $col=null, $row=null, $required=null, $readonly=null) {
        try { ?>
            <label for="<?php echo self::Enlink($id); ?>"><?php echo $text; ?></label>
            <textarea name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" class="<?php echo $field_class; ?>" rows="<?php echo $row; ?>" cols="<?php echo $col; ?>" placeholder="<?php echo $text; ?>" <?php echo $required.' '.$readonly; ?>>
                <?php
                if( $val != '' || !empty($val) || !is_null($val) ) {
                    echo $val;
                }
                ?>
            </textarea>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function textField() Error: ".$e;
        }
    }

    public static function selectField($name, $id, $text, $class_select, $class_text, $arrname, $arrval, $setval, $required=null, $readonly=null) {
        try { ?>
            <span class="<?php echo $class_text; ?>"><?php echo $text; ?></span><br>
            <select name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" class="<?php echo $class_select; ?>" <?php echo $required.' '.$readonly; ?>>
                <?php
                $x = 0;
                foreach($arrname as $val) { ?>
                    <option value="<?php echo self::Enval($val); ?>" <?php if($val == $setval) { echo 'selected'; } ?> ><?php echo $arrval[$x]; ?></option>
                    <?php
                    $x++;
                }
                ?>
            </select>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function selectField() Error: ".$e;
        }
    }

    public static function selefVal(String|float $val1, String|float $val2) {
        return match($val1) {
            $val2 => 'selected ',
            default => ''
        };
    }

    public static function readonlyField(int $readonly) {
        return match($readonly) {
            1 => 'readonly',
            default => ''
        };
    }

    public static function disabledField(int $disabled) {
        return match($disabled) {
            1 => 'disabled',
            default => ''
        };
    }

    public static function dtField($type, $name, $id, $text, $class_input, $class_text, $val=null, $required=null, $readonly=null) {
        try { ?>
            <span class="<?php echo $class_text; ?>"><?php echo $text; ?></span>
            <input type="<?php echo $type; ?>" name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" class="<?php echo $class_input; ?>" value="<?php echo $val; ?>" <?php echo $required.' '.$readonly; ?>>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function radioField() Error: ".$e;
        }
    }

    public static function radioField($name, $id, $text, $class_text, $val1=null, $val2=null, $required=null, $readonly=null) {
        try { ?>
            <input type="radio" name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" value="<?php echo self::Enval($val1); ?>" <?php if( !empty($val1) || !is_null($val1) || $val1 != '' || !empty($val2) || !is_null($val2) || $val2 != '' ) { if( $val1 == $val2 ) { echo 'checked'; } } ?> <?php echo $required.' '.$readonly; ?> />
            <label for="<?php echo self::Enlink($id); ?>" class="<?php echo $class_text; ?>"><?php echo $text; ?></label>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function radioField() Error: ".$e;
        }
    }

    public static function checkField($name, $id, $text, $class_text, $val1=null, $val2=null, $required=null, $readonly=null) {
        try { ?>
            <input type="checkbox" name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" value="<?php echo self::Enval($val); ?>" <?php if( !empty($val1) || !is_null($val1) || $val1 != '' || !empty($val2) || !is_null($val2) || $val2 != '' ) { if( $val1 == $val2 ) { echo 'checked'; } } ?> <?php echo $required.' '.$readonly; ?> />
            <label for="<?php echo self::Enlink($id); ?>" class="<?php echo $class_text; ?>"><?php echo $text; ?></label>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function checkField() Error: ".$e;
        }
    }

    public static function fileField($name, $id, $text, $field_class, $text_class) {
        try { ?>
            <span class="<?php echo $text_class; ?>"><?php echo $text; ?></span><br>
            <input type="file" name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" class="<?php echo $field_class; ?>" />
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function fileField() Error: ".$e;
        }
    }

    public static function buttonField($type, $name, $id, $class, $text) {
        try { ?>
            <button type="<?php echo $type; ?>" name="<?php echo self::Enlink($name); ?>" id="<?php echo self::Enlink($id); ?>" class="<?php echo $class; ?>"><?php echo $text; ?></button>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function fileField() Error: ".$e;
        }
    }


    // NOTIFICATION
    public static function Notrifger($id, $idtrigger, $txt) {
        try { ?>
            <a href="<?php echo '#'.self::Enlink($idtrigger); ?>" id="<?php echo self::Enlink($id); ?>" class="modal-trigger hide"><?php echo 'to-'.$txt; ?></a>
            <?php
        }
        catch(Exception $e) {
            echo "function Notrifger() Error: ".$e;
        }
    }

    public static function Notif($id, $pesan, $tipe, $link='#') {
        try {
            if($tipe == 'notif') { $warna = '#5f5;'; $txt = 'txt-black'; }
            else if($tipe == 'warn') { $warna = '#f00;'; $txt = 'txt-white'; }
            else if($tipe == 'info') { $warna = '#55f;'; $txt = 'txt-white'; }
            else { $warna = '#ccc;'; $txt = 'txt-white'; }
            ?>
            <div class="modal notification borad-20 <?php echo $txt; ?>" id="<?php echo self::Enlink($id); ?>" style="<?php echo 'background: '.$warna; ?>">
                <div class="modal-content choose-date">
                    <a href="<?php echo $link; ?>" class="close-notification no-smoothState modal-close"><i class="ion-android-close"></i></a>
                    <h1 class="uppercase <?php echo $txt; ?> bold italic center"><?php echo $pesan; ?></h1>
                </div>
            </div>
            <?php
        }
        catch(Exception $e) {
            echo "function Notif() Error: ".$e;
        }
    }


    // OTHER
    public static function random(String $str, int $length) {
        try {
            $seed = match($str) {
                'char' => str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),
                'numb' => str_split('0123456789'),
                'pass' => str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),
                'spec' => str_split('`~!@#$%^&*()-_=+[{]}\'\"|;:,<.>/?/'),
                default => str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~!@#$%^&*()-_=+[{]}\'\"|;:,<.>/?/')
            };

            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            echo "function randNumber() Error: ".$e;
        }
    }

    // QR AND BARCODE
    public static function QRCode($path, $kode, $cp, $matrixPointSize) {
        //QR code

        $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
        //$PNG_WEB_DIR = 'qrcard/'.$memberid.'/';

        $errorCorrectionLevel = 'H';
        //$matrixPointSize = 500;

        //$kode = '1000100010001004';
        $filename = $path.$kode.'.png';
        if (!file_exists($filename)){
            QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        }

        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        $resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
    }

    public static function QRCipher($cardno) {
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($cardno, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $ciphertext;
    }
}
?>
