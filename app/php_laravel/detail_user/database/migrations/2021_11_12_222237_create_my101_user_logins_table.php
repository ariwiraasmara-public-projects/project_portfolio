<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMy101UserLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my101_user_login', function (Blueprint $table) {
            $table->string("id_102")->primary();
            $table->string("username")->uniqid();
            $table->string("email")->uniqid();
            $table->text("password");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my101_user_login');
    }
}
