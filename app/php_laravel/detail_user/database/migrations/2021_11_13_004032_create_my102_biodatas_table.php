<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMy102BiodatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my102_biodata', function (Blueprint $table) {
            $table->string("id_102")->primary();
            $table->string("nama")->uniqid();
            $table->string("email")->uniqid();
            $table->date("tgl_lahir")->nullable();
            $table->text("alamat")->nullable();
            $table->text("tlp")->nullable();
            $table->text("status")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my102_biodata');
    }
}
