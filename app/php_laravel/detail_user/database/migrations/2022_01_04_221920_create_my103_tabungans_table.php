<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMy103TabungansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my103_tabungan', function (Blueprint $table) {
            $table->string("id_103")->primary();
            $table->string("id_102");
            $table->integer("tgl");
            $table->string("bulan", 50);
            $table->integer("tahun");
            $table->bigInteger("debet");
            $table->bigInteger("kredit");
            $table->string("byid");
            $table->text("ket")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my103_tabungan');
    }
}
