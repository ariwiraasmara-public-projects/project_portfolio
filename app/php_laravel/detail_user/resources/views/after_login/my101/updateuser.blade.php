@inject('fun', 'App\MyLibs\myfunction')
@extends('index')
@section('content')
@if( isset($_COOKIE['login']) && isset($_COOKIE['username']) && isset($_COOKIE['email']) )
<div class="container m-t-10p borad-20">
    <div class="m-t-30 m-b-10">
        <h1 class="bold center">Ganti Password</h1>

        <div class="m-t-50">
            <form action="{{ route('ulo.update', $fun->enval($userlogin[0]['id_101'])) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <input type="password" name="oldpassword" id="oldpassword" class="form-control" placeholder="Password Lama.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="newpassword" id="newpassword" class="form-control" placeholder="Password Baru.." required />
                </div>

                <div class="center">
                    <a href="{{ route('ulo.show', $_COOKIE['email']); }}" class="btn btn-primary m-r-10">Batal</a>

                    <button type="submit" class="btn btn-primary m-l-10">OK</button>
                </div>

            </form>
        </div>

    </div>
</div>
@else
    <script>window.location.href = "../public/login"; </script>
@endif
@endsection
