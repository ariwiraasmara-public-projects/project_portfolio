@inject('fun', 'App\MyLibs\myfunction')
@extends('index')
@section('content')
@if( isset($_COOKIE['login']) && isset($_COOKIE['username']) && isset($_COOKIE['email']) )
<div class="panel m-t-10">
    <h2 class="bold center">Profil<br/><?php echo $biodata[0]['username']; ?></h2>

    <div class="m-t-30">
        <p>
            <span class="bold">Nama :</span>            <span clas="italic"><?php echo $biodata[0]['nama']; ?></span><br/>
            <span class="bold">Email :</span>           <span clas="italic"><?php echo $biodata[0]['email']; ?></span><br/>
            <span class="bold">Password :</span>        <span clas="italic"><?php echo $biodata[0]['pass']; ?></span><br/>
            <span class="bold">Tanggal Lahir :</span>   <span clas="italic"><?php echo $biodata[0]['tgl_lahir']; ?></span><br/>
            <span class="bold">Alamat :</span>          <span clas="italic"><?php echo $biodata[0]['alamat']; ?></span><br/>
            <span class="bold">No. Telepon :</span>     <span clas="italic"><?php echo $biodata[0]['tlp']; ?></span><br/>
            <span class="bold">Status :</span>          <span clas="italic"><?php echo $biodata[0]['status']; ?></span><br/>
        </p>
    </div>

    <div class="m-t-30 center">
        <a href="{{ route('bio.edit', $fun->enval($biodata[0]->id_102)); }}" class="btn btn-primary">Edit Biodata</a><br/>
    </div>

    <div class="m-t-10 center">
        <a href="{{ route('ulo.edit', $fun->enval($biodata[0]->id_101)); }}" class="btn btn-primary">Ganti Password</a>
    </div>
</div>

<div class="panel menubottom">
    <div class="row">
        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="{{ route('ulo.show', isset($_COOKIE['email'])); }}"><i class="ion-android-person"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="bio"><i class="ion-android-home"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <form action="{{ route('ulo.destroy', $fun->enval(0)) }}" method="post">
                {{ csrf_field() }}
                @method('DELETE')
                <input type="hidden" name="type" id="type" value="{{ $fun->enval('logout') }}" />
                <button type="submit" name="ok" id="ok" class="unbutton"><i class="ion-log-out"></i></button>
            </form>
        </div>

        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-12 center">
            <span class="bold center">Copyright © Syahri Ramadhan Wiraasmara</span>
        </div>
    </div>
</div>
@else
    <script>window.location.href = "../public/login"; </script>
@endif
@endsection
