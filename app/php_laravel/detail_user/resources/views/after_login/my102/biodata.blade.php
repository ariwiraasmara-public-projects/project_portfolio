@inject('fun', 'App\MyLibs\myfunction')
@extends('index')
@section('content')
@if( isset($_COOKIE['login']) && isset($_COOKIE['username']) && isset($_COOKIE['email']) )
<div class="panel m-t-10">
    <h2 class="bold center">SELAMAT DATANG<br/>{{ $fun->readable($nama) }}</h2>

    <div class="m-t-30">
        <table class="table table-responsive width-100">
            <thead>
                <tr>
                    <th class="center">#</th>
                    <th class="center">Nama</th>
                    <th class="center">Email</th>
                    <th class="center">Tanggal Lahir</th>
                    <th class="center">Alamat</th>
                    <th class="center">Nomor Telepon</th>
                    <th class="center">Status</th>
                    <th class="center" colspan="2">--</th>
                </tr>
            </thead>

            <tbody>
                @php $nomor = 1; @endphp
                @foreach($biodata as $bio)
                    <tr>
                        <td>{{ $nomor; }}</td>
                        <td>{{ $fun->readable($bio->nama) }}</td>
                        <td>{{ $fun->readable($bio->email) }}</td>
                        <td>{{ $fun->readable($bio->tgl_lahir) }}</td>
                        <td>{{ $fun->readable($bio->alamat) }}</td>
                        <td>{{ $fun->readable($bio->tlp) }}</td>
                        <td>{{ $fun->readable($bio->status) }}</td>

                        <form action="{{ route('bio.destroy', $fun->readable($fun->enval($bio->id_102))) }}" method="post" id="{{ 'formdel'.$nomor }}">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <td class="center">
                                <a href="{{ route('bio.edit', $fun->readable($fun->enval($bio->id_102))); }}">Edit</a>
                            </td>
                            <td class="center">
                                <button type="button" class="unbutton" id="{{ 'del'.$nomor; }}">Delete</button>
                            </td>
                        </form>
                        <script>
                            $("{{ '#del'.$nomor; }}").click(function(){
                                var cfm = confirm('Anda yakin ingin menghapus data ini?');
                                if(cfm == true) {
                                    $("{{ '#formdel'.$nomor; }}").submit();
                                }
                            });
                        </script>
                    </tr>
                    @php $nomor++; @endphp
                @endforeach
            </tbody>
        </table>

    </div>
</div>

<div class="panel menubottom">
    <div class="row">
        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-12 m-b-10 center">
            <a href="bio/create"><i class="ion-android-add"></i></a>
        </div>
        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="ulo"><i class="ion-android-person"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="bio"><i class="ion-android-home"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <form action="{{ route('ulo.destroy', $fun->enval(0)) }}" method="post">
                {{ csrf_field() }}
                @method('DELETE')
                <input type="hidden" name="type" id="type" value="{{ $fun->enval('logout') }}" />
                <button type="submit" name="ok" id="ok" class="unbutton"><i class="ion-log-out"></i></button>
            </form>
        </div>

        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-12 center">
            <span class="bold center">Copyright © Syahri Ramadhan Wiraasmara</span>
        </div>
    </div>
</div>
@else
    <script>window.location.href = "../public/login"; </script>
@endif
@endsection
