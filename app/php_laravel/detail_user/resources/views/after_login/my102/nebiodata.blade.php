@inject('fun', 'App\MyLibs\myfunction')
@extends('index')
@section('content')
@if( isset($_COOKIE['login']) && isset($_COOKIE['username']) && isset($_COOKIE['email']) )
    @php
    if($ne == 'edit') {
        $url        = route('bio.update', $fun->readable($fun->enval($biodata[0]->id_102)));

        $nama       = $fun->readable($biodata[0]['nama']);
        $email      = $fun->readable($biodata[0]['email']);
        $tgl        = $fun->readable($biodata[0]['tgl_lahir']);
        $alamat     = $fun->readable($biodata[0]['alamat']);
        $tlp        = $fun->readable($biodata[0]['tlp']);
        $status     = $fun->readable($biodata[0]['status']);

        $readonly   = "readonly";
        $route      = "../../bio";
    }
    else {
        $url        = route('bio.store');

        $nama       = null;
        $email      = null;
        $tgl        = date('Y-m-d');
        $alamat     = null;
        $tlp        = null;
        $status     = null;

        $readonly   = "";
        $route      = "../bio";
    }
    @endphp
    <div class="panel">
        <h2 class="bold center"><?php echo $title; ?></h2>

        <div class="m-t-30">
            <form action="{{ $url }}" method="post">
                {{ csrf_field() }}
                @if($ne == 'edit')
                {{ method_field('PATCH') }}
                @endif

                <div class="form-group">
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama.." value="{{ $nama }}" required />
                </div>

                <div class="form-group">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email.." value="{{ $email }}" required {{ $readonly }} />
                </div>

                <div class="form-group">
                    <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control" placeholder="Tanggal Lahir.." value="{{ $tgl }}" />
                </div>

                <div class="form-group">
                    <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat.." value="{{ $alamat }}" />
                </div>

                <div class="form-group">
                    <input type="tel" name="tlp" id="tlp" class="form-control" placeholder="Nomor Telepon.." value="{{ $tlp }}" />
                </div>

                <div class="form-group">
                    <input type="text" name="status" id="status" class="form-control" placeholder="Status.." value="{{ $status }}" />
                </div>

                <div class="center">
                    <button type="submit" class="btn btn-primary center m-r-10">OK</button>
                    <a href="{{ $route }}" class="btn btn-primary center m-l-10">Batal</a>
                </div>
            </form>
        </div>
    </div>
@else
    <script>window.location.href = "../public/login"; </script>
@endif
@endsection
