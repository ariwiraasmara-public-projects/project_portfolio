@inject('fun', 'App\MyLibs\myfunction')
@extends('index')
@section('content')
@if( !isset($_COOKIE['login']) && !isset($_COOKIE['username']) && !isset($_COOKIE['email']) )
<div class="container m-t-10p borad-20">
    <div class="m-t-30 m-b-10 login">
        <h1 class="bold center">Login</h1>

        <div class="m-t-50">
            <form action="{{ route('ulo.store') }}" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <input type="hidden" name="type" id="type" value="{{ $fun->enval('login') }}" />

                <div class="form-group">
                    <input type="text" name="usermail" id="usermail" class="form-control" placeholder="Username / Email.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password.." required />
                </div>

                <div class="center">
                    <button type="submit" class="btn btn-primary center">Masuk</button>
                </div>

                @if( isset($message) )
                    <div class="center m-t-50 m-b-10">
                        <h4 class="bold">{{ $message }}</h4>
                    </div>
                @endif

                <div class="down-10p m-r-10">
                    <div class="right">
                        <a href="{{ route('register') }}" class="bold">Daftar</a>
                        <?php
                        /*
                        <br/>
                        <a href="<?php echo '?'.$lib->setIDParam('pg', 'lupapassword'); ?>" class="">Lupa Password</a>
                        */
                        ?>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="container bottom">
    <div class="center copyright bold">
        Copyright © Syahri Ramadhan Wiraasmara
    </div>
</div>
@else
    <script>window.location.href = "../public/bio"; </script>
@endif
@endsection
