@inject('fun', 'App\MyLibs\myfunction')
@extends('index')
@section('content')
@if(!isset($_COOKIE[$fun->enlink('login')]) && !isset($_COOKIE[$fun->enlink('username')]))
<div class="container m-t-10p borad-20">
    <div class="m-t-30 m-b-10">
        <h1 class="bold center">Daftar Baru</h1>

        <div class="m-t-50">
            <form action="{{ route('ulo.store') }}" method="post">
                {{ csrf_field() }}
				{{ method_field('POST') }}
                <input type="hidden" name="type" id="type" value="{{ $fun->enval('register') }}" />

                <div class="form-group">
                    <input type="text" name="username" id="username" class="form-control" placeholder="Username.." required />
                </div>

                <div class="form-group">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="passver" id="passver" class="form-control" placeholder="Verifikasi Password.." required />
                </div>

                <div class="center">
                    <a href="{{ route('login') }}" class="btn btn-primary m-r-10">Batal</a>

                    <button type="submit" id="daftar" class="btn btn-primary m-l-10">Daftar</button>
                </div>

            </form>

            @if( isset($message) )
                <div class="center m-t-50 m-b-10">
                    <h4 class="bold">{{ $message }}</h4>
                </div>
            @endif

        </div>

    </div>
</div>

<script>
    $("<?php echo '#'.$fun->enlink('passver'); ?>").keyup(function(){

        var pass1 = $("<?php echo '#'.$fun->enlink('password'); ?>").val();
        var pass2 = $("<?php echo '#'.$fun->enlink('passver'); ?>").val();

        if(pass1 == pass2) {
            $("<?php echo '#'.$fun->enlink('daftar'); ?>").attr("disabled", false);
        }
        else {
            $("<?php echo '#'.$fun->enlink('daftar'); ?>").attr("disabled", true);
        }

    });

    $("<?php echo '#'.$fun->enlink('passver'); ?>").keydown(function(){

        var pass1 = $("<?php echo '#'.$fun->enlink('password'); ?>").val();
        var pass2 = $("<?php echo '#'.$fun->enlink('passver'); ?>").val();

        if(pass1 == pass2) {
            $("<?php echo '#'.$fun->enlink('daftar'); ?>").attr("disabled", false);
        }
        else {
            $("<?php echo '#'.$fun->enlink('daftar'); ?>").attr("disabled", true);
        }

    });
</script>
@else
    @php header("location: route('bio')"); @endphp
    <script>//window.location.href = "../bio"; </script>
@endif
@endsection
