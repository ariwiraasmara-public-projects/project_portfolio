@inject('fun', 'App\MyLibs\myfunction')
<!DOCTYPE html>
<html lang="id">
    <head>
        <meta content="authenticity_token" name="csrf-param" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

		<meta content="IE=edge" http-equiv="x-ua-compatible">
		<meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
		<meta content="yes" name="apple-mobile-web-app-capable">
		<meta content="yes" name="apple-touch-fullscreen">

		<link rel="stylesheet" type="text/css" href="{{ URL::to('css/bootstrap-grid.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/bootstrap-grid.min.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/bootstrap-reboot.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/bootstrap-reboot.min.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/bootstrap.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/bootstrap.min.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/custom.css') }}">

		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" />
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,700" />
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine" />

		<!-- Icons -->
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" >
		<link type="image/x-icon"  href="{{ URL::to('images/icon.png') }}" />
		<link rel="shortcut icon"  href="{{ URL::to('images/icon.png') }}" />


		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    	<script type="text/javascript" src="{{ URL::asset('js/default/jquery-2.1.0.min.js') }}"></script>
	</head>

    <body class="p-20">

		@yield('content')

		@php
    		$fun->setTitle($title);
            $fun->setSepatatorTitle(' | ');
            $fun->setProjectTitle("Detail User | Ari Wiraasmara's PHP Laravel Sample Project Portfolio");
    		$fun->getDocTitle();
    	@endphp

    	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    	<script type="text/javascript" src="{{ URL::asset('js/default/bootstrap.bundle.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('js/default/bootstrap.bundle.min.js') }}"></script>
    	<script type="text/javascript" src="{{ URL::asset('js/default/bootstrap.js') }}"></script>
    	<script type="text/javascript" src="{{ URL::asset('js/default/bootstrap.min.js') }}"></script>
    </body>
</html>
