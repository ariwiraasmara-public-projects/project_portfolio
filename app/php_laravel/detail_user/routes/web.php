<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserLoginController;
use App\Http\Controllers\BiodataController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

});

Route::get('/login', function () {
	return view('before_login.login', ['title'=>'Login']);
})->name('login');

Route::get('/register', function () {
	return view('before_login.register', ['title'=>'Daftar']);
})->name('register');

Route::post('proclogin', function () {
	return view('before_login.register', ['title'=>'Daftar']);
})->name('tologin');

Route::resource('home', '\App\Http\Controllers\HomeController');
Route::resource('ulo', '\App\Http\Controllers\UserLoginController');
Route::resource('bio', '\App\Http\Controllers\BiodataController');
