<?php
class my101_user_login_controller extends UserLoginModel implements my101_user_login_controller_interface {

    public function checkRow(String $fields="*", String $clause=null) {
        if($fields=="" || is_null($fields)) $fields = "*";

        $result = $this->getConnection()->query("SELECT $fields from my101_user_login $clause");
        return $result->num_rows;
    }

    public function getData(String $fields="*", String $clause=null, $type=1) {
        if($fields=="" || is_null($fields)) $fields = "*";

        $result = $this->getConnection()->query("SELECT $fields from my101_user_login as a $clause");
        $data = array();
        if($type > 1) {
            while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $data[] = $row;
            }
        }
        else {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $data[] = $row;
        }

        return $data;
    }
    

    public function insertData(array $val=null) {
        $result = $this->getConnection()->prepare($this->safe("INSERT into my101_user_login(id_101, username, email, password) values(?,?,?,?)"));
        $result->bind_param('ssss', $this->escape($val[0]), $this->escape($val[1]), $this->escape($val[2]), $this->escape($val[3]));
        $result->execute();
    }

    public function updateData(String $id='__none__', String $val=null) {
        $result = $this->getConnection()->prepare($this->safe("UPDATE my101_user_login SET password=? where id_101=?"));
        $result->bind_param('ss', $this->escape($val), $this->escape($id));
        $result->execute();
    }

    public function deleteData(String $id='__none__') {
        $result = $this->getConnection()->prepare($this->safe("DELETE from my101_user_login where id_101='?'"));
        $result->bind_param('s', $this->escape($id));
        $result->execute();
    }

    public function getLastID(String $field_yg_dicari="__none__",
                              String $where="__none__",
                              String $order="__none__",
                              int $digitangkaterakhir=0,
                              int $banyaknyapemisah_dalamangka=0,
                              String $pemisah=null, String $debug = null) {
        try {
            $whereclause = match(true) {
                $where == "" || empty($where) || is_null($where) => "",
                default => "where $where",
            };

            $fieldorder = match(true) {
                $order == "" || empty($order) || is_null($order) => "",
                default => "order by $order",
            };


            $clause = "$whereclause $fieldorder limit 1";

            if(empty($debug) || is_null($debug)) {
                $chk = $this->checkRow($field_yg_dicari, $clause);

                if($chk > 0) {
                    $data   = $this->getData($field_yg_dicari, $clause);
                    $graid  = substr($data[0][$field_yg_dicari], $digitangkaterakhir);
                    $gp     = (int)$graid + 1;
                    $intaid = str_pad($gp, $banyaknyapemisah_dalamangka, $pemisah, STR_PAD_LEFT);
                }
                else {
                    $intaid = str_pad(1, $banyaknyapemisah_dalamangka, $pemisah, STR_PAD_LEFT);
                }

                return "UL#".$intaid;
            }
            else {
                return ""; //$clause;
            }
        }
        catch(Exception $e) {
            return "function getLastID() Error: ".$e;
        }
    }
}
?>