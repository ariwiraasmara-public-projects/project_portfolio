<?php
interface my101_user_login_controller_interface {
    public function checkRow(String $fields="*", String $clause=null);
    public function getData(String $fields="*", String $clause=null, $type=1);
    public function insertData(array $val=null);
    public function updateData(String $id='__none__', String $val=null);
    public function deleteData(String $id='__none__');
    public function getLastID(String $field_yg_dicari="__none__",
                              String $where="__none__",
                              String $order="__none__",
                              int $digitangkaterakhir=0,
                              int $banyaknyapemisah_dalamangka=0,
                              String $pemisah=null, String $debug = null);
}
?>