<?php
class my102_biodata_controller extends BiodataModel implements my102_biodata_controller_interface {

    public function checkRow(String $fields="*", String $clause=null) {
        if($fields=="" || is_null($fields)) $fields = "*";

        $result = $this->getConnection()->query("SELECT $fields from my102_biodata $clause");
        return $result->num_rows;
    }

    public function checkEmail(String $email = "__none__") {
        $res = $this->getConnection()->prepare($this->safe("SELECT * from my102_biodata where email=?"));
        $res->bind_param('s', $email);
        $res->execute();
        $res->store_result();
        return $res->num_rows;
        $res->free_result();
    }

    public function getData(String $fields="*", String $clause=null, $type=1) {
        if($fields=="" || is_null($fields)) $fields = "*";

        $result = $this->getConnection()->query("SELECT $fields from my102_biodata $clause");
        $data = array();
        if($type > 1) {
            while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $data[] = $row;
            }
        }
        else {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $data[] = $row;
        }

        return $data;
    }

    public function insertData(array $val=null, int $type=2) {
        if($type > 2) {
            $result = $this->getConnection()->prepare($this->safe("INSERT into my102_biodata(id_102, nama, email, tgl_lahir, alamat, tlp, status) values(?,?,?,?,?,?,?)"));
            $result->bind_param('sssssss', $this->escape($val[0]), $this->escape($val[1]), $this->escape($val[2]), $this->escape($val[3]), $this->escape($val[4]), $this->escape($val[5]), $this->escape($val[6]));
            $result->execute();
        }
        else {
            $result = $this->getConnection()->prepare($this->safe("INSERT into my102_biodata(id_102, email) values(?,?)"));
            $result->bind_param('ss', $this->escape($val[0]), $this->escape($val[1]));
            $result->execute();
        }
    }

    public function updateData(String $id='__none__', String $nama = '__none__', 
                               String $tgl = '__none__', String $alamat = '__none__', 
                               String $tlp = '__none__', String $status = '__none__') {
        $result = $this->getConnection()->prepare($this->safe("UPDATE my102_biodata SET nama=?, tgl_lahir=?, alamat=?, tlp=?, status=? where id_102=?"));
        $result->bind_param('ssssss', $this->escape($nama), $this->escape($tgl), $this->escape($alamat), $this->escape($tlp), $this->escape($status), $this->escape($id));
        $result->execute();
    }

    public function deleteData(String $id='__none__') {
        $result = $this->getConnection()->prepare($this->safe("DELETE from my102_biodata where id_102='$id'"));
        $result->bind_param('s', $this->escape($id));
        $result->execute();
    }

    public function getLastID(String $field_yg_dicari="__none__",
                              String $where="__none__",
                              String $order="__none__",
                              int $digitangkaterakhir=0,
                              int $banyaknyapemisah_dalamangka=0,
                              String $pemisah=null, String $debug = null) {
        try {
            $whereclause = match(true) {
                $where == "" || empty($where) || is_null($where) => "",
                default => "where $where",
            };

            $fieldorder = match(true) {
                $order == "" || empty($order) || is_null($order) => "",
                default => "order by $order",
            };


            $clause = "$whereclause $fieldorder limit 1";

            if(empty($debug) || is_null($debug)) {
                 $chk = $this->checkRow($field_yg_dicari, $clause);

                if($chk > 0) {
                    $data   = $this->getData($field_yg_dicari, $clause);
                    $graid  = substr($data[0][$field_yg_dicari], $digitangkaterakhir);
                    $gp     = (int)$graid + 1;
                    $intaid = str_pad($gp, $banyaknyapemisah_dalamangka, $pemisah, STR_PAD_LEFT);
                }
                else {
                    $intaid = str_pad(1, $banyaknyapemisah_dalamangka, $pemisah, STR_PAD_LEFT);
                }

                return 'BI#'.$intaid;
            }
            else {
                return ""; //$clause;
            }
        }
        catch(Exception $e) {
            return "function getLastID() Error: ".$e;
        }
    }
}
?>