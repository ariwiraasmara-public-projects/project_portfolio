<?php
interface my102_biodata_controller_interface {
    public function checkRow(String $fields="*", String $clause=null);
    public function checkEmail(String $email = "__none__");
    public function getData(String $fields="*", String $clause=null, $type=1);
    public function insertData(array $val=null, int $type=2);
    public function updateData(String $id='__none__', String $nama = '__none__', 
                              String $tgl = '__none__', String $alamat = '__none__', 
                              String $tlp = '__none__', String $status = '__none__');
    public function deleteData(String $id='__none__');
    public function getLastID(String $field_yg_dicari="__none__",
                              String $where="__none__",
                              String $order="__none__",
                              int $digitangkaterakhir=0,
                              int $banyaknyapemisah_dalamangka=0,
                              String $pemisah=null, String $debug = null);
}
?>