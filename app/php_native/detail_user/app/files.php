<?php
class file {

    public function __construct(protected $pg = "") {
       
    }

    protected function getPath() {
        return "view/";
    }

    public function setPage(String $str) {
        $this->pg = htmlspecialchars(htmlentities(addslashes(strtolower($str))));
    }

    protected function getPage() {
        return $this->pg;
    }

    protected function getSubPage() {
        return $this->sp;
    }

    protected function atDir() {
        return match($this->getPage()) {
            "login"         => "my101_userlogin.view/",
            "register"      => "my101_userlogin.view/",
            "lupapassword"  => "my101_userlogin.view/",
            "userprofil"    => "my101_userlogin.view/",
            "updateuser"    => "my101_userlogin.view/",

            "biodata"       => "my102_biodata.view/",
            "nebiodata"     => "my102_biodata.view/",

            "logout"        => "my100_others.view/",
            default         => "my100_others.view/"
        };
    }

    protected function atSeries() {
        return match($this->getPage()) {
            "login"         => "@UL001#",
            "register"      => "@UL002#",
            "lupapassword"  => "@UL003#",
            "userprofil"    => "@UL004#",
            "updateuser"    => "@UL005#",

            "biodata"       => "@BI001#",
            "dashboard"     => "@BI001#",
            "home"          => "@BI001#",
            "nebiodata"     => "@BI002#",

            "logout"        => "@SY001#",

            default => "@ER001#"
        };
    }

    protected function onFile() {
        return match($this->getPage()) {
            "404"   => "[404]",
            default => "[".$this->getPage()."]"
        };
    }

    protected function viewExtension() {
        return ".flash.php";
    }

    public function render() {
        return  $this->getPath().
                $this->atDir().
                $this->atSeries().
                $this->onFile().
                $this->viewExtension();
    }

    public function include(String $str) {
        $series = match($str) {
            'menubottom'    => '@IN001#',
            default         => $str
        };
        return require($this->getPath().'my100_others.view/'.$series.'['.$str.'].flash.php');
    }

}
?>