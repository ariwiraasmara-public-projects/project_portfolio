<!DOCTYPE html>
<html lang="id">
    <head>
        <title></title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<meta name="keywords" content="Syahri Ramadhan Wiraasmara's Private Website, Personal Use Only" />
				
		<meta content="IE=edge" http-equiv="x-ua-compatible">
		<meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
		<meta content="yes" name="apple-mobile-web-app-capable">
		<meta content="yes" name="apple-touch-fullscreen">

		<meta content="authenticity_token" name="csrf-param" />
		<meta content="<?php echo $lib->tokenValue(); ?>" name="csrf-token" />

    	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine"/>
    	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
    
		<!--
    	<link href='../../images/icon/@12! W Circle.png' type='image/x-icon'/>
    	<link href='../../images/icon/@12! W Circle.png' rel='shortcut icon'/>

        <link href="../../css/default/ionicons.min.css" rel="stylesheet" type="text/css">
		<link href="../../css/default/keyframes.css" rel="stylesheet" type="text/css">
		<link href="../../css/default/materialize.min.css" rel="stylesheet" type="text/css">
		<link href="../../css/default/swiper.css" rel="stylesheet" type="text/css">
		<link href="../../css/default/swipebox.min.css" rel="stylesheet" type="text/css">
		<link href="../../css/default/style.css" rel="stylesheet" type="text/css">
				
		<link href="../../css/mycss/index_beforelogin.css" rel="stylesheet" type="text/css">
		<link href="../../css/mycss/my_css.css" rel="stylesheet" type="text/css">
		<link href="../../css/mycss/my_theme.css" rel="stylesheet" type="text/css">

		<script src="../../js/default/jquery-2.1.0.min.js"></script>
		-->

		<link href="resources/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
		<link href="resources/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">
		<link href="resources/css/bootstrap-reboot.css" rel="stylesheet" type="text/css">
		<link href="resources/css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css">
		<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="resources/css/style.css" rel="stylesheet" type="text/css">
		<link href="resources/css/custom.css" rel="stylesheet" type="text/css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	</head>

    <body class="p-20">
	
		<?php
		require("routes.php");
		$lib->getDocTitle();
		?>

		<script src="resources/js/bootstrap.bundle.js"></script>
		<script src="resources/js/bootstrap.bundle.min.js"></script>
		<script src="resources/js/bootstrap.js"></script>
		<script src="resources/js/bootstrap.min.js"></script>
    </body>
</html>