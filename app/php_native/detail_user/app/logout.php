<?php
require_once("util/connectionAbstraction.set.php");
require_once("util/commonlibInterface.set.php");
require_once("util/connection.set.php");
require_once("util/commonlib.set.php");
require_once("util/varcache.set.php");

$lib    = new commonlib();
$cache  = new varcache();

$lib->setURLHome("");
$lib->logoutSystem(array("islogin","tokenid","user_id","user_name","user_email"));
$cache->setUser(null,null,null);
session_destroy();
header('location: index.php?'.$lib->setIDParam('token', $lib->tokenValue()));
?>