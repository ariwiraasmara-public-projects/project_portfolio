<?php
abstract class UserLoginModel extends commonlib {
    
    protected String $id;
    protected String $user;
    protected String $email;
    protected String $pass;
    public function mSet(String $a = "__null_", String $b = "__null_", 
                         String $c = "__null_", String $d = "__null_") {
        $this->$id = match($a) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $a
                    };

        $this->$user = match($b) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $b
                    };

        $this->$email = match($c) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $c
                    };

        $this->$pass = match($d) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $d
                    };
    }

    public function mGet(String $str) {
        return match($this->escape($str)) {
            'id'     => $this->getID(),
            'user'   => $this->getUser(),
            'email'  => $this->getEmail(),
            'pass'    => $this->getPass(),
            default  => null,
        };
    }

    public function getID() {
        return $this->$id;
    }

    public function getUser() {
        return $this->$user;
    }

    public function getEmail() {
        return $this->$email;
    }

    public function getPass() {
        return $this->$pass;
    }

}
?>