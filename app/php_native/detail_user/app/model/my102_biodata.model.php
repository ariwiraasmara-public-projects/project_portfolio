<?php
abstract class BiodataModel extends commonlib {
    
    protected String $id;
    protected String $nama;
    protected String $email;
    protected String $tgl;
    protected String $alamat;
    protected String $tlp;
    protected String $status;
    public function mSet(String $a = "__null__", String $b = "__null__", String $c = "__null__", 
                        String $d = "__null__", String $e = "__null__", String $f = "__null__", 
                        String $g = "__null__") {
        $this->id = match($a) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $a
                    };

        $this->nama = match($b) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $b
                    };

        $this->email = match($c) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $c
                    };

        $this->tgl_lahir = match($d) {
                            "__null__" => "__null__",
                            null => "__null__",
                            default => $d
                        };

        $this->alamat = match($e) {
                            "__null__" => "__null__",
                            null => "__null__",
                            default => $e
                        };

        $this->tlp = match($f) {
                        "__null__" => "__null__",
                        null => "__null__",
                        default => $f
                    };

        $this->status = match($g) {
                            "__null__" => "__null__",
                            null => "__null__",
                            default => $g
                        };
    }

    public function mGet(String $str) {
        return match($this->escape($str)) {
            'id'     => $this->getID(),
            'nama'   => $this->getNama(),
            'email'  => $this->getEmail(),
            'tgl'    => $this->getTgl(),
            'alamat' => $this->getAlamat(),
            'tlp'    => $this->getTlp(),
            'status' => $this->getStatus(),
            default  => null,
        };
    }

    public function getID() {
        return $this->id;
    }

    public function getNama() {
        return $this->nama;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTgl() {
        return $this->tgl_lahir;
    }

    public function getAlamat() {
        return $this->alamat;
    }

    public function getTlp() {
        return $this->tlp;
    }

    public function getStatus() {
        return $this->status;
    }

}
?>