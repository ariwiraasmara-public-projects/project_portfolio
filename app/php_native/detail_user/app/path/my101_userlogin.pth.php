<?php
class my101_userlogin_path {

    public function __construct(protected String $rootPath = "process", 
                                private String $proc = "",
                                protected String $tokenname = "",
                                protected String $tokenvalue = "") {
        
    }

    protected function rootPath() {
        return $this->rootPath."/";
    }

    protected function atDir() {
        return "my101_userlogin.proc/";
    }

    public function setToken(String $name, String $value) {
        $this->tokenname  = $name;
        $this->tokenvalue = $value;
    }

    protected function getToken() {
        return $this->tokenname."=".$this->tokenvalue;
    }

    protected function setProc(String $str) {
        $this->proc = strtolower($str);
    }

    protected function getProc() {
        return $this->proc;
    }

    protected function atSeries() {
        return match($this->getProc()) {
            "login"             => "@001",
            "register"          => "@002",
            "lupapassword"      => "@003",
            "profil"            => "@004",
            "updateprofil"      => "@005",
            default             => $this->getProc()
        };
    }

    protected function onFile() {
        return "[".$this->getProc()."]";
    }

    protected function extension() {
        return ".proc.php";
    }

    public function proc(String $str, String $param=null) {
        $this->setProc($str);
        return $this->rootPath().$this->atDir().$this->atSeries($str).$this->onFile().$this->extension()."?".$this->getToken().'&'.$param;
    }

}
?>