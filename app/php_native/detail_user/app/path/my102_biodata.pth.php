<?php
class my102_biodata_path {

    public function __construct(protected String $rootPath = "process", 
                                private String $proc = "",
                                protected String $tokenname = "",
                                protected String $tokenvalue = "") {

    }

    protected function rootPath() {
        return $this->rootPath."/";
    }

    protected function atDir() {
        return "my102_biodata.proc/";
    }

    public function setToken(String $name, String $value) {
        $this->tokenname  = $name;
        $this->tokenvalue = $value;
    }

    protected function getToken() {
        return $this->tokenname."=".$this->tokenvalue;
    }

    public function setProc(String $str) {
        $this->proc = strtolower($str);
    }

    protected function getProc() {
        return $this->proc;
    }

    protected function atSeries() {
        return match($this->getProc()) {
            "insert" => "@001",
            "update" => "@002",
            "delete" => "@003",
            default => $this->getProc()
        };
    }

    protected function onFile() {
        return "[".$this->getProc()."]";
    }

    protected function extension() {
        return ".proc.php";
    }

    public function proc(String $str, String $param=null) {
        $this->setProc($str);
        return $this->rootPath().$this->atDir().$this->atSeries($str).$this->onFile().$this->extension()."?".$this->getToken().'&'.$param;
    }

}
?>