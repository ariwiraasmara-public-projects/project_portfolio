<?php
//error_reporting(0);
require_once("../../util/connectionAbstraction.set.php");
require_once("../../util/commonlibInterface.set.php");
require_once("../../util/connection.set.php");
require_once("../../util/commonlib.set.php");
require_once("../../util/varcache.set.php");
require_once("../../model/my101_userlogin.model.php");
require_once("../../controller/my101_userlogin.face.php");
require_once("../../controller/my101_userlogin.cont.php");
require_once("../../model/my102_biodata.model.php");
require_once("../../controller/my102_biodata.face.php");
require_once("../../controller/my102_biodata.cont.php");

$lib  = new commonlib();
$var  = new varcache();

$proc1 = $lib->POST('procont1', 2);
$cont1 = match($proc1) {
    "as101" => new my101_user_login_controller(),
    "as102" => new my102_biodata_controller(),
    default => ""
};

$proc2 = $lib->POST('procont2', 2);
$cont2 = match($proc2) {
    "as101" => new my101_user_login_controller(),
    "as102" => new my102_biodata_controller(),
    default => ""
};
?>