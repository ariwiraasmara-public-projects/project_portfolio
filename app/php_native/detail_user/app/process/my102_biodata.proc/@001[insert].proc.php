<?php
require("../index.proc.php"); 
if( $lib->GET('token', 1) ) {
    if( $lib->POST('@Syslog:token', 1) ) {
        if( $lib->POST('@Process:', 1) ) {

            $nama       = $lib->escape($lib->POST('nama', 1));
            $email      = $lib->escape($lib->POST('email', 1));
            $tgl        = $lib->escape(match($lib->POST('tgl_lahir', 1)) {
                            null => '',
                            default => $lib->POST('tgl_lahir', 1)
                        });
            $alamat     = $lib->escape(match($lib->escape($lib->POST('alamat', 1))) {
                            null => '',
                            default => $lib->escape($lib->POST('alamat', 1))
                        });
            $tlp        = $lib->escape(match($lib->escape($lib->POST('tlp', 1))) {
                            null => '',
                            default => $lib->escape($lib->POST('tlp', 1))
                        });
            $status     = $lib->escape(match($lib->escape($lib->POST('status', 1))) {
                            null => '',
                            default => $lib->escape($lib->POST('status', 1))
                        });

            $tid102     = $cont1->getLastID('id_102', "", "id_102 DESC", -5, 5, 0);
            //echo $tid102;
            $res        = $cont1->insertData(array($tid102, $nama, $email, $tgl, $alamat, $tlp, $status), 6);
            //$lib->setOneCookie("error", "Biodata telah ditambahkan!", 2, 0, 0, 1);
            header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
        }
        else {
            echo 3;
            header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
        }
    }
    else {
        echo 2;
        header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
    }
}
else {
    echo 1;
    header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
}
?>