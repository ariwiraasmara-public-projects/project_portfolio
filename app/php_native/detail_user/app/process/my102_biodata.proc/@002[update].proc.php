<?php
require("../index.proc.php"); 
if( $lib->GET('token', 1) ) {
    if( $lib->POST('@Syslog:token', 1) ) {
        if( $lib->POST('@Process:', 1) ) {

            //echo $lib->GET('bid102', 2);
            $nama       = $lib->escape($lib->POST('nama', 1));
            $tgl        = match($lib->POST('tgl_lahir', 1)) {
                            null => '',
                            default => $lib->POST('tgl_lahir', 1)
                        };
            $alamat     = match($lib->escape($lib->POST('alamat', 1))) {
                            null => '',
                            default => $lib->escape($lib->POST('alamat', 1))
                        };
            $tlp        = match($lib->escape($lib->POST('tlp', 1))) {
                            null => '',
                            default => $lib->escape($lib->POST('tlp', 1))
                        };
            $status     = match($lib->escape($lib->POST('status', 1))) {
                            null => '',
                            default => $lib->escape($lib->POST('status', 1))
                        };

            $res        = $cont1->updateData($lib->GET('bid102', 2), $nama, $tgl, $alamat, $tlp, $status);
            //$lib->setOneCookie("error", "Biodata telah ditambahkan!", 2, 0, 0, 1);
            header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
        }
        else {
            echo 3;
            header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
        }
    }
    else {
        echo 2;
        header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
    }
}
else {
    echo 1;
    header('location: ../../?'.$lib->setIDParam('token', $lib->tokenValue()));
}
?>