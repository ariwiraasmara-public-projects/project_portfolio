<?php
$pg = strtolower($lib->GET('pg', 2));
if( isset($_COOKIE[$lib->enlink('islogin')]) ) {
    if( isset($_COOKIE[$lib->enlink('user_id')]) ) {
        if( isset($_COOKIE[$lib->enlink('user_email')]) ) {

            //echo "Masuk 1";
            if($pg == "" || $pg == "home" || $pg == "biodata" || is_null($pg) || empty($pg)) {
                $file->setPage("biodata");
                $lib->setTitle("Dashboard");
                $cont  = new my102_biodata_controller();
                $cont2 = new my101_user_login_controller();
            }
            else if($pg == 'newbiodata') {
                $file->setPage("nebiodata");
                $lib->setTitle("Biodata Baru");
                $cont = new my102_biodata_controller();
            }
            else if($pg == 'editbiodata') {
                $file->setPage("nebiodata");
                $lib->setTitle("Edit Biodata");
                $cont = new my102_biodata_controller();
            }
            else if($pg == 'profil' || $pg == 'userprofil') {
                $file->setPage("userprofil");
                $lib->setTitle("Profil");
                $cont = new my101_user_login_controller();
            }
            else if($pg == 'updatepass') {
                $file->setPage("updateuser");
                $lib->setTitle("Ganti Password");
                $cont = new my101_user_login_controller();
            }
            else if($pg == 'logout') {
                //$file->setPage("logout");
                header('location: logout.php?'.$lib->setIDParam('token', $lib->tokenValue()));
            }
            else {
                $file->setPage("404");
                $lib->setTitle("404!");
                //require($file->render());
            }

            //require($file->render());
        }
        else {
            //echo "Keluar 1";
            header('location: logout.php?'.$lib->setIDParam('token', $lib->tokenValue()));
        }
    }
    else {
        // echo $lib->getCookie('tokenid', 2);
        //echo "Keluar 2";
        header('location: logout.php?'.$lib->setIDParam('token', $lib->tokenValue()));
    }
}
else {
    if($pg == "" || $pg == "login" || is_null($pg) || empty($pg) ) {
        $file->setPage("login");
        $lib->setTitle("Login");
    }
    else if($pg == "register" || $pg == "daftar") {
        $file->setPage("register");
        $lib->setTitle("Daftar");
    }
    else if($pg == "lupapassword" || $pg == "forgotpassword" || $pg == "pass") {
        $file->setPage("lupapassword");
        $lib->setTitle("Lupa Password");
    }
    else { 
        $file->setPage("404");
        $lib->setTitle("404!");
    }

    //require($file->render());
}

require($file->render());
?>