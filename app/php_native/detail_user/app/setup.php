<?php
//error_reporting(0);
@session_start();
date_default_timezone_set("Asia/Jakarta");
require_once('cache.php');
require_once("util/connectionAbstraction.set.php");
require("util/commonlibInterface.set.php");
require_once("util/connection.set.php");
require("util/commonlib.set.php");
require_once("util/varcache.set.php");
require("model/my101_userlogin.model.php");
require("model/my102_biodata.model.php");
require('controller/my101_userlogin.face.php');
require('controller/my102_biodata.face.php');
require("controller/index.cont.php");
require("path/index.pth.php");
require("files.php");
//require("html.php");

$cache  = new varcache();
$con    = new connection();
$lib    = new commonlib();

//$cache->startCache();
$lib->setURLHome("project_porfolio/app/php_native/detail_user/app/");
$lib->setSepatatorTitle(' | ');
$lib->setProjectTitle("Detail User | Ari Wiraasmara's PHP Native Sample Project Portfolio");

$tona = $lib->tokenName('@[Formid]:=>');
$tova = $lib->tokenValue();

$path101->setToken($tona, $tova);
$path102->setToken($tona, $tova);
$file = new file();
//$cache->endCache();
?>