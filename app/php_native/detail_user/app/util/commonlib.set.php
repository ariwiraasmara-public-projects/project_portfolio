<?php
class commonlib extends connection  {

    public $urlhome;
    public function setURLHome(String $str) {
        $this->urlhome = $str;
    }

    public function getURLHome() {
        return $this->urlhome;
    }

    // GET AND POST PARAMETER
    public function POST(String|float $str=null, int $state=0, int $position=null) {
        $str = $this->escape($str);
        try {
            return match(true) {
                ($position != null) || ($position != '') || !empty($position) || !is_null($position) => match($state) {
                    1 => @$_POST[$this->enlink($str)][$position],
                    2 => $this->denval(@$_POST[$this->enlink($str)][$position]),
                    default => @$_POST[$str][$position],
                },
                default => match($state) {
                    1 => @$_POST[$this->enlink($str)],
                    2 => $this->denval(@$_POST[$this->enlink($str)]),
                    default => @$_POST[$str],
                },
            };
        }
        catch(Exception $e) {
            //return "function POST() Error: ".$e;
            return "";
        }
    }
    
    public function GET(String|float $str=null, int $state=0) {
        $str = $this->escape($str);
        try {
            return match($state) {
                1 => @$_GET[$this->enlink($str)],
                2 => $this->denval(@$_GET[$this->enlink($str)]),
                default => @$_GET[$str],
            };
        }
        catch(Exception $e) {
            //return "function GET() Error: ".$e;
            return "";
        }
    }
    
    public function FILE(String|float $str=null, String $tipe=null, int $state=0, int $position=null) {
        try {
            return match(true) {
                $position == null || $position == '' || empty($position) || is_null($position) => match($state) {
                    1 => @$_FILES[$this->enlink($str)][$tipe],
                    default => @$_FILES[$str][$tipe],
                },
                default => match($state) {
                    1 => @$_FILES[$this->enlink($str)][$tipe][$position],
                    default => @$_FILES[$str][$tipe][$position],
                }
            };
        }
        catch(Exception $e) {
            //return "function FILE() Error: ".$e;
            return "";
        }
    }
    
    
    // SET TITLE
    public $title, $project_title, $separator_title;
    public function setTitle(String|float $title = null) {
        try {
            $this->title = $title;
        }
        catch(Exception $e) {
            //echo "function setTitle() Error: ".$e;
            return "";
        }
    }

    public function setProjectTitle(String|float $title = null) {
        try {
            $this->project_title = $title;
        }
        catch(Exception $e) {
            //echo "function setProjectTitle() Error: ".$e;
            return "";
        }
    }

    public function setSepatatorTitle(String $title = null) {
        try {
            $this->separator_title = $title;
        }
        catch(Exception $e) {
            //echo "function setSepatatorTitle() Error: ".$e;
            return "";
        }
    }
    
    public function getTitle() {
        try {
            return $this->title;
        }
        catch(Exception $e) {
            //return "function getTitle() Error: ".$e;
            return "";
        }
    }

    public function getProjectTitle() {
        try {
            return $this->project_title;
        }
        catch(Exception $e) {
            //return "function getProjectTitle() Error: ".$e;
            return "";
        }
    }

    public function getSeparatorTitle() {
        try {
            return $this->separator_title;
        }
        catch(Exception $e) {
            //return "function getSeparatorTitle() Error: ".$e;
            return "";
        }
    }
    
    public function getDocTitle(String|float $title = "__-__") { ?>
        <script type="text/javascript">
            document.title = "<?php echo $this->getTitle().$this->getSeparatorTitle().$this->getProjectTitle(); ?>";
            $("<?php echo '#'.$title; ?>").html("<?php echo $this->getTitle(); ?>");
        </script>
        <?php
    }

    public function redirect(String $str) {
        return header("location: ".$str);
    }
    
    
    
    
    // SAFETY CONNECTION STRING FROM INJECTION
    public function escape(String|float $str=null) {
        try {
            return htmlspecialchars(htmlentities(addslashes($str)));
        }
        catch(Exception $e) {
            //return "Error Function escape() : ".$e;
            return "";
        }
    }

    public function safe(String|float $str=null) {
        try {
            //return mysqli_real_escape_string($this->getConnection(), $this->escape($str));
            return $this->getConnection()->real_escape_string($str);
        }
        catch(Exception $e) {
            //return "Error Function safe() : ".$e;
            return "";
        }
    }


    // READABLE FORMAT TEXT
    public function readable(String|float $str=null) {
        try {
            return html_entity_decode(htmlspecialchars_decode($str));
        }
        catch(Exception $e) {
            //return "Error Function READABLE() : ".$e;
            return "";
        }
    }

    public function formatnumber(int|float $str=null, $koma=0) {
        try {
            return number_format($str, $koma, ',', '.');
        }
        catch(Exception $e) {
            //return "Error Function formatnumber() : ".$e;
            return "";
        }
    }

    public function rupiah(int|float $str=null) {
        try {
            return 'Rp. '.$this->formatnumber($str, 2);
        }
        catch(Exception $e) {
            //return "Error Function Rupiah() : ".$e;
            return "";
        }
    }
    
    
    // SEND EMAIL
    public function toSendMail(String $to=null, String $subject=null, String $txt=null, String $headers=null) {
        try {
            //$headers = "From: dev.ariwiraasmara.emailgateway@gmail.com";
            if($headers == null || $headers == "" || is_null($headers) || is_empty($headers) ) {
                $from = "From: dev.ariwiraasmara.emailgateway@gmail.com";
            }
            else {
                $from = "From: ".$headers;
            }
            mail($to,$subject,$txt,$from);
        }
        catch(Exception $e) {
            //echo "function toSendMail() Error: ".$e;
            return "";
        }
    }

    public function greetings() {
        try {
            $hour = date('Hm');
            return match(true) {
                $hour < 400 => 'Tengah Malam',
                $hour <  600 => 'Waktu Shubuh',
                $hour < 1200 => 'Selamat Pagi!',
                $hour < 1500 => 'Selamat Siang!',
                $hour < 1800 => 'Selamat Sore!',
                $hour < 1900 => 'Waktu Maghrib!',
                $hour < 2200 => 'Selamat Malam!',
                $hour < 2359 => 'Sudah Larut Malam',
            };
        }
        catch(Exception $e) {
            //echo "function greetings() Error: ".$e;
            return "";
        }
    }

    
    // ENCRYPT & DECRYPT TEXT
    public function toCrypt(String|float $str) {
        //return crypt($str, '$6$rounds='.$this->randNumber(7, 1));
        return crypt($str, 'rounds='.$this->randNumber(9, 1).'>');
    }

    public function toMD5(String|float $str) {
        return md5($str, TRUE);
    }

    public function toSHA1(String|float $str) {
        return sha1($str, TRUE);
    }

    public function toSHA512(String|float $str) {
        return hash("sha512", $str);
    }

    public function toHaval256(String|float $str) {
        return hash("haval256,5", $str);
    }

    public function tokenName(String $str) {
        return $this->toCrypt($this->toSHA512($this->toMD5($str)));
    }

    public function tokenValue() {
        return $this->toCrypt($this->toSHA512($this->toMD5($this->toHaval256($this->encrypt(openssl_random_pseudo_bytes($this->randNumber(3)))))));
    }

    public function enlink(String|float $str) {
        try {
            return $this->toCrypt($this->toSHA512($this->toMD5($str)));
        }
        catch(Exception $e) {
            //return 'function enlink() Error: '.$e;
            return "";
        }
    }
    
    public function enparam(String $a, String $b, String $c, String $d) {
        try {
            return $this->setIDParam($a, $b).'&'.$this->setIDParam($c, $d);
        }
        catch(Exception $e) {
            //return 'function enparam Error: '.$e;
            return "";
        }
    }

    public function setIDParam(String|float $id, String|float $val) {
        try {
            return $this->enlink($id).'='.$this->enval($val);
        }
        catch(Exception $e) {
            //return "Error Function setIDParam() : ".$e;
            return "";
        }
    }

    public function getSafeParam(String|float $id) {
        try {
            return $this->safe($this->denval(@$_GET[$this->enlink($id)]));
        }
        catch(Exception $e) {
            //return "Error Function getIDParam() : ".$e;
            return "";
        }
    }


    // Encrypt Value with Decrypt
    public function enval($str) {
        try {
            return bin2hex(base64_encode( $str ));
        }
        catch(Exception $e) {
           // return 'Error function di enval(): '.$e;
           return "";
        }
    }
    
    public function denval($str) {
        try {
            return base64_decode(hex2bin( $str ));
        }
        catch(Exception $e) {
            //return 'function denval() Error: '.$e;
            return "";
        }
    }
    
    // SESSION AND COOKIE
    public function setOneSession(String $str, String $val, int $type=0) {
        try {
            if( $type > 1 ) {
                $_SESSION[$this->enlink($str)] = $this->enval($val);
            }
            else if( $type == 1 ) {
                $_SESSION[$this->enlink($str)] = $val;
            }
            else {
                $_SESSION[$str] = $val;
            }
        }
        catch(Exception $e) {
            //return "Error Function setOneSession() : ".$e;
            return "";
        }
    }
    
    public function setSession(Array $array, int $type=0) {
        try {
            if( $type > 1 ) {
                foreach($array as $arr => $val) {
                    $_SESSION[$this->enlink($arr)] = $this->enval($val);
                }
            }
            else if( $type == 1 ) {
                foreach($array as $arr => $val) {
                    $_SESSION[$this->enlink($arr)] = $val;
                }
            }
            else {
                foreach($array as $arr => $val) {
                    $_SESSION[$arr] = $val;
                }
            }
        }
        catch(Exception $e) {
            //return "Error Function setSession() : ".$e;
            return "";
        }
    }

    public function getSession(String|float $str=null, int $type=0) {
        try {
            return match($type) {
                1 => $_SESSION[$this->enlink($str)],
                2 => $this->denval($_SESSION[$this->enlink($str)]),
                default => $_SESSION[$str],
            };
        }
        catch(Exception $e) {
            //return "Error Function getSession() : ".$e;
            return "";
        }
    }

    public function setOneCookie(String $name, String|float $val=1, int $type=0, int $hari=1, int $jam=24, int $menit=60, int $detik=60) {
        try {
            if($type > 1) {
                setcookie($this->enlink($name), $this->enval($val), time() + ($hari * $jam * $menit * $detik), "/");
            }
            else if($type == 1) {
                setcookie($this->enlink($name), $val, time() + ($hari * $jam * 60 * 60), "/");
            }
            else {
                setcookie($name, $val, time() + ($hari * $jam * 60 * 60), "/");
            }
        }
        catch(Exception $e) {
            //return "Error Function setOneCookie() : ".$e;
            return "";
        }
    }

    public function setCookie($array, $hari=1, $jam=24, $menit=60, $detik=60) {
        try {
            foreach($array as $arr => $val) {
                setcookie($this->enlink($arr), $this->enval($val), time() + ($hari * $jam * $menit * $detik), "/"); // 86400 = 1 day
            }
        }
        catch(Exception $e) {
            //return "Error Function setCookie() : ".$e;
            return "";
        }
    }

    public function getCookie(String|float $str, $type = 0) {
        try {
            return match($type) {
                1 => @$_COOKIE[$this->enlink($str)],
                2 => $this->denval(@$_COOKIE[$this->enlink($str)]),
                default => @$_COOKIE[$str],
            };
        }
        catch(Exception $e) {
            //return "Error Function getCookie() : ".$e;
            return "";
        }
    }

    public function setCookieOff(String $str, String|float $val = null, $type = 1) {
        try {
            if($type > 1) {
                setcookie($this->enlink($str), $val, time() - (365 * 24 * 60 * 60), "/");
            }
            else {
                setcookie($str, $val, time() - (365 * 24 * 60 * 60), "/");
            }
        }
        catch(Exception $e) {
            //return "Error Function getValookie() : ".$e;
            return "";
        }
    }


    // SYSTEM LOGOUT
    public function logoutSystem($array) {
        try {
            foreach($array as $arr) {
                setcookie($this->enlink($arr), null, time() - (365 * 24 * 60 * 60), "/");
            }
        }
        catch(Exception $e) {
            //return "Error Function logoutSystem() : ".$e;
            return "";
        }
    }

    // FORM FILL
    public function inputField(String $type, String|float $id, String $name, String $text, String $field_class, String $text_class, String|float $val=null, String $required=null, String $readonly=null) {
        try { ?>
            <label for="<?php echo $this->enlink($id); ?>" class="<?php echo $text_class; ?>"><?php echo $text; ?></label>
            <input type="<?php echo $type; ?>" name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" class="<?php echo $field_class; ?>" value="<?php echo $val; ?>" <?php echo $required.' '.$readonly; ?> >
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function inputField() Error: ".$e;
            return "";
        }
    }
    
    public function textField(String $name, String|float $id, String $text, String $field_class, String $text_class=null, String|float $val=null, String|float $col=null, String|float $row=null, String $required=null, String $readonly=null) {
        try { ?>
            <label for="<?php echo $this->enlink($id); ?>"><?php echo $text; ?></label>
            <textarea name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" class="<?php echo $field_class; ?>" rows="<?php echo $row; ?>" cols="<?php echo $col; ?>" placeholder="<?php echo $text; ?>" <?php echo $required.' '.$readonly; ?>>
                <?php 
                if( $val != '' || !empty($val) || !is_null($val) ) {
                    echo $val; 
                }
                ?>
            </textarea>
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function textField() Error: ".$e;
            return "";
        }
    }
    
    public function selectField(String $name, String|float $id, String $text, String $class_select, String $class_text, Array $arrname, Array $arrval, String|float $setval, String $required=null, String $readonly=null) {
        try { ?>
            <span class="<?php echo $class_text; ?>"><?php echo $text; ?></span><br>
            <select name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" class="<?php echo $class_select; ?>" <?php echo $required.' '.$readonly; ?>>
                <?php
                $x = 0;
                foreach($arrname as $val) { ?>
                    <option value="<?php echo $this->enval($val); ?>" <?php if($val == $setval) { echo 'selected'; } ?> ><?php echo $arrval[$x]; ?></option>
                    <?php
                    $x++;
                }
                ?>
            </select>
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function selectField() Error: ".$e;
            return "";
        }
    }
    
    public function dtField(String $type, String $name, String|float $id, String $text, String $class_input, String $class_text, String|float $val=null, String $required=null, String $readonly=null) {
        try { ?>
            <span class="<?php echo $class_text; ?>"><?php echo $text; ?></span>
            <input type="<?php echo $type; ?>" name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" class="<?php echo $class_input; ?>" value="<?php echo $val; ?>" <?php echo $required.' '.$readonly; ?>>
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function radioField() Error: ".$e;
            return "";
        }
    }
    
    public function radioField(String $name, String|float $id, String|float $text, String $class_text, String|float $val1=null, String|float $val2=null, String $required=null, String $readonly=null) {
        try { ?>
            <input type="radio" name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" value="<?php echo $this->enval($val1); ?>" <?php if( !empty($val1) || !is_null($val1) || $val1 != '' || !empty($val2) || !is_null($val2) || $val2 != '' ) { if( $val1 == $val2 ) { echo 'checked'; } } ?> <?php echo $required.' '.$readonly; ?> /> 
            <label for="<?php echo $this->enlink($id); ?>" class="<?php echo $class_text; ?>"><?php echo $text; ?></label>
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function radioField() Error: ".$e;
            return "";
        }
    }
    
    public function checkField(String $name, String|float $id, String $text, String $class_text, String|float $val1=null, String|float $val2=null, String $required=null, String $readonly=null) {
        try { ?>
            <input type="checkbox" name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" value="<?php echo $this->enval($val1); ?>" <?php if( !empty($val1) || !is_null($val1) || $val1 != '' || !empty($val2) || !is_null($val2) || $val2 != '' ) { if( $val1 == $val2 ) { echo 'checked'; } } ?> <?php echo $required.' '.$readonly; ?> /> 
            <label for="<?php echo $this->enlink($id); ?>" class="<?php echo $class_text; ?>"><?php echo $text; ?></label>
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function checkField() Error: ".$e;
            return "";
        }
    }
    
    public function fileField(String $name, String|float $id, String $text, String $field_class, String $text_class) {
        try { ?>
            <span class="<?php echo $text_class; ?>"><?php echo $text; ?></span><br>
            <input type="file" name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" class="<?php echo $field_class; ?>" />
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function fileField() Error: ".$e;
            return "";
        }
    }
    
    public function buttonField(String $type, String $name, String|float $id, String $class, String $text) {
        try { ?>
            <button type="<?php echo $type; ?>" name="<?php echo $this->enlink($name); ?>" id="<?php echo $this->enlink($id); ?>" class="<?php echo $class; ?>"><?php echo $text; ?></button>
            <?php
            return null;
        }
        catch(Exception $e) {
            //return "function fileField() Error: ".$e;
            return "";
        }
    }
    
    // OTHER
    public function randNumber(int $length, int $withzero=0) {
        try {
            if($withzero == 0) $seed = str_split('0123456789'); // and any other characters
            else $seed = str_split('123456789'); // and any other characters
            
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            //echo "function randNumber() Error: ".$e;
            return "";
        }
    }


    public function genPass(int $length) {
        try {
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'.
                              'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
                              '0123456789'); // and any other characters
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            //echo "function genPass() Error: ".$e;
            return "";
        }
    }


    public function randChar(int $length) {
        try {
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'.
                              'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
                              '0123456789`~!@#$%^&*()-_=+[{]}\|;:,<.>/?/'); // and any other characters
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            //echo "function randChar() Error: ".$e;
            return "";
        }
    }


    public function randHexColor() {
        try {
            $seed = str_split('0123456789abcdef'); // and any other characters
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, 6) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            //echo "function randHexColor() Error: ".$e;
            return "";
        }
    }
}
?>