<?php
class connection extends connectionAbstraction {

    public function getConnection() {
        try {
            return $this->koneksi;
        }
        catch(Exception $e) {
            return "function getConnection() Error: ".$e;
        }
    }

    // ENCRYPT & DECRYPT
    public function encrypt_plain(String $plaintext=null, String $key=null){
        try {
            $k = $this->cryptkey($key);

            $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
            $hmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
            $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    
            return $ciphertext;
        }
        catch(Exception $e) {
            //return "function encrypt_plain() Error: ".$e;
            return "";
        }
    }
    
    public function decrypt_plain(String $code=null, String $key=null){
        try {
            $k = $this->cryptkey($key);

            $c = base64_decode($code);
            $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
            if (hash_equals($hmac, $calcmac)) {
                return $original_plaintext;
            }
        }
        catch(Exception $e) {
            //return "function decrypt_plain() Error: ".$e;
            return "";
        }
    }

    public function encrypt(String|float $txt=null) {
        try {
            $str = $this->encrypt_plain($txt, $this->getK());
            return $str;
        }
        catch(Exception $e) {
            //return "function encrypt() Error: ".$e;
            return "";
        }
    }

    public function decrypt(String|float $txt=null) {
        try {
            $str = $this->decrypt_plain($txt, $this->getK());
            return $str;
        }
        catch(Exception $e) {
            //return "function decrypt() Error: ".$e;
            return "";
        }
    }

    public function safe(String|float $str=null) {
        try {
            //return mysqli_real_escape_string($this->getConnection(), $this->escape($str));
            return $this->getConnection()->real_escape_string($str);
        }
        catch(Exception $e) {
            //return "Error Function safe() : ".$e;
            return "";
        }
    }
}
?>