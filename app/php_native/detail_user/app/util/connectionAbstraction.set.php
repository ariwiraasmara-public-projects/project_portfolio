<?php
abstract class connectionAbstraction {

    protected $koneksi;
    public function __construct() {
        $this->setConnection();
    }

    protected function setConnection(){
        $this->koneksi = mysqli_connect($this->decrypt($this->getS()),
                                        $this->decrypt($this->getU()),
                                        $this->decrypt($this->getP()),
                                        $this->decrypt($this->getD()));
    }

    protected function getS() {
        try {
            // LOCALHOST
            return "EEkmMC6vMyHUyghVC4sodepQJWE3nROdKrQNxjlrejcaMf+71ZbgcNUVPfYKYw/xRYRKRO1GAUTBpYUNacnaAg==";
        }
        catch(Exception $e) {
            return "function getS() Error: ".$e;
        }
    }
    
    protected function getU() {
        try {
            // LOCALHOST
            return "cJ7FNQ3HvtBHWvKFoi8Gr0cYULFnZEDBiOjTCxhlbmZXyQZAlKFlnKkvbYw7uAjaevXsIim8V9iOMXpqBTQNyw==";
        }
        catch(Exception $e) {
            return "function getU() Error: ".$e;
        }
    }
    
    protected function getP() {
        try {
            // LOCALHOST
            return "9K6Aw/RT/au8ZGoC+Z4aNmQhfjFkzPnbbjqnY2USZMe1qQNIpfx/Pz1UuYjcBnvFMbd6oaWwNsiX8izyGLBQng==";
        }
        catch(Exception $e) {
            return "function getP() Error: ".$e;
        }
    }
    
    protected function getD() {
        try {
            // LOCALHOST
            return "FZ9WAz+7bB5fnD4vKeIenlFAXd+hHXfsujYxotRLAQL3MNx4gb2wL8OxHl8VtjCl5BPLM8hbJrEPvUSRvd9HANvgO893Imj30Q22b+dIqsc=";
        }
        catch(Exception $e) {
            return "function getD() Error: ".$e;
        }
    }
    
    protected function getK() {
        try {
            return "4MciAafPx9zMfIkzHNgslQOeCiKfp0fOtBDuNzmFy3enGWro0SjL+IhWlP8AmA17rhVmlMzfCDEF5KO3ALipBQ==";
        }
        catch(Exception $e) {
            return "function getK() Error: ".$e;
        }
    }

    protected function cryptkey(String $code=null){
        try {
            $c = base64_decode($code);
            $ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            //$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, 'c8f6dae75c5e2010d46924649e0c0357', $options=OPENSSL_RAW_DATA, $iv);
            //$calcmac = hash_hmac('sha256', $ciphertext_raw, 'c8f6dae75c5e2010d46924649e0c0357', $as_binary=true);
            //$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, '{[/=<<@-19O6I1A14A20A19I1DES1I14T53O17U9L125-@>>=\]}', $options=OPENSSL_RAW_DATA, $iv);
            //$calcmac = hash_hmac('sha256', $ciphertext_raw, '{[/=<<@-19O6I1A14A20A19I1DES1I14T53O17U9L125-@>>=\]}', $as_binary=true);
            $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, crypt(sha1(md5('rL+J1dguWCXBiKOt32uUAqTc0/CVZpJwPsA9ahOP6UbQSBAkcXGrpLUi96W15fmmAKBTPbjnc83h83qLJ+rfeRyn0NLt4q/CCB/7I6RVsmk=', TRUE),TRUE),'$6$rounds=999999999'), $options=OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, crypt(sha1(md5('rL+J1dguWCXBiKOt32uUAqTc0/CVZpJwPsA9ahOP6UbQSBAkcXGrpLUi96W15fmmAKBTPbjnc83h83qLJ+rfeRyn0NLt4q/CCB/7I6RVsmk=', TRUE),TRUE),'$6$rounds=999999999'), $as_binary=true);
            if (hash_equals($hmac, $calcmac)) {
                return $original_plaintext;
            }
        }
        catch(Exception $e) {
            //return "function ks() Error: ".$e;
            return "";
        }
    }

    abstract public function getConnection();
    abstract public function encrypt_plain();
    abstract public function decrypt_plain();
    abstract public function encrypt();
    abstract public function decrypt();
}
?>