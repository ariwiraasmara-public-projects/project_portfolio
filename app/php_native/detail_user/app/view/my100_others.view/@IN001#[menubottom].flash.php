<div class="panel menubottom">
    <div class="row">
        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'profil'); ?>"><i class="ion-android-person"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()); ?>"><i class="ion-android-home"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'logout'); ?>"><i class="ion-log-out"></i></></a>
        </div>
    </div>
</div>