<div class="container m-t-10p borad-20">
    <div class="m-t-30 m-b-10 login">
        <h1 class="bold center">Login</h1>

        <div class="m-t-50">
            <form action="<?php echo $path101->proc("login", $lib->setIDParam('token', $lib->tokenValue())); ?>" method="post">
                <input type="hidden" name="<?php echo $lib->tokenName("@Syslog:token"); ?>" id="<?php echo $lib->tokenName("@Syslog:token"); ?>"   value="<?php echo $lib->tokenValue(); ?>" />
                <input type="hidden" name="<?php echo $lib->tokenName("@Process:"); ?>"     id="<?php echo $lib->tokenName("@Process:"); ?>"       value="<?php echo $lib->tokenValue(); ?>" />
                <input type="hidden" name="<?php echo $lib->enlink("procont1"); ?>"         id="<?php echo $lib->enlink("procont1"); ?>"           value="<?php echo $lib->enval('as101'); ?>" />
                
                <div class="form-group">
                    <input type="text" name="<?php echo $lib->enlink('useremail'); ?>" id="<?php echo $lib->enlink('useremail'); ?>" class="form-control" placeholder="Username / Email.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="<?php echo $lib->enlink('password'); ?>" id="<?php echo $lib->enlink('password'); ?>" class="form-control" placeholder="Password.." required />
                </div>

                <div class="center">
                    <button type="submit" class="btn btn-primary center">Masuk</button>
                </div>

                <?php
                if( $lib->getCookie('error', 1) ) { ?>
                    <div class="center m-t-50 m-b-10">
                        <h4 class="bold"><?php echo $lib->getCookie('error', 2); ?></h4>
                    </div>
                    <?php
                }
                ?>
                
                <div class="down-10 m-r-10">
                    <div class="right">
                        <a href="<?php echo '?'.$lib->setIDParam('pg', 'register'); ?>" class="bold">Daftar</a>
                        <?php 
                        /*
                        <br/>
                        <a href="<?php echo '?'.$lib->setIDParam('pg', 'lupapassword'); ?>" class="">Lupa Password</a>
                        */
                        ?>
                    </div>
                </div>
            </form>
        </div>
        
    </div>
</div>

<div class="container bottom">
    <div class="center copyright bold">
        Copyright © Syahri Ramadhan Wiraasmara
    </div>  
</div>