<div class="container m-t-10p borad-20">
    <div class="m-t-30 m-b-10">
        <h1 class="bold center">Daftar Baru</h1>

        <div class="m-t-50">
            <form action="<?php echo $path101->proc("register", $lib->setIDParam('token', $lib->tokenValue())); ?>" method="post">
                <input type="hidden" name="<?php echo $lib->tokenName("@Syslog:token"); ?>"  id="<?php echo $lib->tokenName("@Syslog:token"); ?>"   value="<?php echo $lib->tokenValue(); ?>" />
                <input type="hidden" name="<?php echo $lib->tokenName("@Process:"); ?>" id="<?php echo $lib->tokenName("@Process:"); ?>"       value="<?php echo $lib->tokenValue(); ?>" />
                <input type="hidden" name="<?php echo $lib->enlink("procont1"); ?>"          id="<?php echo $lib->enlink("procont1"); ?>"           value="<?php echo $lib->enval('as101'); ?>" />
                <input type="hidden" name="<?php echo $lib->enlink("procont2"); ?>"          id="<?php echo $lib->enlink("procont2"); ?>"           value="<?php echo $lib->enval('as102'); ?>" />
                
                <div class="form-group">
                    <input type="text" name="<?php echo $lib->enlink('username'); ?>" id="<?php echo $lib->enlink('username'); ?>" class="form-control" placeholder="Username.." required />
                </div>

                <div class="form-group">
                    <input type="text" name="<?php echo $lib->enlink('email'); ?>" id="<?php echo $lib->enlink('email'); ?>" class="form-control" placeholder="Email.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="<?php echo $lib->enlink('password'); ?>" id="<?php echo $lib->enlink('password'); ?>" class="form-control" placeholder="Password.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="<?php echo $lib->enlink('passver'); ?>" id="<?php echo $lib->enlink('passver'); ?>" class="form-control" placeholder="Verifikasi Password.." required />
                </div>

                <div class="center">
                    <a href="?" class="btn btn-primary m-r-10">Batal</a>
                
                    <button type="submit" id="<?php echo $lib->enlink('daftar'); ?>" class="btn btn-primary m-l-10">Daftar</button>
                </div>
                
            </form>
        </div>
        
    </div>
</div>

<script>
    $("<?php echo '#'.$lib->enlink('passver'); ?>").keyup(function(){

        var pass1 = $("<?php echo '#'.$lib->enlink('password'); ?>").val();
        var pass2 = $("<?php echo '#'.$lib->enlink('passver'); ?>").val();

        if(pass1 == pass2) {
            $("<?php echo '#'.$lib->enlink('daftar'); ?>").attr("disabled", false);
        }
        else {
            $("<?php echo '#'.$lib->enlink('daftar'); ?>").attr("disabled", true);
        }

    });

    $("<?php echo '#'.$lib->enlink('passver'); ?>").keydown(function(){

        var pass1 = $("<?php echo '#'.$lib->enlink('password'); ?>").val();
        var pass2 = $("<?php echo '#'.$lib->enlink('passver'); ?>").val();

        if(pass1 == pass2) {
            $("<?php echo '#'.$lib->enlink('daftar'); ?>").attr("disabled", false);
        }
        else {
            $("<?php echo '#'.$lib->enlink('daftar'); ?>").attr("disabled", true);
        }

    });
</script>