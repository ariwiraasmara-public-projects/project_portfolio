<div class="panel m-t-10">
    <h2 class="bold center">Profil<br/><?php echo $lib->getCookie('user_name', 2); ?></h2>

    <div class="m-t-30">
        <p>
            <?php
            $data = $cont->getData("a.id_101 as id_101,
                                    b.id_102 as id_102,
                                    b.nama as nama,
                                    b.email as email,
                                    a.password as pass,
                                    b.tgl_lahir as tgl_lahir,
                                    b.alamat as alamat,
                                    b.tlp as tlp,
                                    b.status as status",

                                    "inner join my102_biodata as b
                                    on a.email = b.email
                                    where a.email='".$lib->getCookie('user_email', 2)."'
                                ");
            
            ?>
            <span class="bold">Nama :</span>            <span clas="italic"><?php echo $lib->readable($data[0]['nama']); ?></span><br/>
            <span class="bold">Email :</span>           <span clas="italic"><?php echo $lib->readable($data[0]['email']); ?></span><br/>
            <span class="bold">Password :</span>        <span clas="italic"><?php echo $lib->readable($data[0]['pass']); ?></span><br/>
            <span class="bold">Tanggal Lahir :</span>   <span clas="italic"><?php echo $lib->readable($data[0]['tgl_lahir']); ?></span><br/>
            <span class="bold">Alamat :</span>          <span clas="italic"><?php echo $lib->readable($data[0]['alamat']); ?></span><br/>
            <span class="bold">No. Telepon :</span>     <span clas="italic"><?php echo $lib->readable($data[0]['tlp']); ?></span><br/>
            <span class="bold">Status :</span>          <span clas="italic"><?php echo $lib->readable($data[0]['status']); ?></span><br/>
        </p>
    </div>

    <div class="m-t-30 center">
        <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'editbiodata').'&'.$lib->setIDParam('nebiodata', 'update').'&'.$lib->setIDParam('bid102', $lib->escape($data[0]['id_102'])); ?>" class="btn btn-primary">Edit Biodata</a><br/>
    </div>

    <div class="m-t-10 center">
        <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'updatepass').'&'.$lib->setIDParam('pid', $lib->escape($data[0]['id_101'])); ?>" class="btn btn-primary">Ganti Password</a>
    </div>
</div>

<div class="panel menubottom">
    <div class="row">
        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'userprofil'); ?>"><i class="ion-android-person"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()); ?>"><i class="ion-android-home"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'logout'); ?>"><i class="ion-log-out"></i></></a>
        </div>

        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-12 center">
            <span class="bold center">Copyright © Syahri Ramadhan Wiraasmara</span>
        </div>
    </div>
</div>