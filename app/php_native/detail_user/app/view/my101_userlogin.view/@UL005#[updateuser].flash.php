<div class="container m-t-10p borad-20">
    <div class="m-t-30 m-b-10">
        <h1 class="bold center">Ganti Password</h1>

        <div class="m-t-50">
            <form action="<?php echo $path101->proc("updateprofil", $lib->setIDParam('token', $lib->tokenValue())); ?>" method="post">
                <input type="hidden" name="<?php echo $lib->tokenName("@Syslog:token"); ?>" id="<?php echo $lib->tokenName("@Syslog:token"); ?>"   value="<?php echo $lib->tokenValue(); ?>" />
                <input type="hidden" name="<?php echo $lib->tokenName("@Process:"); ?>"     id="<?php echo $lib->tokenName("@Process:"); ?>"       value="<?php echo $lib->tokenValue(); ?>" />
                <input type="hidden" name="<?php echo $lib->enlink("procont1"); ?>"         id="<?php echo $lib->enlink("procont1"); ?>"           value="<?php echo $lib->enval('as101'); ?>" />
                
                <div class="form-group">
                    <input type="password" name="<?php echo $lib->enlink('old_pass'); ?>" id="<?php echo $lib->enlink('old_pass'); ?>" class="form-control" placeholder="Password Lama.." required />
                </div>

                <div class="form-group">
                    <input type="password" name="<?php echo $lib->enlink('newpassword'); ?>" id="<?php echo $lib->enlink('newpassword'); ?>" class="form-control" placeholder="Password Baru.." required />
                </div>

                <div class="center">
                    <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'userprofil') ?>" class="btn btn-primary m-r-10">Batal</a>
                
                    <button type="submit" class="btn btn-primary m-l-10">OK</button>
                </div>
                
            </form>
        </div>
        
    </div>
</div>