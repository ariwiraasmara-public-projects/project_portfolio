<div class="panel m-t-10">
    <h2 class="bold center">SELAMAT DATANG<br/><?php echo $lib->getCookie('user_name', 2); ?></h2>

    <div class="m-t-30 m-b-30p">
        <table class="table table-responsive width-100">
            <thead>
                <tr>
                    <th class="center">#</th>
                    <th class="center">Nama</th>
                    <th class="center">Email</th>
                    <th class="center">Tanggal Lahir</th>
                    <th class="center">Alamat</th>
                    <th class="center">Nomor Telepon</th>
                    <th class="center">Status</th>
                    <th class="center" colspan="2">--</th>
                </tr>
            </thead>

            <tbody>
                <?php
                $nomor = 1;
                $row = $cont->getData("*", "where email != '".$lib->getCookie('user_email', 2)."' order by nama ASC", 2);
                foreach($row as $data) { 
                    $cek = $cont2->checkRow("*", "where email='".$data['email']."'");
                    $cont->mSet($data['id_102'], 
                                $data['nama'], 
                                $data['email'], 
                                $data['tgl_lahir'],
                                $data['alamat'], 
                                $data['tlp'], 
                                $data['status']);
                    ?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $cont->mGet('nama'); ?></td>
                        <td><?php echo $cont->mGet('email'); ?></td>
                        <td><?php echo $cont->mGet('tgl'); ?></td>
                        <td><?php echo $cont->mGet('alamat'); ?></td>
                        <td><?php echo $cont->mGet('tlp'); ?></td>
                        <td><?php echo $cont->mGet('status'); ?></td>
                        <?php
                        if($cek == 0) { ?>
                            <td class="center"><a href="<?php echo '?'.$lib->setIDParam('pg', 'editbiodata').'&'.$lib->setIDParam('nebiodata', 'update').'&'.$lib->setIDParam('bid102', $cont->mGet('id')); ?>">Edit</a></td>
                            <td class="center"><a href="<?php echo '#'.$nomor; ?>" id="<?php echo 'del'.$nomor; ?>" value="<?php echo $cont->mGet('id'); ?>">Delete</a></td>
                            <?php
                        }
                        else { 
                        }
                        ?>
                    </tr>
                    <?php
                    $nomor++;
                }
                ?>
            </tbody>
        </table>

        <div>
            
        </div>
    </div>
</div>



<div class="panel menubottom">
    <div class="row">
        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-12 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'newbiodata').'&'.$lib->setIDParam('nebiodata', 'insert'); ?>" class=""><i class="ion-android-add"></i></a>
        </div>
        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('pg', 'userprofil'); ?>"><i class="ion-android-person"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()); ?>"><i class="ion-android-home"></i></></a>
        </div>

        <div class="col-4 col-xs-4 col-md-4 col-lg-4 col-xl-4 center">
            <a href="<?php echo 'logout.php?'.$lib->setIDParam('token', $lib->tokenValue()); ?>"><i class="ion-log-out"></i></></a>
        </div>

        <div class="col-12 col-xs-12 col-md-12 col-lg-12 col-xl-12 center">
            <span class="bold center">Copyright © Syahri Ramadhan Wiraasmara</span>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    <?php
    $nomor = 1; 
    foreach($row as $data) { ?>
        $("<?php echo '#del'.$nomor; ?>").click(function(){
            //alert("<?php echo $cont->mGet('id'); ?>");
            var decision = confirm("Anda yakin ingin menghapus data ini?");
            if(decision == true) {
                var data = "<?php echo $lib->enval($cont->mGet('id')); ?>";
                //console.log(data);
                $.ajax({
                    url : "ajax/@[deletebiodata].axe.php",
                    type : 'POST',
                    data: {  
                        data1: "<?php echo $lib->tokenValue(); ?>",
                        data2: data
                    },
                    success : function(data) {
                        //console.log(data);
                        alert("Data Berhasil Dihapus!");
                        //if(data == 1) { alert("Data Berhasil Dihapus!"); }
                        //else { alert("Data Gagal Dihapus!"); }
                        window.location.href = "<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()); ?>";
                    },
                    error : function(){
                        alert('Terjadi Kegagalan! Tidak Bisa Menghapus Data!');
                        return false;
                    }
                });
            }
        });
        <?php 
        $nomor++;
    } ?>
});
</script>