<?php
if($lib->GET('nebiodata', 2) == 'update') {
    $bid102     = $lib->GET('bid102', 2);
    $netitle    = "Edit Biodata";
    $data       = $cont->getData("*", "where id_102='".$lib->GET('bid102', 2)."'");

    $nama       = $data[0]['nama'];
    $email      = $data[0]['email'];
    $tgl        = $data[0]['tgl_lahir'];
    $alamat     = $data[0]['alamat'];
    $tlp        = $data[0]['tlp'];
    $status     = $data[0]['status'];

    $readonly   = "readonly";
}
else {
    $bid102      = "null";
    $netitle    = "Tambah Biodata";

    $nama       = null;
    $email      = null;
    $tgl        = date('Y-m-d');
    $alamat     = null;
    $tlp        = null;
    $status     = null;

    $readonly   = "";
}

$proc = $lib->GET('nebiodata', 2);
?>
<div class="panel">
    <h2 class="bold center"><?php echo $netitle; ?></h2>

    <div class="m-t-30">
        <form action="<?php echo $path102->proc($proc, $lib->setIDParam('token', $lib->tokenValue()).'&'.$lib->setIDParam('bid102', $bid102)); ?>" method="post">
            <input type="hidden" name="<?php echo $lib->tokenName("@Syslog:token"); ?>" id="<?php echo $lib->tokenName("@Syslog:token"); ?>"   value="<?php echo $lib->tokenValue(); ?>" />
            <input type="hidden" name="<?php echo $lib->tokenName("@Process:"); ?>"     id="<?php echo $lib->tokenName("@Process:"); ?>"       value="<?php echo $lib->tokenValue(); ?>" />
            <input type="hidden" name="<?php echo $lib->enlink("procont1"); ?>"         id="<?php echo $lib->enlink("procont1"); ?>"           value="<?php echo $lib->enval('as102'); ?>" />
        
            <div class="form-group">
                <input type="text" name="<?php echo $lib->enlink('nama'); ?>" id="<?php echo $lib->enlink('nama'); ?>" class="form-control" placeholder="Nama.." value="<?php echo $nama; ?>" required />
            </div>

            <div class="form-group">
                <input type="email" name="<?php echo $lib->enlink('email'); ?>" id="<?php echo $lib->enlink('email'); ?>" class="form-control" placeholder="Email.." value="<?php echo $email; ?>" required <?php echo $readonly; ?> />
            </div>

            <div class="form-group">
                <input type="date" name="<?php echo $lib->enlink('tgl_lahir'); ?>" id="<?php echo $lib->enlink('tgl_lahir'); ?>" class="form-control" placeholder="Tanggal Lahir.." value="<?php echo $tgl; ?>" />
            </div>

            <div class="form-group">
                <input type="text" name="<?php echo $lib->enlink('alamat'); ?>" id="<?php echo $lib->enlink('alamat'); ?>" class="form-control" placeholder="Alamat.." value="<?php echo $alamat; ?>" />
            </div>

            <div class="form-group">
                <input type="tel" name="<?php echo $lib->enlink('tlp'); ?>" id="<?php echo $lib->enlink('tlp'); ?>" class="form-control" placeholder="Nomor Telepon.." value="<?php echo $tlp; ?>" />
            </div>

            <div class="form-group">
                <input type="text" name="<?php echo $lib->enlink('status'); ?>" id="<?php echo $lib->enlink('status'); ?>" class="form-control" placeholder="Status.." value="<?php echo $status; ?>" />
            </div>

            <div class="center">
                <button type="submit" class="btn btn-primary center m-r-10">OK</button>
                <a href="<?php echo '?'.$lib->setIDParam('token', $lib->tokenValue()); ?>" class="btn btn-primary center m-l-10">Batal</a>
            </div>
        </form>
    </div>
</div>