from django.apps import AppConfig


class DetailUserConfig(AppConfig):
    name = 'detail_user'
