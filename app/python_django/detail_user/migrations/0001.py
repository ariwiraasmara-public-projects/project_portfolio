from django.db import migrations, models
class Migration(migrations.Migration):
    initial = True
    dependencies = [
    ]
    operations = [
        migrations.CreateModel(
            name='101_UserLogin',
            fields=[
                ('id_101', models.CharField(max_length=255, primary_key=True)),
                ('username', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=255)),
                ('password', models.TextField(max_length=255)),
            ],
            options={
                'db_table': 'my101_user_login',
            },
        ),
    ]
