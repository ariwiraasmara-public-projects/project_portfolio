from django.db import migrations, models
class Migration(migrations.Migration):
    initial = True
    dependencies = [
    ]
    operations = [
        migrations.CreateModel(
            name='102_Biodata',
            fields=[
                ('id_102', models.CharField(max_length=255, primary_key=True)),
                ('nama', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=255)),
                ('tgl_lahir', models.DateField(max_length=255)),
                ('alamat', models.TextField()),
                ('tlp', models.TextField()),
                ('status', models.TextField()),
            ],
            options={
                'db_table': 'my102_biodata',
            },
        ),
    ]
