from django.db import models

# Create your models here.
class my101_user_login(models.Model):
    id_101 = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    password = models.TextField()
    class Meta:
        db_table = "my101_user_login"

class my102_biodata(models.Model):
    id_102 = models.CharField(max_length=255)
    nama = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    tgl_lahir = models.DateField()
    alamat = models.TextField()
    tlp = models.TextField()
    status = models.TextField()
    class Meta:
        db_table = "my102_biodata"

class my103_tabungan(models.Model):
    id_102 = models.CharField(max_length=255)
    id_103 = models.CharField(max_length=255)
    tgl = models.IntegerField(max_length=3)
    bulan = models.CharField(max_length=50)
    tahun = models.IntegerField(max_length=5)
    debet = models.BigIntegerField(max_length=255)
    kredit = models.BigIntegerField(max_length=255)
    byid = models.CharField(max_length=255)
    ket = models.TextField()
    class Meta:
        db_table = "my103_tabungan"

