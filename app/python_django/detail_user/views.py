from django.shortcuts import render

# Create your views here.
from django.template import loader
from django.http import HttpResponse

def hello(request):
    return HttpResponse("<h2>Project Detail User Akan Segera Dibuat!</h2>")

def login(request):
    template = loader.get_template('01_before_login/login.html') # getting our template
    return HttpResponse(template.render())       # rendering the template in HttpResponse
    # #return HttpResponse("login")

def loginprocess(request):
    if request.method == "POST":
        return request.POST.get('usermail') + " : " + request.POST.get('password')
