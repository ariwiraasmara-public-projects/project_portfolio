from django.shortcuts import render

# Create your views here.
from django.template import loader
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie

def home(request):
    return render(request, '02_after_login/my102/biodata.html', {'nama':"User"})
    #return "home"

def addbiodata(request):
    return render(request, '02_after_login/my102/nebiodata.html', {'ne':"new"})
    #return "addbiodata"

def editbiodata(request):
    return render(request, '02_after_login/my102/nebiodata.html', {'ne':"edit"})
    #return "editbiodata"

def processinsertbiodata(request):
    return "processinsertbiodata"

def processupdatebiodata(request):
    return "processupdatebiodata"

def processdeletebiodata(request):
    return "processdeletebiodata"