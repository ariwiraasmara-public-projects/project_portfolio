from django.shortcuts import render

# Create your views here.
from django.template import loader
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from detail_user.forms import RegisterForm  
from detail_user.models import my101_user_login  

def login(request):
    #template = loader.get_template('01_before_login/login.html') # getting our template
    #return HttpResponse(template.render())       # rendering the template in HttpResponse
    # #return HttpResponse("login")
    return render(request, '01_before_login/login.html')

def processlogin(request):
    if request.method == "POST":
        usermail = request.POST.get('usermail', 'none@none.none')
        password = request.POST.get('password', '__none__')
        str = usermail + " : " + password
        return HttpResponse(str)

def register(request):
    #template = loader.get_template('01_before_login/login.html') # getting our template
    #return HttpResponse(template.render())       # rendering the template in HttpResponse
    # #return HttpResponse("login")
    form = RegisterForm()  
    return render(request, '01_before_login/register.html', {'form':form})

def processregister(request):
    #return ""
    
    if request.method == "POST":  
        #form = RegisterForm(request.POST)  
        form = RegisterForm(request.POST)
        #form.save()  
        #return "Berhasil Simpan Data"
        if form.is_valid():  
            try:  
                form.save()  
                return "Berhasil Simpan"
                #return redirect('detail_user/login/')  
            except:  
                pass
                return HttpResponse("Gagal Simpan 2")
    #return HttpResponse()
    #return render(request,'01_before_login/register.html')  

def userprofil(request):
    return render(request, '02_after_login/userprofil.html')

def updatepassword(request):
    return render(request, '02_after_login/updateuser.html')

def processupdatepassword(request):
    return ""

def processlogout(request):
    return ""