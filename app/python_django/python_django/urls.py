"""python_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from detail_user import views as detus
from detail_user import views_user_login as userlogin
from detail_user import views_biodata as biodata

urlpatterns = [
    path('admin/', admin.site.urls),
    path('detail_user/hello/', detus.hello),
    path('detail_user/login/', userlogin.login),
    path('detail_user/login/process/', userlogin.processlogin),
    path('detail_user/register/', userlogin.register),
    path('detail_user/register/process/', userlogin.processregister),
    path('detail_user/profil/', userlogin.userprofil),
    path('detail_user/profil/pass/', userlogin.updatepassword),
    path('detail_user/profil/pass/update/', userlogin.processupdatepassword),
    path('detail_user/home/', biodata.home),
    path('detail_user/biodata/add/', biodata.addbiodata),
    path('detail_user/biodata/edit/', biodata.editbiodata),
    path('detail_user/biodata/add/process', biodata.processinsertbiodata),
    path('detail_user/biodata/edit/process', biodata.processupdatebiodata),
    path('detail_user/biodata/delete/process', biodata.processdeletebiodata),
]
