-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Jan 2022 pada 07.39
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_contoh_myportfolio`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `my101_user_login`
--

CREATE TABLE `my101_user_login` (
  `id_101` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `my101_user_login`
--

INSERT INTO `my101_user_login` (`id_101`, `username`, `email`, `password`) VALUES
('UL#001', 'ariwiraasmara', 'ariwiraasmara.sc37@gmail.com', 'asd'),
('UL#002', 'sofiaria', 'sofiaria@gmail.com', 'asd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `my102_biodata`
--

CREATE TABLE `my102_biodata` (
  `id_102` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `tlp` text DEFAULT NULL,
  `status` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `my102_biodata`
--

INSERT INTO `my102_biodata` (`id_102`, `nama`, `email`, `tgl_lahir`, `alamat`, `tlp`, `status`) VALUES
('BI#001', 'Syahri Ramadhan Wiraasmara', 'ariwiraasmara.sc37@gmail.com', '1995-11-03', 'Komp. Depag Blok I No. 1 Cipocok Jaya, Serang, Banten, 42121', '08176896353', 'Belum Menikah'),
('BI#002', 'Sofia Anastasia', 'sofiaria@gmail.com', '1995-11-07', 'Rune Factory 3', '08176896353', 'Belum Menikah'),
('BI#003', 'Sofia aa', 'aa', '1995-11-01', '', '', ''),
('BI#004', 'aasd', 'aaasd', '2015-06-05', 'aasd', '123', 'aasd'),
('BI#005', 'aer aer', 'aer', '1989-01-04', 'aer aer', '123', 'aer aer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `my103_tabungan`
--

CREATE TABLE `my103_tabungan` (
  `id_103` varchar(255) NOT NULL,
  `id_102` varchar(255) NOT NULL,
  `tgl` int(2) NOT NULL,
  `bulan` varchar(50) NOT NULL,
  `tahun` int(4) NOT NULL,
  `debet` bigint(255) DEFAULT NULL,
  `kredit` bigint(255) DEFAULT NULL,
  `byid` varchar(255) NOT NULL,
  `ket` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `my101_user_login`
--
ALTER TABLE `my101_user_login`
  ADD PRIMARY KEY (`id_101`);

--
-- Indeks untuk tabel `my102_biodata`
--
ALTER TABLE `my102_biodata`
  ADD PRIMARY KEY (`id_102`);

--
-- Indeks untuk tabel `my103_tabungan`
--
ALTER TABLE `my103_tabungan`
  ADD PRIMARY KEY (`id_103`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
