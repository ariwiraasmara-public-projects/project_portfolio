BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "my101_userlogin" (
	"id_101" varchar(255) not null primary key,
	"username" varchar(255) not null unique,
	"email"	varchar(255) not null unique,
	"password" text not null
);
CREATE TABLE IF NOT EXISTS "my102_biodata" (
	"id_102" varchar(255) not null primary key,
	"nama" varchar(255) not null,
	"email"	varchar(255) not null unique,
	"tgl_lahir"	date not null,
	"alamat" text,
	"tlp" text,
	"status" text
);
CREATE TABLE IF NOT EXISTS "my103_tabungan" (
	"id_103" varchar(255) not null primary key,
	"id_102" varchar(255) not null,
	"tgl" int(2) not null,
	"bulan" varchar(50) not null,
	"tahun" int(4) not null,
	"debet" bigint(255) not null,
	"kredit" bigint(255) not null,
	"byid" varchar(255) not null,
	"ket" text null
);
COMMIT;
